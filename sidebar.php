<?php
/**
 * @package Clever
 */
?>
<div class="el-xs-12 el-md-4">
	<aside id="sidebar" class="widget-area" role="complementary">
		<?php dynamic_sidebar('sidebar'); ?>
	</aside><!-- #sidebar .widget-area -->
</div>