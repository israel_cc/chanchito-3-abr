<?php
/**
 * @package Clever
 */

get_header(); ?>
<section id="main" role="main">
	<div id="content">
		<div class="container group">
			<div class="el-xs-12">
				<article id="post-0" class="post error404 not-found">
					<header class="entry-header">
						<h1 class="entry-title"><?php _e('ERROR 404', 'clever'); ?></h1>
					</header>

					<div class="entry-content">
						<?php _e('El contenido que buscas no existe. Prueba a buscar algo…', 'clever'); ?>
						<?php get_search_form(); ?>
					</div>
				</div>
			</article>
		</div>
	</div>
</section>
<?php get_footer(); ?>