<?php
/**
 * @package Clever
 */
?>

<article id="page-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<div class="container group">
			<div class="el-xs-10 el-xs-push-1">
				<h1 class="entry-title"><?php the_title(); ?></h1>
			</div>
		</div>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<div class="container">
			<div class="el-xs-10 el-xs-push-1">
				<?php the_content(); ?>
			</div>
		</div>
	</div><!-- .entry-content -->
</article><!-- #post-<?php the_ID(); ?> -->
