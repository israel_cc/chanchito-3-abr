<?php
/**
 * @package Clever
 */

define('ICL_DONT_LOAD_LANGUAGE_SELECTOR_CSS', true);

function clever_setup() {
	load_theme_textdomain('clever', get_template_directory() . '/languages');

	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'title-tag' );
	add_theme_support( 'post-thumbnails' );
	add_theme_support( 'html5' );
	add_theme_support( 'nav-menus' );
	register_nav_menus( array( 'main_menu' => 'Menú ' ) );
	add_filter('show_admin_bar', '__return_false');
    //added 4 blog
    add_image_size( 'homepage-thumb', 373, 225, true );
}
add_action( 'after_setup_theme', 'clever_setup' );

add_filter('widget_text', 'do_shortcode');

function clever_widgets_init() {
	register_sidebar( array(
		'name' => 'Sidebar',
		'id' => 'sidebar',
		'before_widget' => '<div class="widget %1$s %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>',
	) );
}
add_action( 'init', 'clever_widgets_init' );

function clever_scripts() {
	global $wp_query;

	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
	remove_action( 'admin_print_styles', 'print_emoji_styles' );

	wp_enqueue_style( 'style-css', get_template_directory_uri().'/css/main.css' );

	wp_deregister_script( 'wp-embed' );
	wp_deregister_script( 'comment-reply' );

	wp_deregister_script('jquery');
	wp_enqueue_script('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js', false, '2.2.0');

	wp_deregister_script('jquery-migrate');
	wp_enqueue_script('jquery-migrate', 'http://code.jquery.com/jquery-migrate-1.3.0.js', array('jquery'), '1.3.0');

	wp_register_script( 'theme_script', get_template_directory_uri().'/js/script.js', array('jquery'), '3.2.0', true );
	wp_localize_script( 'theme_script', 'clever', array(
		'ajaxurl' => admin_url( 'admin-ajax.php' ),
		'query_vars' => json_encode( $wp_query->query_vars ),
	));

	wp_enqueue_script( 'theme_script' );
}
add_action( 'wp_enqueue_scripts', 'clever_scripts' );

function clever_admin_scripts() {
	wp_enqueue_media();
	wp_register_style('font-awesome-css', get_template_directory_uri().'/libraries/font-awesome.min.css');
	wp_register_style('clever-admin-css', get_template_directory_uri().'/libraries/clever/cleveradmin.css');
	wp_register_script('contactform-admin-js', get_template_directory_uri().'/libraries/clever/contactform-admin.js', array('jquery'));
	wp_register_script('slider-admin-js', get_template_directory_uri().'/libraries/clever/slider-admin.js', array('jquery-ui-sortable', 'jquery'));
	wp_register_script('social-admin-js', get_template_directory_uri().'/libraries/clever/social-admin.js', array('jquery-ui-sortable', 'jquery'));
}
add_action('admin_enqueue_scripts', 'clever_admin_scripts');

function clever_entry_meta() {
	if ( in_array( get_post_type(), array( 'post', 'attachment' ) ) ) {

		$time_string = sprintf( '<time class="entry-date published updated" datetime="%1$s">%2$s</time>',
			esc_attr( get_the_date( 'c' ) ),
			get_the_date()
		);

		printf( '<span class="posted-on">%1$s</span>',
			$time_string
		);
	}

	if (get_post_type() === 'post') {
		$categories_list = get_the_category_list( ', ' );
		if ( $categories_list && count(get_categories( array( 'fields' => 'ids', 'hide_empty' => 1, 'number' => 2 ) )) > 1) {
			printf( '<span class="cat-links">%1$s</span>',
				$categories_list
			);
		}
	}

}

function clever_entry_tags() {
	if ( in_array( get_post_type(), array( 'post', 'attachment' ) ) ) {

		$time_string = sprintf( '<time class="entry-date published updated" datetime="%1$s">%2$s</time>',
			esc_attr( get_the_date( 'c' ) ),
			get_the_date()
		);

		printf( '<span class="posted-on">%1$s</span>',
			$time_string
		);
	}

	if (get_post_type() === 'post') {
		$tags_list = get_the_tag_list('', ', ', '');
		if ( $tags_list ) {
			printf( '<span class="tags-links">%1$s</span>',
				$tags_list
			);
		}
	}

}

function install_pages() {
	global $pagenow;
	if ( 'themes.php' == $pagenow && isset( $_GET['activated'] ) ) {
		if (get_page_by_title( 'Inicio' ) === null) {
			$home = array(
				'post_title' => 'Inicio',
				'post_status' => 'publish',
				'post_content' => '',
				'post_type' => 'page',
				'ping_status' => 'closed',
				'comment_status' => 'closed',
			);
			wp_insert_post($home);
		}
		$home = get_page_by_title( 'Inicio' );
		update_option( 'page_on_front', $home->ID );
		update_option( 'show_on_front', 'page' );

		if (get_page_by_title( 'Blog' ) === null) {
			$blog = array(
				'post_title' => 'Blog',
				'post_status' => 'publish',
				'post_content' => '',
				'post_type' => 'page',
				'ping_status' => 'closed',
				'comment_status' => 'closed'
			);
			wp_insert_post($blog);
		}
		$blog = get_page_by_title( 'Blog' );
		update_option( 'page_for_posts', $blog->ID );

		if (get_page_by_title( 'Aviso legal' ) === null) {
			$legal = array(
				'post_title' => 'Aviso legal',
				'post_status' => 'publish',
				'post_type' => 'page',
				'ping_status' => 'closed',
				'comment_status' => 'closed'
			);
			wp_insert_post($legal);
		}

		if (get_page_by_title( 'Política de privacidad' ) === null) {
			$privacy = array(
				'post_title' => 'Política de privacidad',
				'post_status' => 'publish',
				'post_content' => getPrivacyContent(),
				'post_type' => 'page',
				'ping_status' => 'closed',
				'comment_status' => 'closed'
			);
			wp_insert_post($privacy);
		}

		wp_delete_post(2, true);

		if (!wp_get_nav_menu_object( 'main_menu' )) {
			$menu = wp_create_nav_menu( 'main_menu' );
			wp_update_nav_menu_item( $menu, 0, array(
					'menu-item-title' => 'Inicio',
					'menu-item-url' => home_url( '/' ),
					'menu-item-status' => 'publish'
				) );
		}

		update_option( 'blogdescription', "" );
		update_option( 'sidebars_widgets', array() );
	}
}
add_action( 'load-themes.php', 'install_pages' );

function clever_custom_post_types_404() {
	$custom_post_types_without_single = array(
		'clever_contact_forms',
		'clever_sliders',
		'clever_testimonials',
		'text_blocks'
	);
	$queried_post_type = get_query_var('post_type');
	if ( is_single() && in_array($queried_post_type, $custom_post_types_without_single) ) {
		status_header(404);
		nocache_headers();
		include( get_404_template() );
		exit();
	}
}
add_action( 'template_redirect', 'clever_custom_post_types_404' );

function clever_page_menu_args( $args ) {
	$args['show_home'] = true;
	return $args;
}
add_filter( 'wp_page_menu_args', 'clever_page_menu_args' );

function add_menu_parent_class( $items ) {
	$parents = array();
	foreach ( $items as $item ) {
		if ( $item->menu_item_parent && $item->menu_item_parent > 0 ) {
			$parents[] = $item->menu_item_parent;
		}
	}
	foreach ( $items as $item ) {
		if ( in_array( $item->ID, $parents ) ) {
			$item->classes[] = 'menu-parent-item';
		}
	}
	return $items;
}
add_filter( 'wp_nav_menu_objects', 'add_menu_parent_class' );

function clever_numeric_posts_nav($wp_query = null) {
	if( is_singular() )
		return;

	if (!$wp_query)
		global $wp_query;
	/** Stop execution if there's only 1 page */
	if( $wp_query->max_num_pages <= 1 )
		return;

	$paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
	$max   = intval( $wp_query->max_num_pages );

	/**	Add current page to the array */
	if ( $paged >= 1 )
		$links[] = $paged;

	/**	Add the pages around the current page to the array */
	if ( $paged >= 3 ) {
		$links[] = $paged - 1;
		$links[] = $paged - 2;
	}

	if ( ( $paged + 2 ) <= $max ) {
		$links[] = $paged + 2;
		$links[] = $paged + 1;
	}

	echo '<div class="pagination"><ul>' . "\n";

	/**	Previous Post Link */
	if ( get_previous_posts_link() )
		printf( '<li>%s</li>' . "\n", get_previous_posts_link( __('«', 'clever') ) );

	/**	Link to first page, plus ellipses if necessary */
	if ( ! in_array( 1, $links ) ) {
		$class = 1 == $paged ? ' class="active"' : '';

		printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );

		if ( ! in_array( 2, $links ) )
			echo '<li>…</li>';
	}

	/**	Link to current page, plus 2 pages in either direction if necessary */
	sort( $links );
	foreach ( (array) $links as $link ) {
		$class = $paged == $link ? ' class="active"' : '';
		printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
	}

	/**	Link to last page, plus ellipses if necessary */
	if ( ! in_array( $max, $links ) ) {
		if ( ! in_array( $max - 1, $links ) )
			echo '<li>…</li>' . "\n";

		$class = $paged == $max ? ' class="active"' : '';
		printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
	}

	/**	Next Post Link */
	if ( get_next_posts_link() )
		printf( '<li>%s</li>' . "\n", get_next_posts_link(__('»', 'clever')) );

	echo '</ul></div>' . "\n";
}

function clever_post_nav() {
	// Don't print empty markup if there's nowhere to navigate.
	$previous = ( is_attachment() ) ? get_post( get_post()->post_parent ) : get_adjacent_post( false, '', true );
	$next	 = get_adjacent_post( false, '', false );

	if ( ! $next && ! $previous ) {
		return;
	}

	?>
	<nav class="navigation post-navigation" role="navigation">
		<div class="nav-links">
			<?php
			if ( is_attachment() ) :
				previous_post_link( '%link', '&larr; %title' );
			else :
				previous_post_link( '%link', '&larr; %title' );
				next_post_link( '%link', '%title &rarr;' );
			endif;
			?>
		</div><!-- .nav-links -->
	</nav><!-- .navigation -->
	<?php
}

function d() {
	echo '<pre>';
	foreach (func_get_args() as $param) 
		var_dump($param);
	echo '</pre>';
}

function dd() {
	echo '<pre>';
	foreach (func_get_args() as $param) 
		var_dump($param);
	echo '</pre>';
	die();
}

function get_the_excerpt_max_charlength($charlength) {
	$excerpt = get_the_excerpt();
	$charlength++;
	$return = '';

	if ( mb_strlen( $excerpt ) > $charlength ) {
		$subex = mb_substr( $excerpt, 0, $charlength - 3 );
		$exwords = explode( ' ', $subex );
		$excut = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );
		if ( $excut < 0 ) {
			$return .= mb_substr( $subex, 0, $excut );
		} else {
			$return .= $subex;
		}
		$return .= '[…]';
	} else {
		$return .= $excerpt;
	}
	return $return;
}

function get_the_content_max_charlength($charlength) {
	$content = get_the_content();
	$charlength++;
	$return = '';

	if ( mb_strlen( $content ) > $charlength ) {
		$subex = mb_substr( $content, 0, $charlength - 3 );
		$exwords = explode( ' ', $subex );
		$excut = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );
		if ( $excut < 0 ) {
			$return .= mb_substr( $subex, 0, $excut );
		} else {
			$return .= $subex;
		}
		$return .= '[…]';
	} else {
		$return .= $content;
	}
	return $return;
}

function clever_pre_index_section_function() {
	?>
    <!--<div class="group group-content-space-around">-->
    <!--<div id="articles" class="group group-content-center">-->
	<!--<div class="container"  el-lg-pull-1 el-lg-10   class="group-content-space-between group-wrap el-sm-10 el-xs-12"  group-content-center>-->
         <div id="articles" class="container group-content-center">
	<?php
}
add_action('clever_pre_index_section', 'clever_pre_index_section_function', 1);

function clever_post_index_section_function() {
	?>
	<!--</div>-->
    <!--</div>-->
    </div>
	<?php
}
add_action('clever_post_index_section', 'clever_post_index_section_function', 1);

function clever_pre_index_header_function() {
	?>
	<div class="el-sm-10 el-xs-10">
	<?php
}
add_action('clever_pre_index_header', 'clever_pre_index_header_function', 1);

function clever_post_index_header_function() {
	?>
	</div>
	<?php
}
add_action('clever_post_index_header', 'clever_post_index_header_function', 1);

function clever_pre_index_content_function() {
	?>
	<div id="content"  class="el-sm-10 el-xs-10" > <!-- class="el-sm-10 el-xs-12 container group-content-space-between group-wrap" el-lg-12 el-md-12 el-md-8-->
	<?php
}
add_action('clever_pre_index_content', 'clever_pre_index_content_function', 1);

function clever_post_index_content_function() {
	?>
	</div>
	<?php
}
add_action('clever_post_index_content', 'clever_post_index_content_function', 1);

// Auto alt images
function update_image_alt( $meta_id, $post_id, $meta_key, $meta_value ) {
	$post = get_post( $post_id );
	if ( in_array(get_post_type($post), array('product', 'page', 'post', 'clever_testimonials', 'text_blocks', 'clever_sliders')) ) {
		if ( $meta_key == '_product_image_gallery' ) {
			$images = explode(',', $meta_value);
			foreach ($images as $attachment_id) {
				if ( get_post_meta( $attachment_id, '_wp_attachment_image_alt', true ) == '' )
					update_post_meta( $attachment_id, '_wp_attachment_image_alt', $post->post_title );
			}
		}
		else if ( $meta_key == '_thumbnail_id' ) {
			if ( get_post_meta( $attachment_id, '_wp_attachment_image_alt', true ) == '' )
				update_post_meta( $meta_value, '_wp_attachment_image_alt', $post->post_title );
		}
	}
}
add_action( 'added_post_meta', 'update_image_alt', 10, 4 );
add_action( 'updated_post_meta', 'update_image_alt', 10, 4 );

require_once dirname( __FILE__ ) .'/includes/ajax.php';
//require_once dirname( __FILE__ ) .'/includes/cleverslider.php';
//require_once dirname( __FILE__ ) .'/includes/cleversocial.php';
require_once dirname( __FILE__ ) .'/includes/comments.php';
require_once dirname( __FILE__ ) .'/includes/contactform.php';
require_once dirname( __FILE__ ) .'/includes/newsletter.php';
require_once dirname( __FILE__ ) .'/includes/privacy.php';
require_once dirname( __FILE__ ) .'/includes/testimonials.php';
require_once dirname( __FILE__ ) .'/includes/textblocks.php';
//require_once dirname( __FILE__ ) .'/includes/woocommerce.php';