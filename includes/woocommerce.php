<?php

add_filter( 'woocommerce_variable_sale_price_html', 'hide_variable_max_price', PHP_INT_MAX, 2 );
add_filter( 'woocommerce_variable_price_html',      'hide_variable_max_price', PHP_INT_MAX, 2 );
function hide_variable_max_price( $price, $_product ) {
	global $product;
	if( $product->is_type('variable') ){
	    $min_price_regular = $product->get_variation_regular_price( 'min', true );
	    $min_price_sale = $product->get_variation_sale_price( 'min', true );
	    if($min_price_regular === $min_price_sale){
	    	return __("Desde", 'clever') . " " . wc_price($min_price_regular);
	    }else{
	    	return __("Desde", 'clever') . ' <del>' . wc_price($min_price_regular) . '</del>' . ' <ins>' . wc_price($min_price_sale) . '</ins>';
	    }
	}
}

/*add_action( 'woocommerce_after_shop_loop_item_title', 'cj_show_dimensions', 9 );
function cj_show_dimensions() {
	global $product;
	$dimensions = $product->get_dimensions();
        if ( ! empty( $dimensions ) ) {
                echo '<span class="dimensions">' . $dimensions . '</span>';
        }
}*/