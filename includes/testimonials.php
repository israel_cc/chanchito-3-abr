<?php
/*---- TESTIMONIALS ----*/


//add_image_size( 'testimonials', 400, 200, true );

function create_clever_testimonials() {
	register_post_type( 'clever_testimonials', array(
		'labels' => array(
			'name' => 'Testimonios',
			'singular_name' => 'Testimonio',
			'add_new' => 'Añadir nuevo',
			'add_new_item' => 'Añadir nuevo testimonio',
			'edit_item' => 'Editar testimonio',
			'new_item' => 'Nuevo testimonio',
			'all_items' => 'Todos los testimonios',
			'view_item' => 'Ver testimonios',
			'search_items' => 'Buscar testimonios',
			'not_found' => 'No se han encontrado testimonios',
			'not_found_in_trash' => 'No se han encontrado testimonios en la papelera',
		),
		'public' => true,
		'exclude_from_search' => true,
		'supports' => array('title','thumbnail','editor'),
		'has_archive' => true,
		'hierarchical' => false,
		'menu_position' => 27,
		'rewrite' => array('slug' => 'testimonies'),
		'query_var' => false,
		'menu_icon' => 'dashicons-testimonial',
	) );
}
add_action( 'init', 'create_clever_testimonials' );

function show_testimonials($number = -1) {
	$output = '';
	$args = array(
		'post_type' => array('clever_testimonials'),
		'post_status' => 'publish',
		'posts_per_page' => $number,
		'order_by' => 'rand'
	);
	$size = 'thumbnail';
	if (in_array('testimonials', get_intermediate_image_sizes()))
		$size = 'testimonials';
	$query_testimonials = new WP_Query( $args );
	if ( $query_testimonials->have_posts() ) :
		$output = '<div class="testimonials"><ul>';
		while ( $query_testimonials->have_posts() ) : $query_testimonials->the_post();
			$output .= '
			<li>
				<div class="thumbnail">';
			if (has_post_thumbnail()) {
				$output .= wp_get_attachment_image(get_post_thumbnail_id(), $size);
			}
			$output .= '
				</div>
				<section class="info">
					<h3>'.get_the_title().'</h3>
					<p>'.get_the_content().'</p>
				</section>
			</li>';
		endwhile;
		$output .= '</ul></div>';
	endif;
	wp_reset_postdata();
	echo $output;
}

/*---- TESTIMONIALS ----*/
?>