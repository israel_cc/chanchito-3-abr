<?php
/*---- CONTACT FORM ----*/
function create_clever_contact_forms() {
	register_post_type( 'clever_contact_forms', array(
		'labels' => array(
			'name' => 'Contacto',
			'singular_name' => 'Formulario',
			'add_new' => 'Añadir nuevo',
			'add_new_item' => 'Añadir nuevo formulario',
			'edit_item' => 'Editar formulario',
			'new_item' => 'Nuevo formulario',
			'all_items' => 'Todos los formularios',
			'view_item' => 'Ver formulario',
			'search_items' => 'Buscar formularios',
			'not_found' => 'No se han encontrado formularios',
			'not_found_in_trash' => 'No se han encontrado formularios en la papelera',
		),
		'public' => true,
		'exclude_from_search' => true,
		'supports' => 'title',
		'hierarchical' => false,
		'menu_position' => 26,
		'has_archive' => false,
		'rewrite' => false,
		'query_var' => false,
		'menu_icon' => 'dashicons-email',
	) );
}
add_action( 'init', 'create_clever_contact_forms' );

function clever_contact_forms_shortcode( $atts ){
	extract( shortcode_atts( array(
		'id' => 0,
	), $atts ) );

	$post = get_post($id);

	if ($post == NULL || $post->post_type != 'clever_contact_forms')
		return "";

	$fields = get_post_meta( $post->ID, 'clever_contact_forms_fields', true );
	$template = get_post_meta( $post->ID, 'clever_contact_forms_html', true );

	foreach ($fields as $field) {
		$input = getFieldOutput($field);
		$template = str_replace('*'.$field['name'].'*', $input, $template);
	}

	ob_start();
	?>
	<div id="ccf-<?php echo $id; ?>" class="ccf">
		<form action="" method="POST" enctype="multipart/form-data" data-id="<?php echo $id; ?>">
			<input type="hidden" name="id" value="<?php echo $id; ?>"/>
			<input type="hidden" name="cc_send_check" value="sent" />
			<?php echo $template; ?>
		</form>
		<div class="sending" data-url="<?php echo get_template_directory_uri(); ?>"></div>
		<?php (isset($_POST) && isset($_POST['cc_send_check']) && $_POST['cc_send_check'] === 'sent') ? clever_contact_forms_send_mail() : '' ?>
	</div>
	<?php
	return ob_get_clean();
}
add_shortcode( 'ccf', 'clever_contact_forms_shortcode' );

function getFieldOutput($field) {

	$output = "";
	$required = ($field['required'] === 'on' ? ' required' : '');
	$asterisk = ($field['required'] === 'on' && $field['placeholder'] !== '' ? ' *' : '');

	switch ($field['type']) {
		case 'checkbox':
		case 'radio':
			if ($field['type'] === 'checkbox') $field['name'] .= '[]';
			ob_start(); ?>
			<fieldset>
				<legend><?php echo $field['placeholder'].$asterisk; ?></legend>
				<?php foreach (explode(';', $field['options']) as $option) { ?>
				<label class="<?php echo $field['type']; ?>">
					<input type="<?php echo $field['type']; ?>" name="<?php echo $field['name']; ?>"<?php echo $required; ?> value="<?php echo $option; ?>" class="no-js"/>
					<span class="icon"></span>
					 <?php echo $option; ?>
				</label>
				<?php } ?>
			</fieldset>
			<?php
			$output = ob_get_clean();
			break;
		case 'date':
		case 'email':
		case 'number':
		case 'tel':
		case 'text':
		case 'url':
			ob_start(); ?>
			<input type="<?php echo $field['type']; ?>" name="<?php echo $field['name']; ?>" placeholder="<?php echo $field['placeholder'].$asterisk; ?>"<?php echo $required; ?><?php if ($mode = getInputMode($field['type'])) echo ' inputmode="'.$mode.'"'; ?>/>
			<?php
			$output = ob_get_clean();
			break;
		case 'file':
			ob_start(); ?>
			<span class="file-wrap input-wrap" title="<?php echo $field['placeholder'].$asterisk; ?>" data-file="">
				<i class="fa fa-file fa-fw"></i> <?php echo $field['placeholder']; ?>
				<input type="file" name="<?php echo $field['name']; ?>"<?php echo $required; ?> class="no-js"/>
			</span>
			<?php
			$output = ob_get_clean();
			break;
		case 'select':
			ob_start(); ?>
			<span class="select-wrap input-wrap">
				<select name="<?php echo $field['name']; ?>"<?php echo $required; ?> class="no-js">
					<option <?php if ($field['required'] !== 'on') echo 'disabled ';?>selected><?php echo $field['placeholder'].$asterisk; ?></option>
					<?php foreach (explode(';', $field['options']) as $option) { ?>
					<option value="<?php echo $option; ?>"><?php echo $option; ?></option>
					<?php } ?>
				</select>
				<span class="selector">
					<i class="fa fa-caret-up selector-up"></i>
					<i class="fa fa-caret-down selector-down"></i>
				</span>
			</span>
			<?php
			$output = ob_get_clean();
			break;
		case 'submit':
			ob_start(); ?>
			<button type="submit"><?php echo $field['placeholder']; ?></button>
			<?php
			$output = ob_get_clean();
			break;
		case 'textarea':
			ob_start(); ?>
			<textarea name="<?php echo $field['name']; ?>" placeholder="<?php echo $field['placeholder'].$asterisk; ?>"<?php echo $required; ?>></textarea>
			<?php
			$output = ob_get_clean();
			break;
	}
	return $output;
}

function getInputMode($type) {
	switch ($type) {
		case 'email':
		case 'tel':
		case 'url':
			return $type;
			break;
		case 'number':
			return 'numeric';
			break;
	}
	return false;
}

function clever_contact_forms_add_custom_box() {
	add_meta_box(
		'clever_contact_form_settings',
		'Ajustes',
		'clever_contact_forms_custom_box',
		'clever_contact_forms',
		'normal',
		'high'
	);
}
add_action( 'add_meta_boxes', 'clever_contact_forms_add_custom_box' );

function clever_contact_forms_custom_box( $post ) {
	$screen = get_current_screen();
	wp_enqueue_script('contactform-admin-js');
	wp_enqueue_style('clever-admin-css');
	$to_value = get_post_meta( $post->ID, 'clever_contact_forms_to', true );
	$subject_value = get_post_meta( $post->ID, 'clever_contact_forms_subject', true );
	$html_value = get_post_meta( $post->ID, 'clever_contact_forms_html', true );
	$body_value = get_post_meta( $post->ID, 'clever_contact_forms_body', true );
	$fields = get_post_meta( $post->ID, 'clever_contact_forms_fields', true );

	ob_start();
	?>
		<table class="form-table fixed" cellspacing="0">
			<tr>
				<th scope="row">
					Código
				</th>
				<td>
					<input type="text" id="shortcode" value="[ccf id=&quot;<?php echo $post->ID; ?>&quot;]" readonly/>
				</td>
			</tr>
			<tr class="inline-edit-row">
				<th scope="row">
					<label for="clever_contact_forms_to">Para</label>
				</th>
				<td class="input-text-wrap">
					<input type="text" id="clever_contact_forms_to" name="clever_contact_forms_to" value="<?php echo esc_attr($to_value); ?>" placeholder="(separa las direcciones entre comas)"/>
				</td>
			</tr>
			<tr class="inline-edit-row">
				<th scope="row">
					<label for="clever_contact_forms_subject">Asunto</label>
				</th>
				<td class="input-text-wrap">
					<input type="text" id="clever_contact_forms_subject" name="clever_contact_forms_subject" value="<?php echo esc_attr($subject_value); ?>"/>
				</td>
			</tr>
			<tr>
				<th scope="row">
					<label>Campos</label>
				</th>
				<td class="table-container">
					<button type="button" id="new_field" class="button button-primary button-medium">Añadir campo</button>
					<table id="fields_table" class="form-table">
						<thead>
							<tr>
								<th scope="col">Tipo</th>
								<th scope="col">Texto</th>
								<th scope="col">Nombre</th>
								<th scope="col">Opciones</th>
								<th scope="col">Obligatorio</th>
								<th scope="col"></th>
							</tr>
						</thead>
	<?php
	if ($screen->action === 'add') {
		echo getContactFormRow( array('name' => 'cc_name', 'placeholder' => 'Nombre', 'type' => 'text', 'required' => true) );
		echo getContactFormRow( array('name' => 'cc_email', 'placeholder' => 'Correo electrónico', 'type' => 'email', 'required' => true) );
		echo getContactFormRow( array('name' => 'cc_message', 'placeholder' => 'Mensaje', 'type' => 'textarea', 'required' => true) );
		echo getContactFormRow( array('name' => 'cc_submit', 'placeholder' => 'Enviar', 'type' => 'submit', 'required' => false) );
	} else if (is_array($fields)) {
		foreach ($fields as $field) {
			echo getContactFormRow($field);
		}
	}
	?>
					</table>
				</td>
			</tr>
			<tr>
				<th scope="row">
					<label for="clever_contact_forms_html">Formulario</label><br/>
					<ul class="form-table contactshortcodes"></ul>
				</th>
				<td>
					<textarea id="clever_contact_forms_html" name="clever_contact_forms_html"><?php echo esc_attr($html_value); ?></textarea>
				</td>
			</tr>
			<tr>
				<th scope="row">
					<label for="clever_contact_forms_body">Plantilla de email</label><br/>
					<ul class="form-table contactshortcodes"></ul>
				</th>
				<td>
					<textarea id="clever_contact_forms_body" name="clever_contact_forms_body"><?php echo esc_attr($body_value); ?></textarea>
				</td>
			</tr>
		</table>
	<?php
	echo ob_get_clean();
}

function getFieldTypesOptions($selected = null) {
	$output = '';
	$validTypes = array('checkbox','date','email','file','number','radio','select','tel','text','textarea','url');
	if ($selected === 'submit')
		$validTypes = array('submit');

	foreach ($validTypes as $type) {
		$output .= '<option value="'.$type.'"';
		if ($selected === $type)
			$output .= ' selected="selected"';
		$output .= '>'.strtoupper($type).'</option>';
	}
	return $output;
}

function clever_contact_forms_save_postdata( $post_id ) {
	if ( ! current_user_can( 'edit_post', $post_id ) || get_post_type($post_id) !== 'clever_contact_forms') {
		return;
	}

	$mydata = sanitize_text_field( $_POST['clever_contact_forms_to'] );
	update_post_meta($post_id, 'clever_contact_forms_to', $mydata);
	$mydata = sanitize_text_field( $_POST['clever_contact_forms_subject'] );
	update_post_meta($post_id, 'clever_contact_forms_subject', $mydata);
	$mydata =  $_POST['clever_contact_forms_html'];
	update_post_meta($post_id, 'clever_contact_forms_html', $mydata);
	$mydata = $_POST['clever_contact_forms_body'];
	update_post_meta($post_id, 'clever_contact_forms_body', $mydata);

	if (isset($_POST['field_name'])) {
		$fields = array();
		foreach ($_POST['field_name'] as $key => $value) {
			$field = array();
			$field['type'] = $_POST['field_type'][$key];
			$field['name'] = sanitize_text_field($_POST['field_name'][$key]);
			$field['placeholder'] = $_POST['field_placeholder'][$key];
			$field['options'] = $_POST['field_options'][$key];
			$field['required'] = $_POST['field_required'][$key];
			$fields[] = $field;
		}
		$resutl = update_post_meta($post_id, 'clever_contact_forms_fields', $fields);
	}
}
add_action( 'save_post', 'clever_contact_forms_save_postdata' );

function clever_contact_forms_send_mail() {
	// $json = array();
	// $json['result'] = false;
	// $json['test'] = $_POST['id'];

	if (!isset($_POST['id']) || $_POST['id'] == "") {
		echo '<div class="response error">' . __('El formulario no es válido','clever') . '</div>';
	} else {
		$post = get_post($_POST['id']);
		if ($post == NULL || $post->post_type != "clever_contact_forms") {
			echo '<div class="response error">' . __('El formulario no es válido','clever') . '</div>';
		} else {
			$to = get_post_meta( $post->ID, 'clever_contact_forms_to', true );
			$subject = get_post_meta( $post->ID, 'clever_contact_forms_subject', true );
			$body = get_post_meta( $post->ID, 'clever_contact_forms_body', true );

			$fields = get_post_meta( $post->ID, 'clever_contact_forms_fields', true );

			// $json['fields'] = $fields;
			// $json['post'] = $_POST;
			foreach ($fields as $field) {
				switch ($field['type']) {
					case 'checkbox':
						$value = implode(', ', $_POST[$field['name']]);
						break;
					case 'textarea':
						$value = nl2br(htmlspecialchars($_POST[$field['name']]));
						break;
					default:
						$value = $_POST[$field['name']];
						break;
				}
				$body = str_replace('*'.$field['name'].'*', $value, $body);
			}

			// $json['body'] = $body;

			$attachments = array();
			$upload_dir = WP_CONTENT_DIR.'/uploads/';
			if (!file_exists($upload_dir)) {
				mkdir($upload_dir, 0777, true);
			}
			foreach ($_FILES as $file) {
				$url = $upload_dir.basename($file['name']);
				move_uploaded_file($file['tmp_name'], $url);
				$attachments[] = $url;
			}

			$from_address = (isset($_POST['email']) ? $_POST['email'] : get_option('admin_email'));

			$headers = 'From: '.$from_address."\r\n".
				'Reply-To: '.$from_address."\r\n".
				'X-Mailer: PHP/'.phpversion();

			add_filter( 'wp_mail_content_type', 'set_html_content_type' );
			$send = wp_mail($to, $subject, $body, $headers, $attachments);
			remove_filter( 'wp_mail_content_type', 'set_html_content_type' );

			// $json['send'] = $send;

			if ($send) {
				echo '<div class="response correct">' .  __('Mensaje enviado correctamente. Gracias.','clever') . '</div>';
				// $json['result'] = true;
			} else {
				echo '<div class="response error">' .  __('Ha ocurrido un error y no se ha podido enviar el mensaje.','clever') . '</div>';
			}
		}
	}
	// echo json_encode($json);
	// die(var_dump($_POST));
}
// add_action( 'admin_post_clever_contact_forms_send_mail', 'clever_contact_forms_send_mail' );
// add_action( 'admin_post_nopriv_clever_contact_forms_send_mail', 'clever_contact_forms_send_mail' );

function getContactFormRow($field = null) {

	if ($field === null) {
		$field = array(
			'name' => '',
			'placeholder' => '',
			'type' => '',
			'options' => '',
			'required' => false,
		);
	}

	ob_start();
	?>
	<tr class="alternate<?php if ($field['type'] === 'submit') echo ' submit-row'; ?>">
		<td>
			<select name="field_type[]">
				<?php echo getFieldTypesOptions($field['type']); ?>
			</select>
		</td>
		<td>
			<input type="text" name="field_placeholder[]" value="<?php echo $field['placeholder']; ?>"/>
		</td>
		<td>
			<input type="text" name="field_name[]" class="field_name" value="<?php echo $field['name']; ?>" readonly/>
		</td>
		<td>
			<input type="hidden" name="field_options[]" value="<?php echo $field['options']; ?>"/>
		</td>
		<td>
			<?php if ($field['type'] !== 'submit') { ?>
			<input type="checkbox" class="setrequired"<?php if ($field['required'] === 'on' || $field['required'] === true) echo ' checked'; ?>/>
			<?php } ?>
			<input type="hidden" name="field_required[]"<?php if ($field['required'] === 'on' || $field['required'] === true) echo ' value="on"'; ?>/>
		</td>
		<td>
			<?php if ($field['type'] !== 'submit') { ?>
			<a href="#" class="delete_field">Eliminar</a>
			<?php } ?>
		</td>
	</tr>
	<?php
	return ob_get_clean();
}

function clever_contact_forms_get_field_row() {
	$json = array();
	if (isset($_POST['type']))
		$json['row'] = getContactFormRow(array('type' => $_POST['type']));
	else
		$json['row'] = getContactFormRow();
	echo json_encode($json);
	die();
}
add_action( 'wp_ajax_clever_contact_forms_get_field_row', 'clever_contact_forms_get_field_row' );

function set_html_content_type() {
	return 'text/html';
}
/*---- end CONTACT FORM ----*/
