<?php

// Register Custom Post Type
function text_blocks() {
	register_post_type( 'text_blocks', array(
		'labels' => array(
			'name' => 'Bloques de texto',
			'singular_name' => 'Bloque de texto',
			'menu_name' => 'Bloques de texto',
			'all_items' => 'Todos los bloques',
			'view_item' => 'Ver bloque',
			'add_new_item' => 'Añadir nuevo bloque',
			'add_new' => 'Añadir nuevo',
			'edit_item' => 'Editar bloque',
			'update_item' => 'Actualizar bloque',
			'search_items' => 'Buscar bloques',
			'not_found' => 'No se han encontrado bloques',
			'not_found_in_trash' => 'No se han encontrado bloques en la papelera',
		),
		'public' => true,
		'exclude_from_search' => true,
		'hierarchical' => false,
		'menu_position' => 28,
		'supports' => array('title', 'editor'),
		'has_archive' => false,
		'query_var' => false,
		'menu_icon' => 'dashicons-text',
	) );
}

// Hook into the 'init' action
add_action( 'init', 'text_blocks', 0 );

add_filter('manage_posts_columns', 'text_block_column');
add_action('manage_posts_custom_column', 'text_block_column_content', 10, 2);

function text_block_column($defaults) {
	if ($_GET['post_type'] == 'text_blocks')
		$defaults['id'] = 'ID';

	return $defaults;
}
function text_block_column_content($column_name, $post_ID) {
	if ($column_name == 'id') {
	    echo '<p><code>echo get_text_block(' .  $post_ID . ')</code></p>';
	    echo "<p><code>[get_text_block id=\"$post_ID\"]</code></p>";
	}
}

function get_text_block($id)
{
	$p = get_post($id);

	if ($p->post_type !== 'text_blocks')
		return false;

	return apply_filters('the_content', $p->post_content);
}

function get_text_block_f( $atts ) {
	$atts = shortcode_atts( array(
		'id' => ''
	), $atts );

	echo get_text_block($id);
}
add_shortcode( 'get_text_block', 'get_text_block_f' );
