<?php

function clever_social_menu() {
	add_menu_page( 'Social', 'Social', 'manage_options', 'clever-social', 'clever_social_options', 'dashicons-share', 30 );
	add_submenu_page( 'clever-social', 'Compartir entradas en redes sociales', 'Compartir entradas', 'manage_options', 'clever_social_share', 'clever_social_options_share' );
}
add_action( 'admin_menu', 'clever_social_menu' );

global $cleverSocialOptions;
$cleverSocialOptions = array(
	'bitbucket' => 'Bitbucket',
	'delicious' => 'Delicious',
	'facebook-official' => 'Facebook',
	'flickr' => 'Flickr',
	'foursquare' => 'Foursquare',
	'github' => 'GitHub',
	'google-plus' => 'Google+',
	'instagram' => 'Instagram',
	'lastfm' => 'Last fm',
	'linkedin' => 'LinkedIn',
	'pinterest' => 'Pinterest',
	'reddit' => 'reddit',
	'rss' => 'RSS',
	'soundcloud' => 'SoundCloud',
	'spotify' => 'Spotify',
	'twitter' => 'Twitter',
	'vimeo-square' => 'Vimeo',
	'vine' => 'Vine',
	'youtube' => 'YouTube',
);

function clever_social_options() {
	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}

	global $cleverSocialOptions;

	if (isset($_POST['cls'])) {
		$social = array();
		foreach ($_POST['cls'] as $key => $value) {
			$social[$key] = $value;
		}
		update_option( 'clever_social', $social, '', false );
	}
	$social = get_option( 'clever_social' );
	if (!is_array($social))
		$social = array();
	?>
	<div class="wrap">
		<h2>Social</h2>
		<p>Añade las direcciones de tus redes sociales para que puedas <a href="<?php echo esc_url( home_url('/wp-admin/widgets.php') ); ?>">añadirlas a un widget</a>.</p>
		<form action="" method="post">
			<table class="form-table">
				<tbody>
					<?php foreach ($cleverSocialOptions as $key => $name) { ?>
					<tr>
						<th scope="row">
							<label for="cls-<?php echo $key; ?>"><?php echo $name; ?></label>
						</th>
						<td>
							<input type="text" name="cls[<?php echo $key; ?>]" id="cls-<?php echo $key; ?>" class="regular-text" value="<?php if (isset($social[$key])) echo $social[$key]; ?>"/>
						</td>
					</tr>
					<?php } ?>
				</tbody>
			</table>
			<p class="submit">
				<input type="submit" name="submit" id="submit" class="button button-primary" value="Guardar cambios"/>
			</p>
		</form>
	</div>

	<?php
}

function clever_social_options_share() {
	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}

	if (isset($_POST['update'])) {
		update_option('cls-share-facebook', (isset($_POST['cls-share-facebook']) ? 'checked' : 'unchecked'));
		update_option('cls-share-twitter', (isset($_POST['cls-share-twitter']) ? 'checked' : 'unchecked'));
		update_option('cls-share-google', (isset($_POST['cls-share-google']) ? 'checked' : 'unchecked'));
		update_option('cls-share-linkedin', (isset($_POST['cls-share-linkedin']) ? 'checked' : 'unchecked'));
		update_option('cls-share-email', (isset($_POST['cls-share-email']) ? 'checked' : 'unchecked'));
		update_option('cls-share-pinterest', (isset($_POST['cls-share-pinterest']) ? 'checked' : 'unchecked'));
		update_option('cls-share-reddit', (isset($_POST['cls-share-reddit']) ? 'checked' : 'unchecked'));
	}


	wp_enqueue_style('font-awesome-css');
	?>
	<div class="wrap">
		<h2>Social</h2>

		<form action="" method="post">
			<table class="form-table">
				<tbody>
					<tr>
						<th scope="row">
							<label for="cls-facebook">Facebook</label>
						</th>
						<td>
							<input type="checkbox" name="cls-share-facebook" id="cls-facebook" value="<?php echo get_option('cls-share-facebook', 'checked'); ?>" <?php echo get_option('cls-share-facebook', 'checked'); ?>/>
							&nbsp;&nbsp;<i class="fa fa-facebook" style="color:#3b5998;"></i>
						</td>
					</tr>
					<tr>
						<th scope="row">
							<label for="cls-twitter">Twitter</label>
						</th>
						<td>
							<input type="checkbox" name="cls-share-twitter" id="cls-twitter" value="<?php echo get_option('cls-share-twitter', 'checked'); ?>" <?php echo get_option('cls-share-twitter', 'checked'); ?>/>
							&nbsp;&nbsp;<i class="fa fa-twitter" style="color:#00aced;"></i>
						</td>
					</tr>
					<tr>
						<th scope="row">
							<label for="cls-google">Google+</label>
						</th>
						<td>
							<input type="checkbox" name="cls-share-google" id="cls-google" value="<?php echo get_option('cls-share-google', 'checked'); ?>" <?php echo get_option('cls-share-google', 'checked'); ?>/>
							&nbsp;&nbsp;<i class="fa fa-google-plus" style="color:#dd4b39;"></i>
						</td>
					</tr>
					<tr>
						<th scope="row">
							<label for="cls-linkedin">LinkedIn</label>
						</th>
						<td>
							<input type="checkbox" name="cls-share-linkedin" id="cls-linkedin" value="<?php echo get_option('cls-share-linkedin', 'checked'); ?>" <?php echo get_option('cls-share-linkedin', 'checked'); ?>/>
							&nbsp;&nbsp;<i class="fa fa-linkedin" style="color:#007bb6;"></i>
						</td>
					</tr>
					<tr>
						<th scope="row">
							<label for="cls-email">Correo electrónico</label>
						</th>
						<td>
							<input type="checkbox" name="cls-share-email" id="cls-email" value="<?php echo get_option('cls-share-email', 'checked'); ?>" <?php echo get_option('cls-share-email', 'checked'); ?>/>
							&nbsp;&nbsp;<i class="fa fa-envelope" style="color:#0266c8;"></i>
						</td>
					</tr>
					<tr>
						<th scope="row">
							<label for="cls-pinterest">Pinterest</label>
						</th>
						<td>
							<input type="checkbox" name="cls-share-pinterest" id="cls-pinterest" value="<?php echo get_option('cls-share-pinterest', ''); ?>" <?php echo get_option('cls-share-pinterest', ''); ?>/>
							&nbsp;&nbsp;<i class="fa fa-pinterest" style="color:#cb2027;"></i>
						</td>
					</tr>
					<tr>
						<th scope="row">
							<label for="cls-reddit">Reddit</label>
						</th>
						<td>
							<input type="checkbox" name="cls-share-reddit" id="cls-reddit" value="<?php echo get_option('cls-share-reddit', ''); ?>" <?php echo get_option('cls-share-reddit', ''); ?>/>
							&nbsp;&nbsp;<i class="fa fa-reddit" style="color:#ff4500;"></i>
						</td>
					</tr>
				</tbody>
			</table>
			<p class="submit">
				<input type="hidden" name="update" value="true"/>
				<input type="submit" name="submit" id="submit" class="button button-primary" value="Guardar cambios"/>
			</p>
		</form>
	</div>
	<?php
}

function clever_social_share() {
	/*
	 * Facebook: https://www.facebook.com/sharer/sharer.php?u={URL}
	 * Twitter: http://twitter.com/share?url={URL}&text={TITLE}
	 * Google+: https://plus.google.com/share?url={URL}
	 * LinkedIn: https://www.linkedin.com/shareArticle?mini=true&url={URL}&title={TITULO}&summary={CONTENIDO}&source=
	 * Email: mailto:?subject={TITLE}&body={URL}%20{CONTENIDO}
	 * Pinterest: https://pinterest.com/pin/create/button/?url={TITULO}&media={URL}&description={CONTENIDO}
	 * Reddit: http://reddit.com/submit?url={URL}&title={TITLE}
	 */

	?>
	<div class="clear"></div>

	<h4><?php _e('Compártelo', 'clever'); ?></h4>
	<ul class="social-links">
		<?php if (get_option('cls-share-facebook', 'checked') == 'checked'): ?>
			<li><a href="<?php echo esc_url('https://wwww.facebook.com/sharer/sharer.php?u='.get_permalink()); ?>" title="Facebook" target="_blank"><i class="fa fa-lg fa-facebook-official"></i></a></li>
		<?php endif; ?>
		<?php if (get_option('cls-share-twitter', 'checked') == 'checked'): ?>
			<li><a href="<?php echo esc_url('http://twitter.com/share?url='.get_permalink().'&text'.get_the_title()); ?>" title="Twitter" target="_blank"><i class="fa fa-lg fa-twitter"></i></a></li>
		<?php endif; ?>
		<?php if (get_option('cls-share-google', 'checked') == 'checked'): ?>
			<li><a href="<?php echo esc_url('https://plus.google.com/share?url='.get_permalink()); ?>" title="Google+" target="_blank"><i class="fa fa-lg fa-google-plus"></i></a></li>
		<?php endif; ?>
		<?php if (get_option('cls-share-linkedin', 'checked') == 'checked'): ?>
			<li><a href="<?php echo esc_url('https://www.linkedin.com/shareArticle?mini=true&url='.get_permalink().'&title='.get_the_title().'&summary='.get_the_excerpt().'&source='); ?>" title="LinkedIn" target="_blank"><i class="fa fa-lg fa-linkedin"></i></a></li>
		<?php endif; ?>
		<?php if (get_option('cls-share-email', 'checked') == 'checked'): ?>
			<li><a href="<?php echo esc_url('mailto:?subject='.get_the_title().'&body='.get_permalink().' '.get_the_excerpt()); ?>" title="<?php _e('Correo electrónico', 'clever'); ?>" target="_blank"><i class="fa fa-lg fa-envelope"></i></a></li>
		<?php endif; ?>
		<?php if (get_option('cls-share-pinterest', 'unchecked') == 'checked' && has_post_thumbnail()): ?>
			<li><a href="<?php echo esc_url('https://pinterest.com/pin/create/button/?url='.get_the_title().'&media='.get_the_post_thumbnail_url(get_the_ID(), 'full').'&description='.get_the_excerpt()); ?>" title="Pinterest" target="_blank"><i class="fa fa-lg fa-pinterest"></i></a></li>
		<?php endif; ?>
		<?php if (get_option('cls-share-reddit', 'unchecked') == 'checked'): ?>
			<li><a href="<?php echo esc_url('http://reddit.com/submit?url='.get_permalink().'&title='.get_the_title()); ?>" title="Reddit" target="_blank"><i class="fa fa-lg fa-reddit"></i></a></li>
		<?php endif; ?>
	</ul>
	<?php
}

class Clever_Social extends WP_Widget {

	public function __construct() {
		parent::__construct(
			'clever_social',
			'Social Links',
			array( 'description' => 'Redes sociales' )
		);
	}

	public function widget( $args, $instance ) {
		global $cleverSocialOptions;
		$social = ( isset($instance['social']) ) ? explode(',', $instance['social']) : array();
		$socialUrls = get_option( 'clever_social' );
		if (!is_array($socialUrls))
			$socialUrls = array();

		echo $args['before_widget'];
		echo $args['before_title'];
		echo $instance['title'];
		echo $args['after_title'];
		?>
		<ul class="social-links">
		<?php
		foreach ($social as $value) {
			if (isset($socialUrls[$value])) {
			?>
			<li>
				<a href="<?php echo $socialUrls[$value]; ?>" title="<?php echo $cleverSocialOptions[$value]; ?>" target="_blank">
					<i class="fa fa-lg fa-<?php echo $value; ?>"></i>
				</a>
			</li>
		<?php }
		}
		?>
		</ul>
		<?php
		echo $args['after_widget'];
	}

	public function form( $instance ) {
		global $cleverSocialOptions;

		wp_enqueue_script('social-admin-js');
		wp_enqueue_style('clever-admin-css');

		$social = get_option( 'clever_social' );
		if (!is_array($social))
			$social = array();

		$title = ( isset($instance['title']) ) ? $instance['title'] : "";
		$values = ( isset($instance['social']) ) ? explode(',', $instance['social']) : array();

		if (!empty($values)) {
			$oldSocial = $social;
			$newSocial = array();
			foreach ($values as $value)
				$newSocial[$value] = $social[$value];

			uksort($newSocial, function($a, $b) use ($values) {
				return array_search($a, $values) - !array_search($b, $values);
			});
			$social = array_merge($newSocial, array_diff_key($oldSocial, $newSocial));
		}

		?>
		<p>
			<label for="<?php echo $this->get_field_name( 'title' ); ?>">Título:</label>
			<input class="social_ico social_title" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>"/>
		</p>
		<div id="social-options">
			<ul class="ui-sortable">
			<?php
			foreach ($social as $key => $value) {
				if ($value !== "") {
				?>
				<li>
					<span class="sort-social dashicons dashicons-sort"></span>
					<label>
						<input type="checkbox" name="<?php echo $this->get_field_name( 'social' ); ?>[]" value="<?php echo $key; ?>" <?php if (in_array($key, $values)) echo 'checked'; ?>/>
						 <?php echo $cleverSocialOptions[$key]; ?>
					</label>
				</li>
			<?php
				}
			}
			?>
			</ul>
		</div>
		<?php
	}

	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( !empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : "";
		$instance['social'] = ( !empty( $new_instance['social'] ) ) ? implode(',', $new_instance['social']) : "";
		return $instance;
	}
}
add_action( 'widgets_init', 'registerCleverSocial');

function registerCleverSocial() {
	register_widget( 'Clever_Social' );
}
?>