<?php
class Clever_Newsletter extends WP_Widget {

	public function __construct() {
		parent::__construct(
			'clever_newsletter',
			'Newsletter',
			array( 'description' => 'Suscripción a newsletter', )
		);
	}

	public function widget( $args, $instance ) {
		print_newsletter($instance['text'], $instance['title'], $args['before_title'], $args['after_title']);
	}

 	public function form( $instance ) {
 		?>
		<p>
			<label for="<?php echo $this->get_field_name( 'title' ); ?>">Título:</label>
			<input class="social_ico social_title" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>"/>
		</p>
		<p>
			<label for="<?php echo $this->get_field_name( 'text' ); ?>">Texto:</label>
			<textarea class="widefat" name="<?php echo $this->get_field_name( 'text' ); ?>"><?php echo esc_attr( $instance['text'] ); ?></textarea>
		</p>
		<?php
	}

	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$title = ( isset($instance['title']) ) ? strip_tags( $instance['title'] ) : "";
		$instance['text'] = ( ! empty( $new_instance['text'] ) ) ? $new_instance['text'] : '';

		return $instance;
	}
}

function register_newsletter() {
	register_widget( 'Clever_Newsletter' );
}
add_action( 'widgets_init', 'register_newsletter');

function print_newsletter($text = false, $title = false, $before_title = '<h3>', $after_title = '</h3>') { ?>
	
	<div class="newsletter">
		<?php if ($title) {
			echo $before_title.$title.$after_title;
		} ?>
		<?php if ($text) { ?>
		<p><?php echo $text; ?></p>
		<?php } ?>
		<form action="">
			<input type="email" name="email" placeholder="<?php _e('Correo electrónico','clever'); ?>" required/>
			<button type="submit"><?php _e('Apuntarme', 'clever'); ?></button>
			<?php
			$linkOpen = '<a href="'.get_permalink(7).'" class="terms" target="_blank">';
			$linkClose = '</a>';
			?>
			<label class="checkbox">
				<input type="checkbox" name="terms" required/> 
				<span class="icon"></span>
				<small><?php echo sprintf(__('He leído y acepto %1$s la política de privacidad %2$s'), $linkOpen, $linkClose); ?></small>
			</label>
		</form>
	</div>

	<?php
}

?>