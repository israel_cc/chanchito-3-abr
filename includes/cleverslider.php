<?php
/*---- CLEVER SLIDER ----*/

//add_image_size( 'cleverslider', 600, 270, true );

function create_clever_slider() {
	register_post_type( 'clever_sliders', array(
		'labels' => array(
			'name' => 'Sliders',
			'singular_name' => 'Slider',
			'add_new' => 'Añadir slider',
			'menu_name' => 'Slider',
			'add_new_item' => 'Añadir nuevo slider',
			'edit_item' => 'Editar slider',
			'new_item' => 'Nuevo slider',
			'all_items' => 'Todos los sliders',
			'view_item' => 'Ver sliders',
			'search_items' => 'Buscar sliders',
			'not_found' => 'No se han encontrado sliders',
			'not_found_in_trash' => 'No se han encontrado sliders en la papelera',
		),
		'public' => true,
		'exclude_from_search' => true,
		'supports' => array('title'),
		'has_archive' => true,
		'hierarchical' => false,
		'menu_position' => 29,
		'rewrite' => array('slug' => 'sliders'),
		'query_var' => false,
		'menu_icon' => 'dashicons-slides',
	) );
}
add_action( 'init', 'create_clever_slider' );

function clever_slider_add_custom_box() {
	add_meta_box(
		'clever_slider_settings',
		'Imágenes',
		'clever_slider_custom_box',
		'clever_sliders',
		'normal',
		'high'
	);
}
add_action( 'add_meta_boxes', 'clever_slider_add_custom_box' );

function clever_slider_custom_box($post) {
	$screen = get_current_screen();
	wp_enqueue_script('slider-admin-js');
	wp_enqueue_style('clever-admin-css');
	$images = get_post_meta( $post->ID, 'clever_slider_images', true );

	ob_start();
	?>
	<div class="inside">
		<button type="button" id="add-images" class="button button-primary button-large">Añadir imágenes</button>
		<div class="wrapper">
			<ul id="slides" class="ui-sortable clearfix">
				<?php
				if ($images != "") {
					foreach ($images as $image)
						echo get_clever_slider_row($image);
				}
				?>
			</ul>
		</div>
	</div>
	<?php
	echo ob_get_clean();
}

function clever_slider_save_postdata( $post_id ) {
	if ( ! current_user_can( 'edit_post', $post_id ) || get_post_type($post_id) !== 'clever_sliders') {
		return;
	}
	
	if (isset($_POST['slide_images'])) {
		$fields = array();
		foreach ($_POST['slide_images'] as $key => $value) {
			$field = array();
			$field['image'] = $_POST['slide_images'][$key];
			$field['title'] = $_POST['slide_titles'][$key];
			$field['text'] = $_POST['slide_texts'][$key];
			$field['button'] = $_POST['slide_buttons'][$key];
			$field['url'] = $_POST['slide_urls'][$key];
			$fields[] = $field;
		}
		update_post_meta($post_id, 'clever_slider_images', $fields);
	}
}
add_action( 'save_post', 'clever_slider_save_postdata' );

function get_clever_slider_row($slide) {
	ob_start();
	if (wp_get_attachment_image( $slide['image'] , 'thumbnail')) {
	?>
	<li class="slide">
		<div class="sort-image image-action">
			<span class="dashicons dashicons-leftright"></span>
		</div>
		<a href="#" class="delete-image image-action">
			<span class="dashicons dashicons-no-alt"></span>
		</a>
		<?php echo wp_get_attachment_image( $slide['image'] , 'thumbnail'); ?>
		<input type="hidden" name="slide_images[]" value="<?php echo $slide['image']; ?>"/>
		<input type="text" name="slide_titles[]" value="<?php if (isset($slide['title'])) echo $slide['title']; ?>" placeholder="Titular"/>
		<textarea name="slide_texts[]" placeholder="Texto"><?php if (isset($slide['text'])) echo $slide['text']; ?></textarea>
		<input type="text" name="slide_buttons[]" value="<?php if (isset($slide['button'])) echo $slide['button']; ?>" placeholder="Texto del botón"/>
		<input type="url" name="slide_urls[]" value="<?php if (isset($slide['url'])) echo $slide['url']; ?>" placeholder="Url del enlace" novalidate/>
	</li>
	<?php
	}
	return ob_get_clean();
}

function clever_slider_get_field_row() {
	$json = array();
	$json['post'] = $_POST['image'];
	if (isset($_POST['image']))
		$json['row'] = get_clever_slider_row(array('image' => $_POST['image']));
	echo json_encode($json);
	die();
}
add_action( 'wp_ajax_clever_slider_get_field_row', 'clever_slider_get_field_row' );

function print_slider($id) {

	// wpml filter language
	if ( function_exists('icl_object_id') )
		$id = icl_object_id($id, get_post_type($id), false, ICL_LANGUAGE_CODE);

	$images = get_post_meta( $id, 'clever_slider_images', true );

	if (get_post_type($id) !== 'clever_sliders' || $images === "")
		return;

	wp_register_script( 'cleverslider', get_template_directory_uri().'/libraries/FlexSlider/jquery.flexslider.js', array('jquery') );
	wp_enqueue_script( 'cleverslider' );

	$size = 'medium';
	if (in_array('cleverslider', get_intermediate_image_sizes()))
		$size = 'cleverslider';

	ob_start();
	?>
	<div class="clever-slider clearfix">
		<ul class="slides">
		<?php foreach ($images as $image) { ?>
			<li>
				<?php echo wp_get_attachment_image( $image['image'] , $size) ?>
				<div class="caption">
					<?php if ($image['title'] != "") { ?>
					<h3><?php echo $image['title']; ?></h3>
					<?php } ?>
					<?php if ($image['text'] != "") { ?>
					<div class="text">
						<?php echo $image['text']; ?>
					</div>
					<?php } ?>
					<?php if ($image['button'] != "") {
						$url = "";
						if ($image['url'] != "")
							$url = parse_url($image['url']);
					?>
					<a href="<?php echo ($image['url'] == "" ? '#' : $image['url']); ?>" target="<?php echo (!empty($url['host']) && strcasecmp($url['host'], $_SERVER['HTTP_HOST']) ? '_blank' : '_self'); ?>" class="button"><?php echo $image['button']; ?></a>
					<?php } ?>
				</div>
			</li>
		<?php } ?>
		</ul>
	</div>
	<?php
	echo ob_get_clean();
}
/*---- end CLEVER SLIDER ----*/
?>