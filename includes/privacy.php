<?php

function getPrivacyContent() {
	return '
		<a id="cookiesinfo" name="cookiesinfo"></a>
		<h2>Política de cookies</h2>
		<p><strong>¿Qué es una cookie?</strong></p>
		<p>Las cookies son unos pequeños archivos de texto que se instalan en tu ordenador, móvil o tableta y que contienen información sobre lo que has hecho en esta web.</p>
		<p>Esta información se utiliza para cosas tan útiles como que no tengas que meter tu usuario y contraseña cada vez que entras en determinada página web, o para mostrarte publicidad de productos que has investigado anteriormente.</p>
		<p><strong>¿Por qué usamos cookies?</strong></p>
		<p>Las cookies nos ayudan a mejorar tu experiencia de navegación y a detectar mejoras para nuestra web.</p>
		<p><strong>¿Qué cookies usamos y para qué sirven?</strong></p>
		<p>En este sitio usamos cookies propias y de terceros:</p>
		<div class="wrap">
			<table class="cookies-table">
				<thead>
					<tr>
						<th>Tipo de cookies</th>
						<th>Instalador</th>
						<th>Nombre</th>
						<th>Expiración</th>
						<th>Utilidad</th>
					</tr>
				</thead>
				<tbody>
					<!-- Wordpress -->
					<tr>
						<td rowspan="4"><strong>Propias</strong></td>
						<td rowspan="4"><strong>Wordpress</strong></td>
						<td>comment_author</td>
						<td rowspan="3">Temporal según navegador</td>
						<td rowspan="4">Guardan tus datos sólo si comentas un post para que no tengas que meterlos con cada nuevo comentario.</td>
					</tr>
					<tr>
						<!--td></td-->
						<!--td></td-->
						<td>comment_author_email</td>
						<!--td></td-->
						<!--td></td-->
					</tr>
					<tr>
						<!--td></td-->
						<!--td></td-->
						<td>comment_author_url</td>
						<!--td></td-->
						<!--td></td-->
					</tr>
					<tr>
						<!--td></td-->
						<!--td></td-->
						<td>_qca</td>
						<td>Sirve para analizar el tráfico de la web</td>
						<!--td></td-->
					</tr>
					<!-- Google Analytics -->
					<tr>
						<td rowspan="8"><strong>De terceros</strong></td>
						<td rowspan="8"><strong>Google Analytics</strong></td>
						<td>_utma</td>
						<td>2 a&ntilde;os</td>
						<td rowspan="8">Conectan con Google Analytics y sirven para conocer, de forma anónima, cómo has usado esta web</td>
					</tr>
					<tr>
						<!--td></td-->
						<!--td></td-->
						<td>_utmb</td>
						<td>30 minutos</td>
						<!--td></td-->
					</tr>
					<tr>
						<!--td></td-->
						<!--td></td-->
						<td>_utmc</td>
						<td>Al finalizar sesión</td>
						<!--td></td-->
					</tr>
					<tr>
						<!--td></td-->
						<!--td></td-->
						<td>_utmv</td>
						<td>6 meses</td>
						<!--td></td-->
					</tr>
					<tr>
						<!--td></td-->
						<!--td></td-->
						<td>_utmz</td>
						<td>6 meses</td>
						<!--td></td-->
					</tr>
					<tr>
						<!--td></td-->
						<!--td></td-->
						<td>eu_cn</td>
						<td>Permanente</td>
						<!--td></td-->
					</tr>
					<tr>
						<!--td></td-->
						<!--td></td-->
						<td>_lipt</td>
						<td>1 mes</td>
						<!--td></td-->
					</tr>
					<tr>
						<!--td></td-->
						<!--td></td-->
						<td>_bcookie</td>
						<td></td>
						<!--td></td-->
					</tr>
				</tbody>
			</table>
		</div>
		<p><strong>¿Qué pasa si desactivas las cookies?</strong></p>
		<p>Si desactivas las cookies es posible que algunos de nuestros servicios no funcionen correctamente.</p>
		<p><strong>Consentimiento</strong></p>
		<p>Seguir navegando por esta web implica el consentimiento y aceptación de nuestras cookies.</p>
		<p>Para rectificar sobre este consentimiento, sigue las instrucciones del siguiente apartado.</p>
		<p><strong>¿Cómo cambiar la configuración o desactivar las cookies?</strong></p>
		<p>Para cambiar, bloquear o eliminar las cookies entra en las opciones de configuración de tu navegador y sigue las instrucciones:</p>
		<ul>
			<li><p><strong>Internet Explorer:</strong> Herramientas -> Opciones de Internet -> Privacidad -> Configuración.</p>
			<p>Si quieres más información visita el <a href="https://login.live.com/login.srf?wa=wsignin1.0&rpsnv=12&ct=1391519729&rver=6.1.6206.0&wp=LBI_SSL&wreply=https:%2F%2Flogin.live.com:443%2Flogin.srf%3Fwa%3Dwsignin1.0%26rpsnv%3D12%26rver%3D6.1.6206.0%26wp%3DLBI_SSL%26wreply%3Dhttps%253a%252f%252fredir.windows.microsoft.com%252fliveid%253fru%253des-es%25252finternet-explorer%25252fdelete-manage-cookies%26lc%3D1033%26id%3D285275%26cbcxt%3D%26mkt%3D&lc=1033&id=285275#ie=ie-11">soporte de Microsoft</a> o la Ayuda del navegador.</p></li>
			<li><p><strong>Firefox:</strong> Herramientas -> Opciones -> Privacidad -> Historial -> Configuración Personalizada.</p>
			<p>Si quieres más información visita el <a href="http://support.mozilla.org/es/search?esab=a&q=cookies">soporte de Mozilla</a> o la Ayuda del navegador.</p></li>
			<li><p><strong>Chrome:</strong> Configuración -> Mostrar opciones avanzadas -> Privacidad -> Configuración de contenido.</p>
			<p>Si quieres más información visita el <a href="https://support.google.com/chrome/answer/95647?hl=es">soporte de Google</a> o la Ayuda del navegador.</p></li>
			<li><p><strong>Safari:</strong> Preferencias -> Seguridad.</p>
			<p>Si quieres más información visita el <a href="http://support.apple.com/kb/HT1677?viewlocale=es_ES&locale=es_ES">soporte de Apple</a> o la Ayuda del navegador.</p></li>
		</ul>
		<p><strong>Si no usas ninguno de estos navegadores,</strong> selecciona en tu navegador la pestaña “Ayuda” y la opción “Cookies” para obtener información sobre la ubicación de la carpeta de cookies.</p>
		<p><strong>Actualizaciones y cambios en nuestra política de cookies</strong></p>
		<p>Esta política puede cambiar para cumplir con futuros cambios sobre la Ley de Cookies <a href="http://www.boe.es/boe/dias/2012/03/31/pdfs/BOE-A-2012-4442.pdf">Real Decreto-ley 13/2012</a>, por eso te recomendamos revisar esta información periódicamente.</p>';
}
?>