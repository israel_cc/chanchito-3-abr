<?php
/**
 * @package Clever
 */
if ( post_password_required() ) {
	return;
}
?>
<div id="comments" class="comments-area">

	<?php if ( have_comments() ) : ?>
		<h3 class="comments-title">
			<?php
				printf( _nx( 'Un comentario', '%1$s comentarios', get_comments_number(), 'comments title', 'clever' ),
					number_format_i18n( get_comments_number() ) );
			?>
		</h3>

		<?php clever_comment_nav(); ?>

		<ol class="comment-list">
			<?php
				wp_list_comments( array(
					'callback'    => 'clever_comments',
					'style'       => 'ol',
					'avatar_size' => 0,
				) );
			?>
		</ol><!-- .comment-list -->

		<?php clever_comment_nav(); ?>

	<?php endif; ?>

	<?php
		if ( ! comments_open() && get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) :
	?>
		<p class="no-comments"><?php _e( 'Los comentarios están cerrados.', 'clever' ); ?></p>
	<?php endif; ?>

	<?php
		$commenter = wp_get_current_commenter();
		$commentArgs = array(
			'comment_field' => '
				<p class="comment-form-comment">
					<textarea id="comment" name="comment" placeholder="'.__( 'Comentario', 'clever' ).'"></textarea>
				</p>',
			'comment_notes_after' => '',
			'comment_notes_before' => '',
			'fields' => array(
				'author' =>  '
					<p class="comment-form-author">
						<input id="author" name="author" type="text" value="'.esc_attr( $commenter['comment_author'] ).'" placeholder="'.__('Nombre','clever').' *"/>
					</p>',
  				'email' => '
  					<p class="comment-form-email">
  						<input id="email" name="email" type="text" value="'.esc_attr( $commenter['comment_author_email'] ).'" placeholder="'.__('Correo electrónico','clever').' *"/>
  					</p>',
			),
			'logged_in_as' => '',
			'title_reply' => __('Deja un comentario','clever'),
		); 
		comment_form( $commentArgs );
	?>

</div><!-- .comments-area -->