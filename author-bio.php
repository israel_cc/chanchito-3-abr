<?php
/**
 * @package Clever
 */
?>

<div class="author-info el-xs-12">
	<h2 class="author-heading"><?php _e( 'Sobre el autor', 'clever' ); ?></h2>
	<div class="author-avatar">
		<?php
		echo get_avatar( get_the_author_meta( 'user_email' ), 56 );
		?>
	</div>

	<div class="author-description">
		<h3 class="author-title"><?php echo get_the_author(); ?></h3>

		<p class="author-bio">
			<?php the_author_meta( 'description' ); ?>
			<a class="author-link" href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>" rel="author">
				<?php printf( __( 'Ver todos los posts de %s', 'clever' ), get_the_author() ); ?>
			</a>
		</p>

	</div><!-- .author-description -->
</div><!-- .author-info -->
