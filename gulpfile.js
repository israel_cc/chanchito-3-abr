var gulp = require('gulp'),
	sass = require('gulp-sass'),
	autoprefixer = require('gulp-autoprefixer');

gulp.task('styles', function() {
	return gulp.src('./scss/main.scss')
			.pipe(sass().on('error', sass.logError))
			.pipe(autoprefixer('last 2 versions'))
			.pipe(gulp.dest('./css'));
});

gulp.task('build', ['styles']);

gulp.task('default', function() {
	gulp.start('build');
	gulp.start('watch');
});

gulp.task('watch', function() {
	gulp.watch(['./scss/*/*', './scss/*'], ['styles']);
});