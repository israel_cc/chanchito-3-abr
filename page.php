<?php
/**
 * @package Clever
 */

get_header(); ?>
<section id="main" role="main">
	<div id="content">
		<?php while ( have_posts() ) : the_post(); ?>

			<?php get_template_part( 'content', 'page' ); ?>

		<?php endwhile; ?>
	</div>
</section>
<?php get_footer(); ?>