<?php
/**
 * @package Clever
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>"/>
	<meta name="viewport" content="width=device-width,initial-scale=1"/>
	<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico"/>
	<?php if ( is_singular() && get_option( 'thread_comments' ) ) wp_enqueue_script( 'comment-reply' ); ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>"/>
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<div id="page" class="hfeed">
	<div id="cookies">
		<div class="container group">
			<?php $url = '<a href="'.get_permalink(7).'#cookiesinfo" target="_blank">la política de privacidad</a>'; ?>
			<?php echo sprintf(__('Como todos los sitios web, usamos cookies. Si continúas navegando aceptas %1$s.','clever'), $url); ?>
			<i class="close-cookies fa fa-times-circle fa-lg"></i>
		</div>
	</div>
	<div id="menu-opener"></div>
	<header id="banner" role="banner" class="clearfix">
            <div class="el-md-push-5 el-sm-push-3 el-md-pusll-1 el-xs-push-1 el-md-6 el-sm-8 el-xs-10" ><?php get_search_form(); ?></div>
		<div class="container group-content-center"> <!--group-content-space-between-->
            
			<!--<div class="container" style="border: 1px solid red" > <!--class="logo" el-lg-12 el-md-12 -->
                <div id="site-logo" class="el-lg-2 el-sm-3 el-xs-6" > <!--class="el-lg-2 el-md-4"-->
                    
                   <a href="<?php echo esc_url(home_url( '/' )); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php echo get_text_block(113); ?></a>                             <?php //bloginfo( 'name' ); ?>          
                </div>
                <div id="site-title-menu" class="el-lg-5 el-sm-6 el-xs-8">
				<div id="site-title" > <!--class="el-lg-9 el-sm-6"-->
                    <?php echo get_text_block(99); ?>
				</div>
                <div class="menu-block group group-align-content-center text-center"> <!-- class="el-lg-9 el-sm-6" el-lg-7 el-md-7-->
                    <div id="toggle">
                        <i class="fa fa-bars"></i>
                    </div>
                    <nav id="navigation" role="navigation" ><!--class="clearfix"-->
                        <div class="shadow"></div>
                        <?php wp_nav_menu( array( 'theme_location' => 'main_menu' ) ); ?>
                    </nav>
                </div>
                </div>
                <div id="newsletter" class="group group-align-conten-sapce-between group-align-content-center el-sm-10 el-xs-10"> <!--el-lg-10 el-md-10 el-sm-10 el-xs-10-->
                    <div class="el-sm-5 el-xs-10 text-center"> <!--text-sm-left-->
                        <?php echo get_text_block(117); //pull-right ?>
                    </div>
                    <div class="el-sm-5 el-xs-10">
                        <a href="<?php echo get_permalink(7); ?>"><?php _e('Política de privacidad', 'clever'); ?></a>
                    </div>
                </div>
            <!-- * propio formulario* **include newsletter** -->
			<!--</div>-->
		</div>
	</header><!-- #banner -->