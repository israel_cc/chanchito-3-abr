-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 04-04-2018 a las 02:31:18
-- Versión del servidor: 10.1.30-MariaDB
-- Versión de PHP: 5.6.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `localwp`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `localwp_commentmeta`
--

CREATE TABLE `localwp_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `localwp_comments`
--

CREATE TABLE `localwp_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `localwp_comments`
--

INSERT INTO `localwp_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'Un comentarista de WordPress', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2018-03-19 12:01:04', '2018-03-19 18:01:04', 'Hola, esto es un comentario. Para empezar a moderar, editar y borrar comentarios, por favor, visita la pantalla de comentarios en el escritorio. Los avatares de los comentaristas provienen de <a href=\"https://gravatar.com\">Gravatar</a>.', 0, 'post-trashed', '', '', 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `localwp_links`
--

CREATE TABLE `localwp_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `localwp_options`
--

CREATE TABLE `localwp_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `localwp_options`
--

INSERT INTO `localwp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://localhost:8080/localwp.dev', 'yes'),
(2, 'home', 'http://localhost:8080/localwp.dev', 'yes'),
(3, 'blogname', 'Luis Monge Malo', 'yes'),
(4, 'blogdescription', '', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'iromero@cleverconsulting.net', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '0', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'j F, Y', 'yes'),
(24, 'time_format', 'g:i a', 'yes'),
(25, 'links_updated_date_format', 'j F, Y g:i a', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%year%/%monthnum%/%day%/%postname%/', 'yes'),
(29, 'rewrite_rules', 'a:130:{s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:14:\"testimonies/?$\";s:39:\"index.php?post_type=clever_testimonials\";s:44:\"testimonies/feed/(feed|rdf|rss|rss2|atom)/?$\";s:56:\"index.php?post_type=clever_testimonials&feed=$matches[1]\";s:39:\"testimonies/(feed|rdf|rss|rss2|atom)/?$\";s:56:\"index.php?post_type=clever_testimonials&feed=$matches[1]\";s:31:\"testimonies/page/([0-9]{1,})/?$\";s:57:\"index.php?post_type=clever_testimonials&paged=$matches[1]\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:23:\"category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:39:\"text_blocks/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:49:\"text_blocks/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:69:\"text_blocks/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:64:\"text_blocks/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:64:\"text_blocks/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:45:\"text_blocks/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:28:\"text_blocks/([^/]+)/embed/?$\";s:59:\"index.php?post_type=text_blocks&name=$matches[1]&embed=true\";s:32:\"text_blocks/([^/]+)/trackback/?$\";s:53:\"index.php?post_type=text_blocks&name=$matches[1]&tb=1\";s:40:\"text_blocks/([^/]+)/page/?([0-9]{1,})/?$\";s:66:\"index.php?post_type=text_blocks&name=$matches[1]&paged=$matches[2]\";s:47:\"text_blocks/([^/]+)/comment-page-([0-9]{1,})/?$\";s:66:\"index.php?post_type=text_blocks&name=$matches[1]&cpage=$matches[2]\";s:36:\"text_blocks/([^/]+)(?:/([0-9]+))?/?$\";s:65:\"index.php?post_type=text_blocks&name=$matches[1]&page=$matches[2]\";s:28:\"text_blocks/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:38:\"text_blocks/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:58:\"text_blocks/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:53:\"text_blocks/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:53:\"text_blocks/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:34:\"text_blocks/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:39:\"testimonies/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:49:\"testimonies/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:69:\"testimonies/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:64:\"testimonies/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:64:\"testimonies/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:45:\"testimonies/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:28:\"testimonies/([^/]+)/embed/?$\";s:67:\"index.php?post_type=clever_testimonials&name=$matches[1]&embed=true\";s:32:\"testimonies/([^/]+)/trackback/?$\";s:61:\"index.php?post_type=clever_testimonials&name=$matches[1]&tb=1\";s:52:\"testimonies/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:73:\"index.php?post_type=clever_testimonials&name=$matches[1]&feed=$matches[2]\";s:47:\"testimonies/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:73:\"index.php?post_type=clever_testimonials&name=$matches[1]&feed=$matches[2]\";s:40:\"testimonies/([^/]+)/page/?([0-9]{1,})/?$\";s:74:\"index.php?post_type=clever_testimonials&name=$matches[1]&paged=$matches[2]\";s:47:\"testimonies/([^/]+)/comment-page-([0-9]{1,})/?$\";s:74:\"index.php?post_type=clever_testimonials&name=$matches[1]&cpage=$matches[2]\";s:36:\"testimonies/([^/]+)(?:/([0-9]+))?/?$\";s:73:\"index.php?post_type=clever_testimonials&name=$matches[1]&page=$matches[2]\";s:28:\"testimonies/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:38:\"testimonies/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:58:\"testimonies/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:53:\"testimonies/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:53:\"testimonies/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:34:\"testimonies/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:27:\"comment-page-([0-9]{1,})/?$\";s:39:\"index.php?&page_id=96&cpage=$matches[1]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:58:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:68:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:88:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:83:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:83:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:64:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:53:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/embed/?$\";s:91:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/trackback/?$\";s:85:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&tb=1\";s:77:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]\";s:72:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]\";s:65:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/page/?([0-9]{1,})/?$\";s:98:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&paged=$matches[5]\";s:72:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/comment-page-([0-9]{1,})/?$\";s:98:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&cpage=$matches[5]\";s:61:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)(?:/([0-9]+))?/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&page=$matches[5]\";s:47:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:57:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:77:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:72:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:72:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:53:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&cpage=$matches[4]\";s:51:\"([0-9]{4})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&cpage=$matches[3]\";s:38:\"([0-9]{4})/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&cpage=$matches[2]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";}', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:1:{i:0;s:29:\"easy-wp-smtp/easy-wp-smtp.php\";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '-6', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', 'a:4:{i:0;s:87:\"D:\\xampp\\htdocs\\localwp.dev/wp-content/themes/cleverconsulting-chanchito-base/style.css\";i:1;s:101:\"D:\\xampp\\htdocs\\localwp.dev/wp-content/themes/cleverconsulting-chanchito-base-aacef1806d34/header.php\";i:2;s:100:\"D:\\xampp\\htdocs\\localwp.dev/wp-content/themes/cleverconsulting-chanchito-base-aacef1806d34/style.css\";i:3;s:0:\"\";}', 'no'),
(40, 'template', 'cleverconsulting-chanchito-base', 'yes'),
(41, 'stylesheet', 'cleverconsulting-chanchito-base', 'yes'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '38590', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '0', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'posts', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(79, 'widget_text', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(80, 'widget_rss', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(81, 'uninstall_plugins', 'a:1:{s:29:\"easy-wp-smtp/easy-wp-smtp.php\";s:22:\"swpsmtp_send_uninstall\";}', 'no'),
(82, 'timezone_string', '', 'yes'),
(84, 'page_on_front', '96', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'initial_db_version', '38590', 'yes'),
(92, 'localwp_user_roles', 'a:5:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:61:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:34:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}}', 'yes'),
(93, 'fresh_site', '0', 'yes'),
(94, 'WPLANG', 'es_MX', 'yes'),
(95, 'widget_search', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(96, 'widget_recent-posts', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(97, 'widget_recent-comments', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(98, 'widget_archives', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(99, 'widget_meta', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(100, 'sidebars_widgets', 'a:3:{s:19:\"wp_inactive_widgets\";a:0:{}s:7:\"sidebar\";a:0:{}s:13:\"array_version\";i:3;}', 'yes'),
(101, 'widget_pages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(102, 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(103, 'widget_media_audio', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(104, 'widget_media_image', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(105, 'widget_media_gallery', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(106, 'widget_media_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(107, 'widget_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(108, 'widget_nav_menu', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(109, 'widget_custom_html', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(110, 'cron', 'a:4:{i:1522821666;a:3:{s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1522864909;a:2:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1522867688;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}s:7:\"version\";i:2;}', 'yes'),
(111, 'theme_mods_twentyseventeen', 'a:3:{s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1521612358;s:4:\"data\";a:4:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:9:\"sidebar-2\";a:0:{}s:9:\"sidebar-3\";a:0:{}}}s:18:\"nav_menu_locations\";a:2:{s:6:\"social\";i:15;s:3:\"top\";i:17;}}', 'yes'),
(134, 'can_compress_scripts', '1', 'no'),
(143, 'recently_activated', 'a:0:{}', 'yes'),
(149, 'swpsmtp_options', 'a:8:{s:16:\"from_email_field\";s:28:\"iromero@cleverconsulting.net\";s:15:\"from_name_field\";s:6:\"Israel\";s:23:\"force_from_name_replace\";i:1;s:13:\"smtp_settings\";a:8:{s:4:\"host\";s:14:\"smtp.gmail.com\";s:15:\"type_encryption\";s:3:\"tls\";s:4:\"port\";s:3:\"587\";s:13:\"autentication\";s:3:\"yes\";s:8:\"username\";s:28:\"iromero@cleverconsulting.net\";s:8:\"password\";s:12:\"aXJmQDM4NTM=\";s:12:\"enable_debug\";b:0;s:12:\"insecure_ssl\";b:0;}s:15:\"allowed_domains\";s:12:\"bG9jYWxob3N0\";s:14:\"reply_to_email\";s:0:\"\";s:17:\"email_ignore_list\";s:0:\"\";s:19:\"enable_domain_check\";b:0;}', 'yes'),
(152, 'smtp_test_mail', 'a:3:{s:10:\"swpsmtp_to\";s:28:\"iromero@cleverconsulting.net\";s:15:\"swpsmtp_subject\";s:16:\"tst wp smtp mail\";s:15:\"swpsmtp_message\";s:16:\"tst wp smtp mail\";}', 'yes'),
(155, 'current_theme', 'Clever', 'yes'),
(156, 'theme_mods_twentyfifteen', 'a:4:{i:0;b:0;s:18:\"nav_menu_locations\";a:2:{s:7:\"primary\";i:17;s:6:\"social\";i:15;}s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1521626159;s:4:\"data\";a:2:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}}}}', 'yes'),
(157, 'theme_switched', '', 'yes'),
(167, 'category_children', 'a:0:{}', 'yes'),
(192, 'nav_menu_options', 'a:1:{s:8:\"auto_add\";a:0:{}}', 'yes'),
(204, '_site_transient_update_core', 'O:8:\"stdClass\":4:{s:7:\"updates\";a:1:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:6:\"latest\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-4.9.4.zip\";s:6:\"locale\";s:5:\"es_MX\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-4.9.4.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-4.9.4-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-4.9.4-new-bundled.zip\";s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"4.9.4\";s:7:\"version\";s:5:\"4.9.4\";s:11:\"php_version\";s:5:\"5.2.4\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"4.7\";s:15:\"partial_version\";s:0:\"\";}}s:12:\"last_checked\";i:1522778480;s:15:\"version_checked\";s:5:\"4.9.4\";s:12:\"translations\";a:0:{}}', 'no'),
(205, '_site_transient_update_plugins', 'O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1522778481;s:8:\"response\";N;s:12:\"translations\";N;s:9:\"no_update\";N;}', 'no'),
(214, 'widget_widget_twentyfourteen_ephemera', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(215, 'theme_mods_twentyfourteen', 'a:3:{s:18:\"custom_css_post_id\";i:-1;s:18:\"nav_menu_locations\";a:1:{s:7:\"primary\";i:17;}s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1521732937;s:4:\"data\";a:4:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:0:{}s:9:\"sidebar-2\";a:0:{}s:9:\"sidebar-3\";a:0:{}}}}', 'yes'),
(226, 'widget_clever_social', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(227, 'widget_clever_newsletter', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(228, 'theme_mods_cleverconsulting-chanchito-base-aacef1806d34', 'a:3:{s:18:\"custom_css_post_id\";i:-1;s:18:\"nav_menu_locations\";a:1:{s:9:\"main_menu\";i:17;}s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1522018968;s:4:\"data\";a:4:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:0:{}s:9:\"sidebar-2\";a:0:{}s:9:\"sidebar-3\";a:0:{}}}}', 'yes'),
(229, '_site_transient_update_themes', 'O:8:\"stdClass\":2:{s:12:\"last_checked\";i:1522778482;s:7:\"checked\";a:6:{s:44:\"cleverconsulting-chanchito-base-aacef1806d34\";s:3:\"3.4\";s:31:\"cleverconsulting-chanchito-base\";s:3:\"3.4\";s:13:\"twentyfifteen\";s:3:\"1.9\";s:14:\"twentyfourteen\";s:3:\"2.1\";s:15:\"twentyseventeen\";s:3:\"1.4\";s:13:\"twentysixteen\";s:3:\"1.4\";}}', 'no'),
(253, '_transient_twentyfifteen_categories', '2', 'yes'),
(266, 'theme_switched_via_customizer', '', 'yes'),
(267, 'customize_stashed_theme_mods', 'a:0:{}', 'no'),
(274, 'page_for_posts', '0', 'yes'),
(307, 'theme_switch_menu_locations', 'a:1:{s:9:\"main_menu\";i:17;}', 'yes'),
(310, '_transient_twentyfourteen_category_count', '2', 'yes'),
(311, '_transient_featured_content_ids', 'a:0:{}', 'yes'),
(362, 'theme_mods_cleverconsulting-chanchito-base', 'a:2:{s:18:\"custom_css_post_id\";i:-1;s:18:\"nav_menu_locations\";a:1:{s:9:\"main_menu\";i:17;}}', 'yes'),
(442, '_transient_timeout_dash_v2_3605f57d6641b1f6504fce9d39bcf566', '1522808088', 'no'),
(443, '_transient_dash_v2_3605f57d6641b1f6504fce9d39bcf566', '<div class=\"rss-widget\"><p><strong>Error RSS:</strong> A feed could not be found at http://wordpress.org/news/feed/. A feed with an invalid mime type may fall victim to this error, or SimplePie was unable to auto-discover it.. Use force_feed() if you are certain this URL is a real feed.</p></div><div class=\"rss-widget\"><p><strong>Error RSS:</strong> WP HTTP Error: cURL error 35: error:14077419:SSL routines:SSL23_GET_SERVER_HELLO:tlsv1 alert access denied</p></div>', 'no'),
(448, '_site_transient_timeout_theme_roots', '1522780282', 'no'),
(449, '_site_transient_theme_roots', 'a:6:{s:44:\"cleverconsulting-chanchito-base-aacef1806d34\";s:7:\"/themes\";s:31:\"cleverconsulting-chanchito-base\";s:7:\"/themes\";s:13:\"twentyfifteen\";s:7:\"/themes\";s:14:\"twentyfourteen\";s:7:\"/themes\";s:15:\"twentyseventeen\";s:7:\"/themes\";s:13:\"twentysixteen\";s:7:\"/themes\";}', 'no');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `localwp_postmeta`
--

CREATE TABLE `localwp_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `localwp_postmeta`
--

INSERT INTO `localwp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(2, 4, '_edit_last', '1'),
(3, 4, '_edit_lock', '1521506060:1'),
(4, 5, '_wp_attached_file', '2018/03/planted.jpg'),
(5, 5, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:262;s:6:\"height\";i:192;s:4:\"file\";s:19:\"2018/03/planted.jpg\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"planted-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(6, 7, '_edit_last', '1'),
(7, 7, '_edit_lock', '1521506171:1'),
(10, 9, '_wp_attached_file', '2018/03/157509357.jpg'),
(11, 9, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:612;s:6:\"height\";i:180;s:4:\"file\";s:21:\"2018/03/157509357.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"157509357-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"157509357-300x88.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:88;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(12, 9, '_wp_attachment_image_alt', 'Liston Rojo'),
(13, 7, '_thumbnail_id', '9'),
(15, 11, '_wp_attached_file', '2018/03/descarga-1.jpg'),
(16, 11, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:327;s:6:\"height\";i:154;s:4:\"file\";s:22:\"2018/03/descarga-1.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"descarga-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:22:\"descarga-1-300x141.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:141;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(17, 12, '_wp_attached_file', '2018/03/descarga-2.jpg'),
(18, 12, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:318;s:6:\"height\";i:159;s:4:\"file\";s:22:\"2018/03/descarga-2.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"descarga-2-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:22:\"descarga-2-300x150.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(19, 13, '_wp_attached_file', '2018/03/descarga-3.jpg'),
(20, 13, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:262;s:6:\"height\";i:193;s:4:\"file\";s:22:\"2018/03/descarga-3.jpg\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"descarga-3-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(21, 14, '_wp_attached_file', '2018/03/descarga.jpg'),
(22, 14, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:318;s:6:\"height\";i:159;s:4:\"file\";s:20:\"2018/03/descarga.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"descarga-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"descarga-300x150.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(23, 10, '_edit_last', '1'),
(24, 10, '_edit_lock', '1521507111:1'),
(25, 10, '_wp_trash_meta_status', 'draft'),
(26, 10, '_wp_trash_meta_time', '1521507165'),
(27, 10, '_wp_desired_post_slug', ''),
(28, 16, '_edit_last', '1'),
(29, 16, '_edit_lock', '1521511358:1'),
(31, 19, '_wp_attached_file', '2018/03/audio.m4a'),
(32, 19, '_wp_attachment_metadata', 'a:16:{s:10:\"dataformat\";s:3:\"mp4\";s:5:\"codec\";s:19:\"ISO/IEC 14496-3 AAC\";s:11:\"sample_rate\";d:48000;s:8:\"channels\";i:2;s:15:\"bits_per_sample\";i:16;s:8:\"lossless\";b:0;s:11:\"channelmode\";s:6:\"stereo\";s:7:\"bitrate\";d:259210.44303797468;s:17:\"compression_ratio\";d:0.16875679885284808;s:10:\"fileformat\";s:3:\"mp4\";s:8:\"filesize\";i:54651;s:9:\"mime_type\";s:9:\"audio/mp4\";s:6:\"length\";i:2;s:16:\"length_formatted\";s:4:\"0:02\";s:6:\"artist\";s:0:\"\";s:5:\"album\";s:0:\"\";}'),
(33, 18, '_edit_last', '1'),
(34, 18, '_edit_lock', '1521511343:1'),
(36, 18, 'enclosure', 'http://localhost:8080/localwp.dev/wp-content/uploads/2018/03/audio.m4a\n54651\naudio/mpeg\n'),
(38, 22, '_edit_last', '1'),
(39, 22, '_edit_lock', '1521513861:1'),
(40, 24, '_edit_last', '1'),
(41, 24, '_edit_lock', '1521610454:1'),
(43, 26, '_edit_lock', '1521514433:1'),
(45, 27, '_customize_changeset_uuid', '7feb150a-8db5-46d2-9ca1-1fc905bc6986'),
(46, 26, '_wp_trash_meta_status', 'publish'),
(47, 26, '_wp_trash_meta_time', '1521514465'),
(48, 29, '_wp_trash_meta_status', 'publish'),
(49, 29, '_wp_trash_meta_time', '1521514770'),
(50, 30, '_edit_lock', '1521515761:1'),
(83, 30, '_wp_trash_meta_status', 'publish'),
(84, 30, '_wp_trash_meta_time', '1521515812'),
(93, 35, '_wp_trash_meta_status', 'publish'),
(94, 35, '_wp_trash_meta_time', '1521515871'),
(95, 37, '_edit_lock', '1521516244:1'),
(104, 37, '_wp_trash_meta_status', 'publish'),
(105, 37, '_wp_trash_meta_time', '1521516244'),
(106, 39, '_edit_lock', '1521516309:1'),
(115, 39, '_wp_trash_meta_status', 'publish'),
(116, 39, '_wp_trash_meta_time', '1521516345'),
(130, 18, '_wp_trash_meta_status', 'publish'),
(131, 18, '_wp_trash_meta_time', '1521607173'),
(132, 18, '_wp_desired_post_slug', 'podcast-for-ios-developers'),
(133, 16, '_wp_trash_meta_status', 'publish'),
(134, 16, '_wp_trash_meta_time', '1521607186'),
(135, 16, '_wp_desired_post_slug', 'pictures-of-tanks'),
(136, 7, '_wp_trash_meta_status', 'publish'),
(137, 7, '_wp_trash_meta_time', '1521607197'),
(138, 7, '_wp_desired_post_slug', '1st-post'),
(139, 1, '_wp_trash_meta_status', 'publish'),
(140, 1, '_wp_trash_meta_time', '1521607199'),
(141, 1, '_wp_desired_post_slug', 'hola-mundo'),
(142, 1, '_wp_trash_meta_comments_status', 'a:1:{i:1;s:1:\"1\";}'),
(143, 49, '_edit_last', '1'),
(144, 49, '_edit_lock', '1522794294:1'),
(150, 53, '_edit_last', '1'),
(151, 53, '_edit_lock', '1522778567:1'),
(155, 56, '_edit_last', '1'),
(156, 56, '_edit_lock', '1522778528:1'),
(160, 59, '_edit_last', '1'),
(161, 59, '_edit_lock', '1522778459:1'),
(165, 62, '_edit_last', '1'),
(166, 62, '_edit_lock', '1522778394:1'),
(171, 65, '_edit_last', '1'),
(172, 65, '_edit_lock', '1522730134:1'),
(176, 68, '_edit_last', '1'),
(177, 68, '_edit_lock', '1522778255:1'),
(181, 71, '_edit_last', '1'),
(182, 71, '_edit_lock', '1522778129:1'),
(186, 74, '_edit_last', '1'),
(187, 74, '_edit_lock', '1522732774:1'),
(193, 27, '_wp_trash_meta_status', 'publish'),
(194, 27, '_wp_trash_meta_time', '1521609076'),
(195, 27, '_wp_desired_post_slug', 'about-me'),
(199, 44, '_wp_trash_meta_status', 'publish'),
(200, 44, '_wp_trash_meta_time', '1521609076'),
(201, 44, '_wp_desired_post_slug', 'aviso-legal'),
(202, 22, '_wp_trash_meta_status', 'publish'),
(203, 22, '_wp_trash_meta_time', '1521609084'),
(204, 22, '_wp_desired_post_slug', 'home-page'),
(205, 43, '_wp_trash_meta_status', 'publish'),
(206, 43, '_wp_trash_meta_time', '1521609129'),
(207, 43, '_wp_desired_post_slug', 'inicio'),
(208, 45, '_wp_trash_meta_status', 'publish'),
(209, 45, '_wp_trash_meta_time', '1521609133'),
(210, 45, '_wp_desired_post_slug', 'politica-de-privacidad'),
(211, 80, '_wp_attached_file', '2018/03/layer-13.png'),
(212, 80, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:180;s:6:\"height\";i:180;s:4:\"file\";s:20:\"2018/03/layer-13.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"layer-13-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(213, 81, '_edit_lock', '1521609298:1'),
(214, 81, '_wp_trash_meta_status', 'publish'),
(215, 81, '_wp_trash_meta_time', '1521609294'),
(216, 82, '_edit_lock', '1521609502:1'),
(217, 82, '_wp_trash_meta_status', 'publish'),
(218, 82, '_wp_trash_meta_time', '1521609490'),
(219, 83, '_edit_lock', '1521609626:1'),
(220, 83, '_wp_trash_meta_status', 'publish'),
(221, 83, '_wp_trash_meta_time', '1521609625'),
(224, 86, '_edit_lock', '1521610356:1'),
(225, 87, '_menu_item_type', 'custom'),
(226, 87, '_menu_item_menu_item_parent', '0'),
(227, 87, '_menu_item_object_id', '87'),
(228, 87, '_menu_item_object', 'custom'),
(229, 87, '_menu_item_target', ''),
(230, 87, '_menu_item_classes', 'a:1:{i:0;s:8:\"linkedin\";}'),
(231, 87, '_menu_item_xfn', ''),
(232, 87, '_menu_item_url', ''),
(233, 88, '_menu_item_type', 'custom'),
(234, 88, '_menu_item_menu_item_parent', '0'),
(235, 88, '_menu_item_object_id', '88'),
(236, 88, '_menu_item_object', 'custom'),
(237, 88, '_menu_item_target', ''),
(238, 88, '_menu_item_classes', 'a:1:{i:0;s:7:\"twitter\";}'),
(239, 88, '_menu_item_xfn', ''),
(240, 88, '_menu_item_url', ''),
(241, 89, '_menu_item_type', 'custom'),
(242, 89, '_menu_item_menu_item_parent', '0'),
(243, 89, '_menu_item_object_id', '89'),
(244, 89, '_menu_item_object', 'custom'),
(245, 89, '_menu_item_target', ''),
(246, 89, '_menu_item_classes', 'a:1:{i:0;s:7:\"podcast\";}'),
(247, 89, '_menu_item_xfn', ''),
(248, 89, '_menu_item_url', ''),
(249, 86, '_wp_trash_meta_status', 'publish'),
(250, 86, '_wp_trash_meta_time', '1521610387'),
(251, 90, '_wp_trash_meta_status', 'publish'),
(252, 90, '_wp_trash_meta_time', '1521610421'),
(253, 24, '_wp_trash_meta_status', 'publish'),
(254, 24, '_wp_trash_meta_time', '1521610618'),
(255, 24, '_wp_desired_post_slug', 'blog'),
(256, 92, 'clever_contact_forms_to', 'iromero@cleverconsulting.net'),
(257, 92, 'clever_contact_forms_subject', 'Contacto desde WP Blog'),
(258, 92, 'clever_contact_forms_html', ''),
(259, 92, 'clever_contact_forms_body', ''),
(260, 92, '_edit_last', '1'),
(261, 92, 'clever_contact_forms_fields', 'a:4:{i:0;a:5:{s:4:\"type\";s:4:\"text\";s:4:\"name\";s:7:\"cc_name\";s:11:\"placeholder\";s:6:\"Nombre\";s:7:\"options\";s:0:\"\";s:8:\"required\";s:2:\"on\";}i:1;a:5:{s:4:\"type\";s:5:\"email\";s:4:\"name\";s:8:\"cc_email\";s:11:\"placeholder\";s:19:\"Correo electrónico\";s:7:\"options\";s:0:\"\";s:8:\"required\";s:2:\"on\";}i:2;a:5:{s:4:\"type\";s:8:\"textarea\";s:4:\"name\";s:10:\"cc_message\";s:11:\"placeholder\";s:7:\"Mensaje\";s:7:\"options\";s:0:\"\";s:8:\"required\";s:2:\"on\";}i:3;a:5:{s:4:\"type\";s:6:\"submit\";s:4:\"name\";s:9:\"cc_submit\";s:11:\"placeholder\";s:6:\"Enviar\";s:7:\"options\";s:0:\"\";s:8:\"required\";s:0:\"\";}}'),
(262, 92, '_edit_lock', '1522541115:1'),
(263, 94, '_menu_item_type', 'post_type'),
(264, 94, '_menu_item_menu_item_parent', '0'),
(265, 94, '_menu_item_object_id', '92'),
(266, 94, '_menu_item_object', 'clever_contact_forms'),
(267, 94, '_menu_item_target', ''),
(268, 94, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(269, 94, '_menu_item_xfn', ''),
(270, 94, '_menu_item_url', ''),
(271, 93, '_wp_trash_meta_status', 'publish'),
(272, 93, '_wp_trash_meta_time', '1521610921'),
(273, 95, '_wp_trash_meta_status', 'publish'),
(274, 95, '_wp_trash_meta_time', '1521612489'),
(283, 98, '_wp_trash_meta_status', 'publish'),
(284, 98, '_wp_trash_meta_time', '1521626379'),
(288, 99, '_edit_last', '1'),
(289, 99, '_edit_lock', '1522616051:1'),
(316, 109, '_edit_lock', '1521732852:1'),
(317, 109, '_wp_trash_meta_status', 'publish'),
(318, 109, '_wp_trash_meta_time', '1521732874'),
(319, 110, '_wp_trash_meta_status', 'publish'),
(320, 110, '_wp_trash_meta_time', '1521732911'),
(321, 111, '_wp_trash_meta_status', 'publish'),
(322, 111, '_wp_trash_meta_time', '1521732938'),
(323, 112, '_wp_trash_meta_status', 'publish'),
(324, 112, '_wp_trash_meta_time', '1521733250'),
(325, 113, '_edit_last', '1'),
(326, 113, '_edit_lock', '1522240753:1'),
(335, 114, '_wp_trash_meta_status', 'publish'),
(336, 114, '_wp_trash_meta_time', '1521736785'),
(337, 116, 'clever_contact_forms_to', 'iromero@cleverconsulting.net'),
(338, 116, 'clever_contact_forms_subject', 'Contacto desde WP Headet Blog'),
(339, 116, 'clever_contact_forms_html', '*cc_email*\r\n*cc_submit*'),
(340, 116, 'clever_contact_forms_body', '*cc_email*\r\n*cc_submit*'),
(341, 116, '_edit_last', '1'),
(342, 116, 'clever_contact_forms_fields', 'a:2:{i:0;a:5:{s:4:\"type\";s:5:\"email\";s:4:\"name\";s:8:\"cc_email\";s:11:\"placeholder\";s:19:\"Correo electrónico\";s:7:\"options\";s:0:\"\";s:8:\"required\";s:2:\"on\";}i:1;a:5:{s:4:\"type\";s:6:\"submit\";s:4:\"name\";s:9:\"cc_submit\";s:11:\"placeholder\";s:6:\"Enviar\";s:7:\"options\";s:0:\"\";s:8:\"required\";s:0:\"\";}}'),
(343, 116, '_edit_lock', '1522273320:1'),
(344, 117, '_edit_last', '1'),
(345, 117, '_edit_lock', '1522621615:1'),
(346, 119, '_wp_trash_meta_status', 'publish'),
(347, 119, '_wp_trash_meta_time', '1522018968'),
(348, 120, '_wp_attached_file', '2018/03/layer-21@2x.png'),
(349, 120, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:746;s:6:\"height\";i:450;s:4:\"file\";s:23:\"2018/03/layer-21@2x.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"layer-21@2x-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:23:\"layer-21@2x-300x181.png\";s:5:\"width\";i:300;s:6:\"height\";i:181;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(350, 74, '_thumbnail_id', '120'),
(351, 120, '_wp_attachment_image_alt', 'Organiza tu vida digital: consume el máximo contenido en el menor tiempo y optimiza en redes sociales'),
(352, 123, '_wp_attached_file', '2018/03/layer-25@2x.png'),
(353, 123, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:746;s:6:\"height\";i:448;s:4:\"file\";s:23:\"2018/03/layer-25@2x.png\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"layer-25@2x-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:23:\"layer-25@2x-300x180.png\";s:5:\"width\";i:300;s:6:\"height\";i:180;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"homepage-thumb\";a:4:{s:4:\"file\";s:23:\"layer-25@2x-373x225.png\";s:5:\"width\";i:373;s:6:\"height\";i:225;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(354, 65, '_thumbnail_id', '123'),
(355, 123, '_wp_attachment_image_alt', 'Cómo no perder un concurso público'),
(356, 128, '_wp_attached_file', '2018/03/layer-19-copy@2x.png'),
(357, 128, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:746;s:6:\"height\";i:450;s:4:\"file\";s:28:\"2018/03/layer-19-copy@2x.png\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:28:\"layer-19-copy@2x-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:28:\"layer-19-copy@2x-300x181.png\";s:5:\"width\";i:300;s:6:\"height\";i:181;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"homepage-thumb\";a:4:{s:4:\"file\";s:28:\"layer-19-copy@2x-373x225.png\";s:5:\"width\";i:373;s:6:\"height\";i:225;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(358, 71, '_thumbnail_id', '128'),
(359, 128, '_wp_attachment_image_alt', 'Reseña de los 42 libros que he leído en 2016'),
(361, 129, '_wp_attached_file', '2018/03/layer-23@2x.png'),
(362, 129, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:746;s:6:\"height\";i:450;s:4:\"file\";s:23:\"2018/03/layer-23@2x.png\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"layer-23@2x-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:23:\"layer-23@2x-300x181.png\";s:5:\"width\";i:300;s:6:\"height\";i:181;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"homepage-thumb\";a:4:{s:4:\"file\";s:23:\"layer-23@2x-373x225.png\";s:5:\"width\";i:373;s:6:\"height\";i:225;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(363, 68, '_thumbnail_id', '129'),
(364, 129, '_wp_attachment_image_alt', 'La forma más barata de mejorar tu producto o servicio'),
(367, 130, '_wp_attached_file', '2018/03/layer-27@2x.png'),
(368, 130, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:746;s:6:\"height\";i:448;s:4:\"file\";s:23:\"2018/03/layer-27@2x.png\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"layer-27@2x-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:23:\"layer-27@2x-300x180.png\";s:5:\"width\";i:300;s:6:\"height\";i:180;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"homepage-thumb\";a:4:{s:4:\"file\";s:23:\"layer-27@2x-373x225.png\";s:5:\"width\";i:373;s:6:\"height\";i:225;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(369, 62, '_thumbnail_id', '130'),
(370, 130, '_wp_attachment_image_alt', 'Por qué los detalles matan los proyectos'),
(372, 131, '_wp_attached_file', '2018/03/layer-29@2x.png'),
(373, 131, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:746;s:6:\"height\";i:448;s:4:\"file\";s:23:\"2018/03/layer-29@2x.png\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"layer-29@2x-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:23:\"layer-29@2x-300x180.png\";s:5:\"width\";i:300;s:6:\"height\";i:180;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"homepage-thumb\";a:4:{s:4:\"file\";s:23:\"layer-29@2x-373x225.png\";s:5:\"width\";i:373;s:6:\"height\";i:225;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(374, 59, '_thumbnail_id', '131'),
(375, 131, '_wp_attachment_image_alt', 'Consejo del fundador de Alibaba a jóvenes emprendedores'),
(377, 132, '_wp_attached_file', '2018/03/layer-31@2x.png'),
(378, 132, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:746;s:6:\"height\";i:450;s:4:\"file\";s:23:\"2018/03/layer-31@2x.png\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"layer-31@2x-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:23:\"layer-31@2x-300x181.png\";s:5:\"width\";i:300;s:6:\"height\";i:181;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"homepage-thumb\";a:4:{s:4:\"file\";s:23:\"layer-31@2x-373x225.png\";s:5:\"width\";i:373;s:6:\"height\";i:225;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(379, 56, '_thumbnail_id', '132'),
(380, 132, '_wp_attachment_image_alt', 'Cómo asesinar a tu peor enemigo'),
(382, 133, '_wp_attached_file', '2018/03/layer-33@2x.png'),
(383, 133, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:746;s:6:\"height\";i:450;s:4:\"file\";s:23:\"2018/03/layer-33@2x.png\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"layer-33@2x-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:23:\"layer-33@2x-300x181.png\";s:5:\"width\";i:300;s:6:\"height\";i:181;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"homepage-thumb\";a:4:{s:4:\"file\";s:23:\"layer-33@2x-373x225.png\";s:5:\"width\";i:373;s:6:\"height\";i:225;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(384, 53, '_thumbnail_id', '133'),
(385, 133, '_wp_attachment_image_alt', 'El peor tipo de emprendedor'),
(387, 134, '_wp_attached_file', '2018/03/layer-35@2x.png'),
(388, 134, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:746;s:6:\"height\";i:450;s:4:\"file\";s:23:\"2018/03/layer-35@2x.png\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"layer-35@2x-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:23:\"layer-35@2x-300x181.png\";s:5:\"width\";i:300;s:6:\"height\";i:181;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"homepage-thumb\";a:4:{s:4:\"file\";s:23:\"layer-35@2x-373x225.png\";s:5:\"width\";i:373;s:6:\"height\";i:225;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(389, 49, '_thumbnail_id', '134'),
(390, 134, '_wp_attachment_image_alt', 'Esta frase tiene cinco palabras');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `localwp_posts`
--

CREATE TABLE `localwp_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `localwp_posts`
--

INSERT INTO `localwp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2018-03-19 12:01:04', '2018-03-19 18:01:04', 'Bienvenido a WordPress. Esta es tu primera entrada. Edítala o bórrala, ¡y comienza a publicar!.', '¡Hola mundo!', '', 'trash', 'open', 'open', '', 'hola-mundo__trashed', '', '', '2018-03-20 22:39:59', '2018-03-21 04:39:59', '', 0, 'http://localhost:8080/localwp.dev/?p=1', 0, 'post', '', 1),
(4, 1, '2018-03-19 12:58:50', '2018-03-19 18:58:50', 'Word press practice on localhost\r\n\r\n[caption id=\"attachment_5\" align=\"alignnone\" width=\"262\"]<img class=\"size-full wp-image-5\" src=\"http://localhost:8080/localwp.dev/wp-content/uploads/2018/03/planted.jpg\" alt=\"\" width=\"262\" height=\"192\" /> planted tank[/caption]', 'About Me (israel)', '', 'publish', 'closed', 'closed', '', 'about-me-israel', '', '', '2018-03-21 00:06:58', '2018-03-21 06:06:58', '', 0, 'http://localhost:8080/localwp.dev/?page_id=4', 0, 'page', '', 0),
(5, 1, '2018-03-19 12:53:13', '2018-03-19 18:53:13', '', 'planted', 'planted tank', 'inherit', 'open', 'closed', '', 'planted', '', '', '2018-03-19 12:53:53', '2018-03-19 18:53:53', '', 4, 'http://localhost:8080/localwp.dev/wp-content/uploads/2018/03/planted.jpg', 0, 'attachment', 'image/jpeg', 0),
(6, 1, '2018-03-19 12:54:26', '2018-03-19 18:54:26', 'Word press practice on localhost\r\n\r\n[caption id=\"attachment_5\" align=\"alignnone\" width=\"262\"]<img class=\"size-full wp-image-5\" src=\"http://localhost:8080/localwp.dev/wp-content/uploads/2018/03/planted.jpg\" alt=\"\" width=\"262\" height=\"192\" /> planted tank[/caption]', 'About Me (israel)', '', 'inherit', 'closed', 'closed', '', '4-revision-v1', '', '', '2018-03-19 12:54:26', '2018-03-19 18:54:26', '', 4, 'http://localhost:8080/localwp.dev/2018/03/19/4-revision-v1/', 0, 'revision', '', 0),
(7, 1, '2018-03-19 13:02:50', '2018-03-19 19:02:50', 'I assume this is the post page so I wrtie my ist post here', '1st post', '', 'trash', 'open', 'open', '', '1st-post__trashed', '', '', '2018-03-20 22:39:58', '2018-03-21 04:39:58', '', 0, 'http://localhost:8080/localwp.dev/?p=7', 0, 'post', '', 0),
(8, 1, '2018-03-19 13:02:22', '2018-03-19 19:02:22', 'I assume this is the post page so I wrtie my ist post here', '1st post', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2018-03-19 13:02:22', '2018-03-19 19:02:22', '', 7, 'http://localhost:8080/localwp.dev/2018/03/19/7-revision-v1/', 0, 'revision', '', 0),
(9, 1, '2018-03-19 18:37:49', '2018-03-20 00:37:49', '', '157509357', '', 'inherit', 'open', 'closed', '', '157509357', '', '', '2018-03-19 18:38:04', '2018-03-20 00:38:04', '', 7, 'http://localhost:8080/localwp.dev/wp-content/uploads/2018/03/157509357.jpg', 0, 'attachment', 'image/jpeg', 0),
(10, 1, '2018-03-19 18:52:45', '2018-03-20 00:52:45', '[gallery columns=\"2\" size=\"medium\" ids=\"11,12,14,13\"]', 'Screenshots of a hobbie', '', 'trash', 'open', 'open', '', '__trashed', '', '', '2018-03-19 18:52:45', '2018-03-20 00:52:45', '', 0, 'http://localhost:8080/localwp.dev/?p=10', 0, 'post', '', 0),
(11, 1, '2018-03-19 18:46:57', '2018-03-20 00:46:57', '', 'descarga (1)', '', 'inherit', 'open', 'closed', '', 'descarga-1', '', '', '2018-03-19 18:46:57', '2018-03-20 00:46:57', '', 10, 'http://localhost:8080/localwp.dev/wp-content/uploads/2018/03/descarga-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(12, 1, '2018-03-19 18:46:58', '2018-03-20 00:46:58', '', 'descarga (2)', '', 'inherit', 'open', 'closed', '', 'descarga-2', '', '', '2018-03-19 18:46:58', '2018-03-20 00:46:58', '', 10, 'http://localhost:8080/localwp.dev/wp-content/uploads/2018/03/descarga-2.jpg', 0, 'attachment', 'image/jpeg', 0),
(13, 1, '2018-03-19 18:47:00', '2018-03-20 00:47:00', '', 'descarga (3)', '', 'inherit', 'open', 'closed', '', 'descarga-3', '', '', '2018-03-19 18:47:00', '2018-03-20 00:47:00', '', 10, 'http://localhost:8080/localwp.dev/wp-content/uploads/2018/03/descarga-3.jpg', 0, 'attachment', 'image/jpeg', 0),
(14, 1, '2018-03-19 18:47:01', '2018-03-20 00:47:01', '', 'descarga', '', 'inherit', 'open', 'closed', '', 'descarga', '', '', '2018-03-19 18:47:01', '2018-03-20 00:47:01', '', 10, 'http://localhost:8080/localwp.dev/wp-content/uploads/2018/03/descarga.jpg', 0, 'attachment', 'image/jpeg', 0),
(15, 1, '2018-03-19 18:52:45', '2018-03-20 00:52:45', '[gallery columns=\"2\" size=\"medium\" ids=\"11,12,14,13\"]', 'Screenshots of a hobbie', '', 'inherit', 'closed', 'closed', '', '10-revision-v1', '', '', '2018-03-19 18:52:45', '2018-03-20 00:52:45', '', 10, 'http://localhost:8080/localwp.dev/2018/03/19/10-revision-v1/', 0, 'revision', '', 0),
(16, 1, '2018-03-19 19:17:25', '2018-03-20 01:17:25', '[gallery columns=\"2\" size=\"medium\" link=\"none\" ids=\"14,13,12,11\"]', 'pictures of tanks', '', 'trash', 'open', 'open', '', 'pictures-of-tanks__trashed', '', '', '2018-03-20 22:39:47', '2018-03-21 04:39:47', '', 0, 'http://localhost:8080/localwp.dev/?p=16', 0, 'post', '', 0),
(17, 1, '2018-03-19 19:17:25', '2018-03-20 01:17:25', '[gallery columns=\"2\" size=\"medium\" link=\"none\" ids=\"14,13,12,11\"]', 'pictures of tanks', '', 'inherit', 'closed', 'closed', '', '16-revision-v1', '', '', '2018-03-19 19:17:25', '2018-03-20 01:17:25', '', 16, 'http://localhost:8080/localwp.dev/2018/03/19/16-revision-v1/', 0, 'revision', '', 0),
(18, 1, '2018-03-19 19:56:24', '2018-03-20 01:56:24', 'Audio Test Post\r\n\r\n[audio m4a=\"http://localhost:8080/localwp.dev/wp-content/uploads/2018/03/audio.m4a\"][/audio]\r\n\r\n&nbsp;', 'Podcast for iOS Developers', '', 'trash', 'open', 'open', '', 'podcast-for-ios-developers__trashed', '', '', '2018-03-20 22:39:33', '2018-03-21 04:39:33', '', 0, 'http://localhost:8080/localwp.dev/?p=18', 0, 'post', '', 0),
(19, 1, '2018-03-19 19:28:25', '2018-03-20 01:28:25', '\"audio\".', 'audio', '', 'inherit', 'open', 'closed', '', 'audio', '', '', '2018-03-19 19:28:36', '2018-03-20 01:28:36', '', 18, 'http://localhost:8080/localwp.dev/wp-content/uploads/2018/03/audio.m4a', 0, 'attachment', 'audio/mpeg', 0),
(20, 1, '2018-03-19 19:30:11', '2018-03-20 01:30:11', 'Audio Test Post\r\n\r\n[audio m4a=\"http://localhost:8080/localwp.dev/wp-content/uploads/2018/03/audio.m4a\"][/audio]\r\n\r\n&nbsp;', 'Podcast for iOS Developers', '', 'inherit', 'closed', 'closed', '', '18-revision-v1', '', '', '2018-03-19 19:30:11', '2018-03-20 01:30:11', '', 18, 'http://localhost:8080/localwp.dev/2018/03/19/18-revision-v1/', 0, 'revision', '', 0),
(22, 1, '2018-03-19 20:46:39', '2018-03-20 02:46:39', 'Sample of a Home Page.\r\n\r\nLorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo\r\n\r\nLorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo', 'Home Page', '', 'trash', 'closed', 'closed', '', 'home-page__trashed', '', '', '2018-03-20 23:11:28', '2018-03-21 05:11:28', '', 0, 'http://localhost:8080/localwp.dev/?page_id=22', 0, 'page', '', 0),
(23, 1, '2018-03-19 20:46:39', '2018-03-20 02:46:39', 'Sample of a Home Page.\r\n\r\nLorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo\r\n\r\nLorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo', 'Home Page', '', 'inherit', 'closed', 'closed', '', '22-revision-v1', '', '', '2018-03-19 20:46:39', '2018-03-20 02:46:39', '', 22, 'http://localhost:8080/localwp.dev/2018/03/19/22-revision-v1/', 0, 'revision', '', 0),
(24, 1, '2018-03-19 20:47:26', '2018-03-20 02:47:26', 'Contratame o cuentame algo\r\n\r\nEsta página deberá ser un formulario', 'Contacto', '', 'trash', 'closed', 'closed', '', 'blog__trashed', '', '', '2018-03-20 23:36:58', '2018-03-21 05:36:58', '', 0, 'http://localhost:8080/localwp.dev/?page_id=24', 0, 'page', '', 0),
(25, 1, '2018-03-19 20:47:26', '2018-03-20 02:47:26', 'News / Blog page', 'Blog', '', 'inherit', 'closed', 'closed', '', '24-revision-v1', '', '', '2018-03-19 20:47:26', '2018-03-20 02:47:26', '', 24, 'http://localhost:8080/localwp.dev/2018/03/19/24-revision-v1/', 0, 'revision', '', 0),
(26, 1, '2018-03-19 20:54:24', '2018-03-20 02:54:24', '{\n    \"show_on_front\": {\n        \"value\": \"page\",\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-03-20 02:53:53\"\n    },\n    \"page_on_front\": {\n        \"value\": \"4\",\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-03-20 02:54:24\"\n    },\n    \"page_for_posts\": {\n        \"value\": \"24\",\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-03-20 02:53:53\"\n    },\n    \"nav_menus_created_posts\": {\n        \"value\": [\n            27\n        ],\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-03-20 02:54:24\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '7feb150a-8db5-46d2-9ca1-1fc905bc6986', '', '', '2018-03-19 20:54:24', '2018-03-20 02:54:24', '', 0, 'http://localhost:8080/localwp.dev/?p=26', 0, 'customize_changeset', '', 0),
(27, 1, '2018-03-19 20:54:25', '2018-03-20 02:54:25', '', 'About me', '', 'trash', 'closed', 'closed', '', 'about-me__trashed', '', '', '2018-03-20 23:11:16', '2018-03-21 05:11:16', '', 0, 'http://localhost:8080/localwp.dev/?page_id=27', 0, 'page', '', 0),
(28, 1, '2018-03-19 20:54:25', '2018-03-20 02:54:25', '', 'About me', '', 'inherit', 'closed', 'closed', '', '27-revision-v1', '', '', '2018-03-19 20:54:25', '2018-03-20 02:54:25', '', 27, 'http://localhost:8080/localwp.dev/2018/03/19/27-revision-v1/', 0, 'revision', '', 0),
(29, 1, '2018-03-19 20:59:29', '2018-03-20 02:59:29', '{\n    \"page_on_front\": {\n        \"value\": \"22\",\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-03-20 02:59:29\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '4e52f4a3-6b4b-40f7-81ca-e35f7a8c6979', '', '', '2018-03-19 20:59:29', '2018-03-20 02:59:29', '', 0, 'http://localhost:8080/localwp.dev/2018/03/19/4e52f4a3-6b4b-40f7-81ca-e35f7a8c6979/', 0, 'customize_changeset', '', 0),
(30, 1, '2018-03-19 21:16:40', '2018-03-20 03:16:40', '{\n    \"twentyfifteen::nav_menu_locations[primary]\": {\n        \"value\": -1228837480,\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-03-20 03:12:45\"\n    },\n    \"nav_menu[-1228837480]\": {\n        \"value\": {\n            \"name\": \"Main Menu\",\n            \"description\": \"\",\n            \"parent\": 0,\n            \"auto_add\": false\n        },\n        \"type\": \"nav_menu\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-03-20 03:12:45\"\n    },\n    \"nav_menu_item[-1147607752]\": {\n        \"value\": {\n            \"object_id\": 0,\n            \"object\": \"\",\n            \"menu_item_parent\": 0,\n            \"position\": 2,\n            \"type\": \"custom\",\n            \"title\": \"Inicio\",\n            \"url\": \"http://localhost:8080/localwp.dev\",\n            \"target\": \"\",\n            \"attr_title\": \"\",\n            \"description\": \"\",\n            \"classes\": \"\",\n            \"xfn\": \"\",\n            \"status\": \"publish\",\n            \"original_title\": \"Inicio\",\n            \"nav_menu_term_id\": -1228837480,\n            \"_invalid\": false,\n            \"type_label\": \"Enlace Personalizado\"\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-03-20 03:12:45\"\n    },\n    \"nav_menu_item[-753857076]\": {\n        \"value\": false,\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-03-20 03:12:45\"\n    },\n    \"nav_menu_item[-1306662574]\": {\n        \"value\": false,\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-03-20 03:12:45\"\n    },\n    \"nav_menu_item[-272123484]\": {\n        \"value\": {\n            \"object_id\": 4,\n            \"object\": \"page\",\n            \"menu_item_parent\": 0,\n            \"position\": 3,\n            \"type\": \"post_type\",\n            \"title\": \"About Me (israel)\",\n            \"url\": \"http://localhost:8080/localwp.dev/about-me-israel/\",\n            \"target\": \"\",\n            \"attr_title\": \"\",\n            \"description\": \"\",\n            \"classes\": \"\",\n            \"xfn\": \"\",\n            \"status\": \"publish\",\n            \"original_title\": \"About Me (israel)\",\n            \"nav_menu_term_id\": -1228837480,\n            \"_invalid\": false,\n            \"type_label\": \"P\\u00e1gina\"\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-03-20 03:12:45\"\n    },\n    \"nav_menu_item[-1832504535]\": {\n        \"value\": {\n            \"object_id\": 27,\n            \"object\": \"page\",\n            \"menu_item_parent\": 0,\n            \"position\": 4,\n            \"type\": \"post_type\",\n            \"title\": \"About me\",\n            \"url\": \"http://localhost:8080/localwp.dev/about-me/\",\n            \"target\": \"\",\n            \"attr_title\": \"\",\n            \"description\": \"\",\n            \"classes\": \"\",\n            \"xfn\": \"\",\n            \"status\": \"publish\",\n            \"original_title\": \"About me\",\n            \"nav_menu_term_id\": -1228837480,\n            \"_invalid\": false,\n            \"type_label\": \"P\\u00e1gina\"\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-03-20 03:12:45\"\n    },\n    \"nav_menu_item[-1620932600]\": {\n        \"value\": {\n            \"object_id\": 22,\n            \"object\": \"page\",\n            \"menu_item_parent\": 0,\n            \"position\": 1,\n            \"type\": \"post_type\",\n            \"title\": \"Home Page\",\n            \"url\": \"http://localhost:8080/localwp.dev/\",\n            \"target\": \"\",\n            \"attr_title\": \"\",\n            \"description\": \"\",\n            \"classes\": \"\",\n            \"xfn\": \"\",\n            \"status\": \"publish\",\n            \"original_title\": \"Home Page\",\n            \"nav_menu_term_id\": -1228837480,\n            \"_invalid\": false,\n            \"type_label\": \"P\\u00e1gina\"\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-03-20 03:12:45\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', 'd5c53695-1434-48f7-b4a8-63d110e8c6f9', '', '', '2018-03-19 21:16:40', '2018-03-20 03:16:40', '', 0, 'http://localhost:8080/localwp.dev/?p=30', 0, 'customize_changeset', '', 0),
(35, 1, '2018-03-19 21:17:50', '2018-03-20 03:17:50', '{\n    \"nav_menu_item[-1340442535]\": {\n        \"value\": {\n            \"object_id\": 24,\n            \"object\": \"page\",\n            \"menu_item_parent\": 0,\n            \"position\": 5,\n            \"type\": \"post_type\",\n            \"title\": \"Blog\",\n            \"url\": \"http://localhost:8080/localwp.dev/blog/\",\n            \"target\": \"\",\n            \"attr_title\": \"\",\n            \"description\": \"\",\n            \"classes\": \"\",\n            \"xfn\": \"\",\n            \"status\": \"publish\",\n            \"original_title\": \"Blog\",\n            \"nav_menu_term_id\": 14,\n            \"_invalid\": false,\n            \"type_label\": \"P\\u00e1gina\"\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-03-20 03:17:50\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', 'c100b446-c515-4759-8fc4-045c9a43dda6', '', '', '2018-03-19 21:17:50', '2018-03-20 03:17:50', '', 0, 'http://localhost:8080/localwp.dev/2018/03/19/c100b446-c515-4759-8fc4-045c9a43dda6/', 0, 'customize_changeset', '', 0),
(37, 1, '2018-03-19 21:24:04', '2018-03-20 03:24:04', '{\n    \"twentyfifteen::nav_menu_locations[social]\": {\n        \"value\": 14,\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-03-20 03:22:55\"\n    },\n    \"nav_menu_item[-50808648]\": {\n        \"value\": {\n            \"object_id\": 0,\n            \"object\": \"custom\",\n            \"menu_item_parent\": 0,\n            \"position\": 6,\n            \"type\": \"custom\",\n            \"title\": \"facebook\",\n            \"url\": \"http://facebook.com\",\n            \"target\": \"\",\n            \"attr_title\": \"\",\n            \"description\": \"\",\n            \"classes\": \"\",\n            \"xfn\": \"\",\n            \"status\": \"publish\",\n            \"original_title\": \"facebook\",\n            \"nav_menu_term_id\": 14,\n            \"_invalid\": false,\n            \"type_label\": \"Enlace Personalizado\"\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-03-20 03:24:04\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '4f5b2b54-60aa-4020-9558-c61c39fe3ab0', '', '', '2018-03-19 21:24:04', '2018-03-20 03:24:04', '', 0, 'http://localhost:8080/localwp.dev/?p=37', 0, 'customize_changeset', '', 0),
(39, 1, '2018-03-19 21:25:41', '2018-03-20 03:25:41', '{\n    \"twentyfifteen::nav_menu_locations[social]\": {\n        \"value\": -1160407894,\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-03-20 03:25:41\"\n    },\n    \"nav_menu_item[38]\": {\n        \"value\": false,\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-03-20 03:25:08\"\n    },\n    \"nav_menu[-1160407894]\": {\n        \"value\": {\n            \"name\": \"social media menu\",\n            \"description\": \"\",\n            \"parent\": 0,\n            \"auto_add\": false\n        },\n        \"type\": \"nav_menu\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-03-20 03:25:41\"\n    },\n    \"nav_menu_item[-829828439]\": {\n        \"value\": {\n            \"object_id\": 0,\n            \"object\": \"custom\",\n            \"menu_item_parent\": 0,\n            \"position\": 1,\n            \"type\": \"custom\",\n            \"title\": \"facebook\",\n            \"url\": \"http://facebook.com\",\n            \"target\": \"\",\n            \"attr_title\": \"\",\n            \"description\": \"\",\n            \"classes\": \"\",\n            \"xfn\": \"\",\n            \"status\": \"publish\",\n            \"original_title\": \"facebook\",\n            \"nav_menu_term_id\": -1160407894,\n            \"_invalid\": false,\n            \"type_label\": \"Enlace Personalizado\"\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-03-20 03:25:41\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', 'a9d1a592-d9ee-4768-93b4-b3f5bf4bd6cf', '', '', '2018-03-19 21:25:41', '2018-03-20 03:25:41', '', 0, 'http://localhost:8080/localwp.dev/?p=39', 0, 'customize_changeset', '', 0),
(43, 1, '2018-03-20 22:09:41', '2018-03-21 04:09:41', '', 'Inicio', '', 'trash', 'closed', 'closed', '', 'inicio__trashed', '', '', '2018-03-20 23:12:13', '2018-03-21 05:12:13', '', 0, 'http://localhost:8080/localwp.dev/inicio/', 0, 'page', '', 0),
(44, 1, '2018-03-20 22:10:45', '2018-03-21 04:10:45', '', 'Aviso legal', '', 'trash', 'closed', 'closed', '', 'aviso-legal__trashed', '', '', '2018-03-20 23:11:16', '2018-03-21 05:11:16', '', 0, 'http://localhost:8080/localwp.dev/aviso-legal/', 0, 'page', '', 0),
(45, 1, '2018-03-20 22:10:47', '2018-03-21 04:10:47', '\r\n		<a id=\"cookiesinfo\" name=\"cookiesinfo\"></a>\r\n		<h2>Política de cookies</h2>\r\n		<p><strong>¿Qué es una cookie?</strong></p>\r\n		<p>Las cookies son unos pequeños archivos de texto que se instalan en tu ordenador, móvil o tableta y que contienen información sobre lo que has hecho en esta web.</p>\r\n		<p>Esta información se utiliza para cosas tan útiles como que no tengas que meter tu usuario y contraseña cada vez que entras en determinada página web, o para mostrarte publicidad de productos que has investigado anteriormente.</p>\r\n		<p><strong>¿Por qué usamos cookies?</strong></p>\r\n		<p>Las cookies nos ayudan a mejorar tu experiencia de navegación y a detectar mejoras para nuestra web.</p>\r\n		<p><strong>¿Qué cookies usamos y para qué sirven?</strong></p>\r\n		<p>En este sitio usamos cookies propias y de terceros:</p>\r\n		<div class=\"wrap\">\r\n			<table class=\"cookies-table\">\r\n				<thead>\r\n					<tr>\r\n						<th>Tipo de cookies</th>\r\n						<th>Instalador</th>\r\n						<th>Nombre</th>\r\n						<th>Expiración</th>\r\n						<th>Utilidad</th>\r\n					</tr>\r\n				</thead>\r\n				<tbody>\r\n					<!-- Wordpress -->\r\n					<tr>\r\n						<td rowspan=\"4\"><strong>Propias</strong></td>\r\n						<td rowspan=\"4\"><strong>Wordpress</strong></td>\r\n						<td>comment_author</td>\r\n						<td rowspan=\"3\">Temporal según navegador</td>\r\n						<td rowspan=\"4\">Guardan tus datos sólo si comentas un post para que no tengas que meterlos con cada nuevo comentario.</td>\r\n					</tr>\r\n					<tr>\r\n						<!--td></td-->\r\n						<!--td></td-->\r\n						<td>comment_author_email</td>\r\n						<!--td></td-->\r\n						<!--td></td-->\r\n					</tr>\r\n					<tr>\r\n						<!--td></td-->\r\n						<!--td></td-->\r\n						<td>comment_author_url</td>\r\n						<!--td></td-->\r\n						<!--td></td-->\r\n					</tr>\r\n					<tr>\r\n						<!--td></td-->\r\n						<!--td></td-->\r\n						<td>_qca</td>\r\n						<td>Sirve para analizar el tráfico de la web</td>\r\n						<!--td></td-->\r\n					</tr>\r\n					<!-- Google Analytics -->\r\n					<tr>\r\n						<td rowspan=\"8\"><strong>De terceros</strong></td>\r\n						<td rowspan=\"8\"><strong>Google Analytics</strong></td>\r\n						<td>_utma</td>\r\n						<td>2 a&ntilde;os</td>\r\n						<td rowspan=\"8\">Conectan con Google Analytics y sirven para conocer, de forma anónima, cómo has usado esta web</td>\r\n					</tr>\r\n					<tr>\r\n						<!--td></td-->\r\n						<!--td></td-->\r\n						<td>_utmb</td>\r\n						<td>30 minutos</td>\r\n						<!--td></td-->\r\n					</tr>\r\n					<tr>\r\n						<!--td></td-->\r\n						<!--td></td-->\r\n						<td>_utmc</td>\r\n						<td>Al finalizar sesión</td>\r\n						<!--td></td-->\r\n					</tr>\r\n					<tr>\r\n						<!--td></td-->\r\n						<!--td></td-->\r\n						<td>_utmv</td>\r\n						<td>6 meses</td>\r\n						<!--td></td-->\r\n					</tr>\r\n					<tr>\r\n						<!--td></td-->\r\n						<!--td></td-->\r\n						<td>_utmz</td>\r\n						<td>6 meses</td>\r\n						<!--td></td-->\r\n					</tr>\r\n					<tr>\r\n						<!--td></td-->\r\n						<!--td></td-->\r\n						<td>eu_cn</td>\r\n						<td>Permanente</td>\r\n						<!--td></td-->\r\n					</tr>\r\n					<tr>\r\n						<!--td></td-->\r\n						<!--td></td-->\r\n						<td>_lipt</td>\r\n						<td>1 mes</td>\r\n						<!--td></td-->\r\n					</tr>\r\n					<tr>\r\n						<!--td></td-->\r\n						<!--td></td-->\r\n						<td>_bcookie</td>\r\n						<td></td>\r\n						<!--td></td-->\r\n					</tr>\r\n				</tbody>\r\n			</table>\r\n		</div>\r\n		<p><strong>¿Qué pasa si desactivas las cookies?</strong></p>\r\n		<p>Si desactivas las cookies es posible que algunos de nuestros servicios no funcionen correctamente.</p>\r\n		<p><strong>Consentimiento</strong></p>\r\n		<p>Seguir navegando por esta web implica el consentimiento y aceptación de nuestras cookies.</p>\r\n		<p>Para rectificar sobre este consentimiento, sigue las instrucciones del siguiente apartado.</p>\r\n		<p><strong>¿Cómo cambiar la configuración o desactivar las cookies?</strong></p>\r\n		<p>Para cambiar, bloquear o eliminar las cookies entra en las opciones de configuración de tu navegador y sigue las instrucciones:</p>\r\n		<ul>\r\n			<li><p><strong>Internet Explorer:</strong> Herramientas -> Opciones de Internet -> Privacidad -> Configuración.</p>\r\n			<p>Si quieres más información visita el <a href=\"https://login.live.com/login.srf?wa=wsignin1.0&rpsnv=12&ct=1391519729&rver=6.1.6206.0&wp=LBI_SSL&wreply=https:%2F%2Flogin.live.com:443%2Flogin.srf%3Fwa%3Dwsignin1.0%26rpsnv%3D12%26rver%3D6.1.6206.0%26wp%3DLBI_SSL%26wreply%3Dhttps%253a%252f%252fredir.windows.microsoft.com%252fliveid%253fru%253des-es%25252finternet-explorer%25252fdelete-manage-cookies%26lc%3D1033%26id%3D285275%26cbcxt%3D%26mkt%3D&lc=1033&id=285275#ie=ie-11\">soporte de Microsoft</a> o la Ayuda del navegador.</p></li>\r\n			<li><p><strong>Firefox:</strong> Herramientas -> Opciones -> Privacidad -> Historial -> Configuración Personalizada.</p>\r\n			<p>Si quieres más información visita el <a href=\"http://support.mozilla.org/es/search?esab=a&q=cookies\">soporte de Mozilla</a> o la Ayuda del navegador.</p></li>\r\n			<li><p><strong>Chrome:</strong> Configuración -> Mostrar opciones avanzadas -> Privacidad -> Configuración de contenido.</p>\r\n			<p>Si quieres más información visita el <a href=\"https://support.google.com/chrome/answer/95647?hl=es\">soporte de Google</a> o la Ayuda del navegador.</p></li>\r\n			<li><p><strong>Safari:</strong> Preferencias -> Seguridad.</p>\r\n			<p>Si quieres más información visita el <a href=\"http://support.apple.com/kb/HT1677?viewlocale=es_ES&locale=es_ES\">soporte de Apple</a> o la Ayuda del navegador.</p></li>\r\n		</ul>\r\n		<p><strong>Si no usas ninguno de estos navegadores,</strong> selecciona en tu navegador la pestaña “Ayuda” y la opción “Cookies” para obtener información sobre la ubicación de la carpeta de cookies.</p>\r\n		<p><strong>Actualizaciones y cambios en nuestra política de cookies</strong></p>\r\n		<p>Esta política puede cambiar para cumplir con futuros cambios sobre la Ley de Cookies <a href=\"http://www.boe.es/boe/dias/2012/03/31/pdfs/BOE-A-2012-4442.pdf\">Real Decreto-ley 13/2012</a>, por eso te recomendamos revisar esta información periódicamente.</p>', 'Política de privacidad', '', 'trash', 'closed', 'closed', '', 'politica-de-privacidad__trashed', '', '', '2018-03-20 23:12:13', '2018-03-21 05:12:13', '', 0, 'http://localhost:8080/localwp.dev/politica-de-privacidad/', 0, 'page', '', 0),
(48, 1, '2018-03-20 22:39:59', '2018-03-21 04:39:59', 'Bienvenido a WordPress. Esta es tu primera entrada. Edítala o bórrala, ¡y comienza a publicar!.', '¡Hola mundo!', '', 'inherit', 'closed', 'closed', '', '1-revision-v1', '', '', '2018-03-20 22:39:59', '2018-03-21 04:39:59', '', 1, 'http://localhost:8080/localwp.dev/2018/03/20/1-revision-v1/', 0, 'revision', '', 0),
(49, 1, '2018-03-20 22:47:08', '2018-03-21 04:47:08', 'Esta frase tiene cinco palabras\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla', 'Esta frase tiene cinco palabras', '', 'publish', 'open', 'open', '', 'esta-frase-tiene-cinco-palabras', '', '', '2018-04-03 12:05:51', '2018-04-03 18:05:51', '', 0, 'http://localhost:8080/localwp.dev/?p=49', 0, 'post', '', 0),
(52, 1, '2018-03-20 22:47:08', '2018-03-21 04:47:08', '<img class=\"alignnone size-medium wp-image-51\" src=\"http://localhost:8080/localwp.dev/wp-content/uploads/2018/03/layer-35-300x181.png\" alt=\"\" width=\"300\" height=\"181\" />\r\n\r\nEsta frase tiene cinco palabras\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla', 'Esta frase tiene cinco palabras', '', 'inherit', 'closed', 'closed', '', '49-revision-v1', '', '', '2018-03-20 22:47:08', '2018-03-21 04:47:08', '', 49, 'http://localhost:8080/localwp.dev/2018/03/20/49-revision-v1/', 0, 'revision', '', 0),
(53, 1, '2018-03-20 22:50:40', '2018-03-21 04:50:40', 'El peor tipo de emprendedor\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla', 'El peor tipo de emprendedor', '', 'publish', 'open', 'open', '', 'el-peor-tipo-de-emprendedor', '', '', '2018-04-03 12:05:02', '2018-04-03 18:05:02', '', 0, 'http://localhost:8080/localwp.dev/?p=53', 0, 'post', '', 0),
(55, 1, '2018-03-20 22:50:40', '2018-03-21 04:50:40', '<img class=\"alignnone size-medium wp-image-54\" src=\"http://localhost:8080/localwp.dev/wp-content/uploads/2018/03/layer-33-300x181.png\" alt=\"\" width=\"300\" height=\"181\" />\r\n\r\nEl peor tipo de emprendedor\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla', 'El peor tipo de emprendedor', '', 'inherit', 'closed', 'closed', '', '53-revision-v1', '', '', '2018-03-20 22:50:40', '2018-03-21 04:50:40', '', 53, 'http://localhost:8080/localwp.dev/2018/03/20/53-revision-v1/', 0, 'revision', '', 0),
(56, 1, '2018-03-20 22:53:26', '2018-03-21 04:53:26', 'Cómo asesinar a tu peor enemigo\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla', 'Cómo asesinar a tu peor enemigo', '', 'publish', 'open', 'open', '', 'como-asesinar-a-tu-peor-enemigo', '', '', '2018-04-03 12:04:20', '2018-04-03 18:04:20', '', 0, 'http://localhost:8080/localwp.dev/?p=56', 0, 'post', '', 0),
(58, 1, '2018-03-20 22:53:26', '2018-03-21 04:53:26', '<img class=\"alignnone size-medium wp-image-57\" src=\"http://localhost:8080/localwp.dev/wp-content/uploads/2018/03/layer-31-300x181.png\" alt=\"\" width=\"300\" height=\"181\" />\r\n\r\nCómo asesinar a tu peor enemigo\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla', 'Cómo asesinar a tu peor enemigo', '', 'inherit', 'closed', 'closed', '', '56-revision-v1', '', '', '2018-03-20 22:53:26', '2018-03-21 04:53:26', '', 56, 'http://localhost:8080/localwp.dev/2018/03/20/56-revision-v1/', 0, 'revision', '', 0),
(59, 1, '2018-03-20 22:55:09', '2018-03-21 04:55:09', 'Consejo del fundador de Alibaba a jóvenes emprendedores\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla', 'Consejo del fundador de Alibaba a jóvenes emprendedores', '', 'publish', 'open', 'open', '', 'consejo-del-fundador-de-alibaba-a-jovenes-emprendedores', '', '', '2018-04-03 12:03:15', '2018-04-03 18:03:15', '', 0, 'http://localhost:8080/localwp.dev/?p=59', 0, 'post', '', 0),
(61, 1, '2018-03-20 22:55:09', '2018-03-21 04:55:09', '<img class=\"alignnone size-medium wp-image-60\" src=\"http://localhost:8080/localwp.dev/wp-content/uploads/2018/03/layer-29-300x180.png\" alt=\"\" width=\"300\" height=\"180\" />\r\n\r\nConsejo del fundador de Alibaba a jóvenes emprendedores\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla', 'Consejo del fundador de Alibaba a jóvenes emprendedores', '', 'inherit', 'closed', 'closed', '', '59-revision-v1', '', '', '2018-03-20 22:55:09', '2018-03-21 04:55:09', '', 59, 'http://localhost:8080/localwp.dev/2018/03/20/59-revision-v1/', 0, 'revision', '', 0),
(62, 1, '2018-03-20 22:56:57', '2018-03-21 04:56:57', 'Por qué los detalles matan los proyectos\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla', 'Por qué los detalles matan los proyectos', '', 'publish', 'open', 'open', '', 'por-que-los-detalles-matan-los-proyectos', '', '', '2018-04-03 12:01:46', '2018-04-03 18:01:46', '', 0, 'http://localhost:8080/localwp.dev/?p=62', 0, 'post', '', 0),
(64, 1, '2018-03-20 22:56:57', '2018-03-21 04:56:57', '<img class=\"alignnone size-medium wp-image-63\" src=\"http://localhost:8080/localwp.dev/wp-content/uploads/2018/03/layer-27-300x180.png\" alt=\"\" width=\"300\" height=\"180\" />\r\n\r\nPor qué los detalles matan los proyectos\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla', 'Por qué los detalles matan los proyectos', '', 'inherit', 'closed', 'closed', '', '62-revision-v1', '', '', '2018-03-20 22:56:57', '2018-03-21 04:56:57', '', 62, 'http://localhost:8080/localwp.dev/2018/03/20/62-revision-v1/', 0, 'revision', '', 0),
(65, 1, '2018-03-20 22:59:59', '2018-03-21 04:59:59', '<h1>No participes en concursos que no hayas contribuido a lanzar.</h1>\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla', 'Cómo no perder un concurso público', '', 'publish', 'open', 'open', '', 'como-no-perder-un-concurso-publico', '', '', '2018-04-02 22:30:24', '2018-04-03 04:30:24', '', 0, 'http://localhost:8080/localwp.dev/?p=65', 0, 'post', '', 0),
(67, 1, '2018-03-20 22:59:59', '2018-03-21 04:59:59', '<img class=\"alignnone size-medium wp-image-66\" src=\"http://localhost:8080/localwp.dev/wp-content/uploads/2018/03/layer-25-300x180.png\" alt=\"\" width=\"300\" height=\"180\" />\r\n\r\nCómo no perder un concurso público\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla', 'Cómo no perder un concurso público', '', 'inherit', 'closed', 'closed', '', '65-revision-v1', '', '', '2018-03-20 22:59:59', '2018-03-21 04:59:59', '', 65, 'http://localhost:8080/localwp.dev/2018/03/20/65-revision-v1/', 0, 'revision', '', 0),
(68, 1, '2018-03-20 23:02:57', '2018-03-21 05:02:57', 'La forma más barata de mejorar tu producto o servicio\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla', 'La forma más barata de mejorar tu producto o servicio', '', 'publish', 'open', 'open', '', 'la-forma-mas-barata-de-mejorar-tu-producto-o-servicio', '', '', '2018-04-03 11:59:36', '2018-04-03 17:59:36', '', 0, 'http://localhost:8080/localwp.dev/?p=68', 0, 'post', '', 0),
(70, 1, '2018-03-20 23:02:57', '2018-03-21 05:02:57', '<img class=\"alignnone size-medium wp-image-69\" src=\"http://localhost:8080/localwp.dev/wp-content/uploads/2018/03/layer-23-300x181.png\" alt=\"\" width=\"300\" height=\"181\" />\r\n\r\nLa forma más barata de mejorar tu producto o servicio\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla', 'La forma más barata de mejorar tu producto o servicio', '', 'inherit', 'closed', 'closed', '', '68-revision-v1', '', '', '2018-03-20 23:02:57', '2018-03-21 05:02:57', '', 68, 'http://localhost:8080/localwp.dev/2018/03/20/68-revision-v1/', 0, 'revision', '', 0),
(71, 1, '2018-03-20 23:05:22', '2018-03-21 05:05:22', 'Reseña de los 42 libros que he leído en 2016\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla', 'Reseña de los 42 libros que he leído en 2016', '', 'publish', 'open', 'open', '', 'resena-de-los-42-libros-que-he-leido-en-2016', '', '', '2018-04-03 11:57:08', '2018-04-03 17:57:08', '', 0, 'http://localhost:8080/localwp.dev/?p=71', 0, 'post', '', 0),
(73, 1, '2018-03-20 23:05:22', '2018-03-21 05:05:22', '<img class=\"alignnone size-medium wp-image-72\" src=\"http://localhost:8080/localwp.dev/wp-content/uploads/2018/03/layer-19-copy-300x181.png\" alt=\"\" width=\"300\" height=\"181\" />\r\n\r\nReseña de los 42 libros que he leído en 2016\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla', 'Reseña de los 42 libros que he leído en 2016', '', 'inherit', 'closed', 'closed', '', '71-revision-v1', '', '', '2018-03-20 23:05:22', '2018-03-21 05:05:22', '', 71, 'http://localhost:8080/localwp.dev/2018/03/20/71-revision-v1/', 0, 'revision', '', 0),
(74, 1, '2018-03-20 23:08:20', '2018-03-21 05:08:20', '<h1>Organiza tu vida digital: consume el máximo contenido en el menor tiempo y optimiza en redes sociales</h1>\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla', 'Organiza tu vida digital: consume el máximo contenido en el menor tiempo y optimiza en redes sociales', '', 'publish', 'open', 'open', '', 'organiza-tu-vida-digital-consume-el-maximo-contenido-en-el-menor-tiempo-y-optimiza-en-redes-sociales', '', '', '2018-04-02 22:38:15', '2018-04-03 04:38:15', '', 0, 'http://localhost:8080/localwp.dev/?p=74', 0, 'post', '', 0),
(76, 1, '2018-03-20 23:08:20', '2018-03-21 05:08:20', '<img class=\"alignnone size-medium wp-image-75\" src=\"http://localhost:8080/localwp.dev/wp-content/uploads/2018/03/layer-21-300x181.png\" alt=\"\" width=\"300\" height=\"181\" />\r\n\r\nOrganiza tu vida digital: consume el máximo contenido en el menor tiempo y optimiza en redes sociales\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla', 'Organiza tu vida digital: consume el máximo contenido en el menor tiempo y optimiza en redes sociales', '', 'inherit', 'closed', 'closed', '', '74-revision-v1', '', '', '2018-03-20 23:08:20', '2018-03-21 05:08:20', '', 74, 'http://localhost:8080/localwp.dev/2018/03/20/74-revision-v1/', 0, 'revision', '', 0),
(77, 1, '2018-03-20 23:11:16', '2018-03-21 05:11:16', '', 'Aviso legal', '', 'inherit', 'closed', 'closed', '', '44-revision-v1', '', '', '2018-03-20 23:11:16', '2018-03-21 05:11:16', '', 44, 'http://localhost:8080/localwp.dev/2018/03/20/44-revision-v1/', 0, 'revision', '', 0),
(78, 1, '2018-03-20 23:12:13', '2018-03-21 05:12:13', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '43-revision-v1', '', '', '2018-03-20 23:12:13', '2018-03-21 05:12:13', '', 43, 'http://localhost:8080/localwp.dev/2018/03/20/43-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `localwp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(79, 1, '2018-03-20 23:12:13', '2018-03-21 05:12:13', '\r\n		<a id=\"cookiesinfo\" name=\"cookiesinfo\"></a>\r\n		<h2>Política de cookies</h2>\r\n		<p><strong>¿Qué es una cookie?</strong></p>\r\n		<p>Las cookies son unos pequeños archivos de texto que se instalan en tu ordenador, móvil o tableta y que contienen información sobre lo que has hecho en esta web.</p>\r\n		<p>Esta información se utiliza para cosas tan útiles como que no tengas que meter tu usuario y contraseña cada vez que entras en determinada página web, o para mostrarte publicidad de productos que has investigado anteriormente.</p>\r\n		<p><strong>¿Por qué usamos cookies?</strong></p>\r\n		<p>Las cookies nos ayudan a mejorar tu experiencia de navegación y a detectar mejoras para nuestra web.</p>\r\n		<p><strong>¿Qué cookies usamos y para qué sirven?</strong></p>\r\n		<p>En este sitio usamos cookies propias y de terceros:</p>\r\n		<div class=\"wrap\">\r\n			<table class=\"cookies-table\">\r\n				<thead>\r\n					<tr>\r\n						<th>Tipo de cookies</th>\r\n						<th>Instalador</th>\r\n						<th>Nombre</th>\r\n						<th>Expiración</th>\r\n						<th>Utilidad</th>\r\n					</tr>\r\n				</thead>\r\n				<tbody>\r\n					<!-- Wordpress -->\r\n					<tr>\r\n						<td rowspan=\"4\"><strong>Propias</strong></td>\r\n						<td rowspan=\"4\"><strong>Wordpress</strong></td>\r\n						<td>comment_author</td>\r\n						<td rowspan=\"3\">Temporal según navegador</td>\r\n						<td rowspan=\"4\">Guardan tus datos sólo si comentas un post para que no tengas que meterlos con cada nuevo comentario.</td>\r\n					</tr>\r\n					<tr>\r\n						<!--td></td-->\r\n						<!--td></td-->\r\n						<td>comment_author_email</td>\r\n						<!--td></td-->\r\n						<!--td></td-->\r\n					</tr>\r\n					<tr>\r\n						<!--td></td-->\r\n						<!--td></td-->\r\n						<td>comment_author_url</td>\r\n						<!--td></td-->\r\n						<!--td></td-->\r\n					</tr>\r\n					<tr>\r\n						<!--td></td-->\r\n						<!--td></td-->\r\n						<td>_qca</td>\r\n						<td>Sirve para analizar el tráfico de la web</td>\r\n						<!--td></td-->\r\n					</tr>\r\n					<!-- Google Analytics -->\r\n					<tr>\r\n						<td rowspan=\"8\"><strong>De terceros</strong></td>\r\n						<td rowspan=\"8\"><strong>Google Analytics</strong></td>\r\n						<td>_utma</td>\r\n						<td>2 a&ntilde;os</td>\r\n						<td rowspan=\"8\">Conectan con Google Analytics y sirven para conocer, de forma anónima, cómo has usado esta web</td>\r\n					</tr>\r\n					<tr>\r\n						<!--td></td-->\r\n						<!--td></td-->\r\n						<td>_utmb</td>\r\n						<td>30 minutos</td>\r\n						<!--td></td-->\r\n					</tr>\r\n					<tr>\r\n						<!--td></td-->\r\n						<!--td></td-->\r\n						<td>_utmc</td>\r\n						<td>Al finalizar sesión</td>\r\n						<!--td></td-->\r\n					</tr>\r\n					<tr>\r\n						<!--td></td-->\r\n						<!--td></td-->\r\n						<td>_utmv</td>\r\n						<td>6 meses</td>\r\n						<!--td></td-->\r\n					</tr>\r\n					<tr>\r\n						<!--td></td-->\r\n						<!--td></td-->\r\n						<td>_utmz</td>\r\n						<td>6 meses</td>\r\n						<!--td></td-->\r\n					</tr>\r\n					<tr>\r\n						<!--td></td-->\r\n						<!--td></td-->\r\n						<td>eu_cn</td>\r\n						<td>Permanente</td>\r\n						<!--td></td-->\r\n					</tr>\r\n					<tr>\r\n						<!--td></td-->\r\n						<!--td></td-->\r\n						<td>_lipt</td>\r\n						<td>1 mes</td>\r\n						<!--td></td-->\r\n					</tr>\r\n					<tr>\r\n						<!--td></td-->\r\n						<!--td></td-->\r\n						<td>_bcookie</td>\r\n						<td></td>\r\n						<!--td></td-->\r\n					</tr>\r\n				</tbody>\r\n			</table>\r\n		</div>\r\n		<p><strong>¿Qué pasa si desactivas las cookies?</strong></p>\r\n		<p>Si desactivas las cookies es posible que algunos de nuestros servicios no funcionen correctamente.</p>\r\n		<p><strong>Consentimiento</strong></p>\r\n		<p>Seguir navegando por esta web implica el consentimiento y aceptación de nuestras cookies.</p>\r\n		<p>Para rectificar sobre este consentimiento, sigue las instrucciones del siguiente apartado.</p>\r\n		<p><strong>¿Cómo cambiar la configuración o desactivar las cookies?</strong></p>\r\n		<p>Para cambiar, bloquear o eliminar las cookies entra en las opciones de configuración de tu navegador y sigue las instrucciones:</p>\r\n		<ul>\r\n			<li><p><strong>Internet Explorer:</strong> Herramientas -> Opciones de Internet -> Privacidad -> Configuración.</p>\r\n			<p>Si quieres más información visita el <a href=\"https://login.live.com/login.srf?wa=wsignin1.0&rpsnv=12&ct=1391519729&rver=6.1.6206.0&wp=LBI_SSL&wreply=https:%2F%2Flogin.live.com:443%2Flogin.srf%3Fwa%3Dwsignin1.0%26rpsnv%3D12%26rver%3D6.1.6206.0%26wp%3DLBI_SSL%26wreply%3Dhttps%253a%252f%252fredir.windows.microsoft.com%252fliveid%253fru%253des-es%25252finternet-explorer%25252fdelete-manage-cookies%26lc%3D1033%26id%3D285275%26cbcxt%3D%26mkt%3D&lc=1033&id=285275#ie=ie-11\">soporte de Microsoft</a> o la Ayuda del navegador.</p></li>\r\n			<li><p><strong>Firefox:</strong> Herramientas -> Opciones -> Privacidad -> Historial -> Configuración Personalizada.</p>\r\n			<p>Si quieres más información visita el <a href=\"http://support.mozilla.org/es/search?esab=a&q=cookies\">soporte de Mozilla</a> o la Ayuda del navegador.</p></li>\r\n			<li><p><strong>Chrome:</strong> Configuración -> Mostrar opciones avanzadas -> Privacidad -> Configuración de contenido.</p>\r\n			<p>Si quieres más información visita el <a href=\"https://support.google.com/chrome/answer/95647?hl=es\">soporte de Google</a> o la Ayuda del navegador.</p></li>\r\n			<li><p><strong>Safari:</strong> Preferencias -> Seguridad.</p>\r\n			<p>Si quieres más información visita el <a href=\"http://support.apple.com/kb/HT1677?viewlocale=es_ES&locale=es_ES\">soporte de Apple</a> o la Ayuda del navegador.</p></li>\r\n		</ul>\r\n		<p><strong>Si no usas ninguno de estos navegadores,</strong> selecciona en tu navegador la pestaña “Ayuda” y la opción “Cookies” para obtener información sobre la ubicación de la carpeta de cookies.</p>\r\n		<p><strong>Actualizaciones y cambios en nuestra política de cookies</strong></p>\r\n		<p>Esta política puede cambiar para cumplir con futuros cambios sobre la Ley de Cookies <a href=\"http://www.boe.es/boe/dias/2012/03/31/pdfs/BOE-A-2012-4442.pdf\">Real Decreto-ley 13/2012</a>, por eso te recomendamos revisar esta información periódicamente.</p>', 'Política de privacidad', '', 'inherit', 'closed', 'closed', '', '45-revision-v1', '', '', '2018-03-20 23:12:13', '2018-03-21 05:12:13', '', 45, 'http://localhost:8080/localwp.dev/2018/03/20/45-revision-v1/', 0, 'revision', '', 0),
(80, 1, '2018-03-20 23:14:12', '2018-03-21 05:14:12', '', 'layer-13', '', 'inherit', 'open', 'closed', '', 'layer-13', '', '', '2018-03-20 23:14:12', '2018-03-21 05:14:12', '', 0, 'http://localhost:8080/localwp.dev/wp-content/uploads/2018/03/layer-13.png', 0, 'attachment', 'image/png', 0),
(81, 1, '2018-03-20 23:14:53', '2018-03-21 05:14:53', '{\n    \"blogname\": {\n        \"value\": \"Luis Monge Malo\",\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-03-21 05:14:25\"\n    },\n    \"blogdescription\": {\n        \"value\": \"Erpiciatis unde omnis iste natus error sit voluptatem accusanti\",\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-03-21 05:14:25\"\n    },\n    \"site_icon\": {\n        \"value\": 80,\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-03-21 05:14:53\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '42a4a8d3-5a1a-42a2-9722-ca75b37795ed', '', '', '2018-03-20 23:14:53', '2018-03-21 05:14:53', '', 0, 'http://localhost:8080/localwp.dev/?p=81', 0, 'customize_changeset', '', 0),
(82, 1, '2018-03-20 23:17:44', '2018-03-21 05:17:44', '{\n    \"nav_menu_item[31]\": {\n        \"value\": {\n            \"menu_item_parent\": 0,\n            \"object_id\": 31,\n            \"object\": \"custom\",\n            \"type\": \"custom\",\n            \"type_label\": \"Enlace Personalizado\",\n            \"title\": \"LinkedIn\",\n            \"url\": \"https://www.linkedin.com/uas/login\",\n            \"target\": \"\",\n            \"attr_title\": \"\",\n            \"description\": \"\",\n            \"classes\": \"LinkedIn\",\n            \"xfn\": \"\",\n            \"nav_menu_term_id\": 14,\n            \"position\": 2,\n            \"status\": \"publish\",\n            \"original_title\": \"\",\n            \"_invalid\": false\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-03-21 05:17:44\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '9bd3643f-7a39-4935-9bc1-d15731f8b27c', '', '', '2018-03-20 23:17:44', '2018-03-21 05:17:44', '', 0, 'http://localhost:8080/localwp.dev/?p=82', 0, 'customize_changeset', '', 0),
(83, 1, '2018-03-20 23:19:38', '2018-03-21 05:19:38', '{\n    \"nav_menu_item[34]\": {\n        \"value\": false,\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-03-21 05:18:56\"\n    },\n    \"nav_menu_item[32]\": {\n        \"value\": false,\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-03-21 05:18:56\"\n    },\n    \"nav_menu_item[33]\": {\n        \"value\": {\n            \"menu_item_parent\": 0,\n            \"object_id\": 27,\n            \"object\": \"page\",\n            \"type\": \"post_type\",\n            \"type_label\": \"P\\u00e1gina\",\n            \"_invalid\": true,\n            \"url\": \"http://localhost:8080/localwp.dev/about-me__trashed/\",\n            \"title\": \"Seguir\",\n            \"target\": \"\",\n            \"attr_title\": \"\",\n            \"description\": \"\",\n            \"classes\": \"Seguir\",\n            \"xfn\": \"\",\n            \"nav_menu_term_id\": 14,\n            \"position\": 4,\n            \"status\": \"publish\",\n            \"original_title\": \"About me\"\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-03-21 05:19:38\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', 'bbccda41-31d6-4ae9-9a70-b28748d63376', '', '', '2018-03-20 23:19:38', '2018-03-21 05:19:38', '', 0, 'http://localhost:8080/localwp.dev/?p=83', 0, 'customize_changeset', '', 0),
(85, 1, '2018-03-20 23:26:15', '2018-03-21 05:26:15', '{\n    \"cleverconsulting-chanchito-base-aacef1806d34::nav_menu_locations[main_menu]\": {\n        \"value\": 0,\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-03-21 05:26:15\"\n    },\n    \"nav_menu[14]\": {\n        \"value\": false,\n        \"type\": \"nav_menu\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-03-21 05:26:15\"\n    },\n    \"nav_menu[16]\": {\n        \"value\": false,\n        \"type\": \"nav_menu\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-03-21 05:26:15\"\n    },\n    \"nav_menu[15]\": {\n        \"value\": false,\n        \"type\": \"nav_menu\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-03-21 05:26:15\"\n    }\n}', '', '', 'publish', 'closed', 'closed', '', 'a57cbee0-ea51-4c8a-a0a0-9418268e46a2', '', '', '2018-03-20 23:26:15', '2018-03-21 05:26:15', '', 0, 'http://localhost:8080/localwp.dev/2018/03/20/a57cbee0-ea51-4c8a-a0a0-9418268e46a2/', 0, 'customize_changeset', '', 0),
(86, 1, '2018-03-20 23:32:51', '2018-03-21 05:32:51', '{\n    \"cleverconsulting-chanchito-base-aacef1806d34::nav_menu_locations[main_menu]\": {\n        \"value\": -1358043554,\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-03-21 05:29:57\"\n    },\n    \"nav_menu[14]\": {\n        \"value\": false,\n        \"type\": \"nav_menu\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-03-21 05:28:57\"\n    },\n    \"nav_menu[16]\": {\n        \"value\": false,\n        \"type\": \"nav_menu\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-03-21 05:28:57\"\n    },\n    \"nav_menu[15]\": {\n        \"value\": false,\n        \"type\": \"nav_menu\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-03-21 05:28:57\"\n    },\n    \"nav_menu[-1358043554]\": {\n        \"value\": {\n            \"name\": \"mainMenu\",\n            \"description\": \"\",\n            \"parent\": 0,\n            \"auto_add\": false\n        },\n        \"type\": \"nav_menu\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-03-21 05:29:57\"\n    },\n    \"nav_menu_item[-662207327]\": {\n        \"value\": {\n            \"object_id\": 0,\n            \"object\": \"custom\",\n            \"menu_item_parent\": 0,\n            \"position\": 1,\n            \"type\": \"custom\",\n            \"title\": \"linkedIn\",\n            \"url\": \"https://www.linkedin.com/uas/login\",\n            \"target\": \"\",\n            \"attr_title\": \"\",\n            \"description\": \"\",\n            \"classes\": \"linkedin\",\n            \"xfn\": \"\",\n            \"status\": \"publish\",\n            \"original_title\": \"linkedIn\",\n            \"nav_menu_term_id\": -1358043554,\n            \"_invalid\": false,\n            \"type_label\": \"Enlace Personalizado\"\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-03-21 05:32:51\"\n    },\n    \"nav_menu_item[-1332383398]\": {\n        \"value\": {\n            \"object_id\": 0,\n            \"object\": \"custom\",\n            \"menu_item_parent\": 0,\n            \"position\": 2,\n            \"type\": \"custom\",\n            \"title\": \"Sigueme\",\n            \"url\": \"https://twitter.com/?lang=es/\",\n            \"target\": \"\",\n            \"attr_title\": \"\",\n            \"description\": \"\",\n            \"classes\": \"twitter\",\n            \"xfn\": \"\",\n            \"status\": \"publish\",\n            \"original_title\": \"Sigueme\",\n            \"nav_menu_term_id\": -1358043554,\n            \"_invalid\": false,\n            \"type_label\": \"Enlace Personalizado\"\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-03-21 05:32:51\"\n    },\n    \"nav_menu_item[-1212112577]\": {\n        \"value\": {\n            \"object_id\": 0,\n            \"object\": \"\",\n            \"menu_item_parent\": 0,\n            \"position\": 3,\n            \"type\": \"custom\",\n            \"title\": \"Podcast\",\n            \"url\": \"http://localhost:8080/localwp.dev\",\n            \"target\": \"\",\n            \"attr_title\": \"\",\n            \"description\": \"\",\n            \"classes\": \"podcast\",\n            \"xfn\": \"\",\n            \"status\": \"publish\",\n            \"original_title\": \"Inicio\",\n            \"nav_menu_term_id\": -1358043554,\n            \"_invalid\": false,\n            \"type_label\": \"Enlace Personalizado\"\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-03-21 05:32:51\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', 'db7c9281-246b-4c44-a1fb-c9272ee3cdd5', '', '', '2018-03-20 23:32:51', '2018-03-21 05:32:51', '', 0, 'http://localhost:8080/localwp.dev/?p=86', 0, 'customize_changeset', '', 0),
(87, 1, '2018-03-20 23:32:52', '2018-03-21 05:32:52', '', 'linkedIn', '', 'publish', 'closed', 'closed', '', 'linkedin', '', '', '2018-04-02 19:04:53', '2018-04-03 01:04:53', '', 0, 'http://localhost:8080/localwp.dev/2018/03/20/linkedin/', 1, 'nav_menu_item', '', 0),
(88, 1, '2018-03-20 23:33:06', '2018-03-21 05:33:06', '', '<!--<i class=\"fa fa-twitter\"> </i>-->Seguir', '', 'publish', 'closed', 'closed', '', 'sigueme', '', '', '2018-04-02 19:04:53', '2018-04-03 01:04:53', '', 0, 'http://localhost:8080/localwp.dev/2018/03/20/sigueme/', 2, 'nav_menu_item', '', 0),
(89, 1, '2018-03-20 23:33:06', '2018-03-21 05:33:06', '', 'Podcast', '', 'publish', 'closed', 'closed', '', 'podcast', '', '', '2018-04-02 19:04:53', '2018-04-03 01:04:53', '', 0, 'http://localhost:8080/localwp.dev/2018/03/20/podcast/', 3, 'nav_menu_item', '', 0),
(90, 1, '2018-03-20 23:33:40', '2018-03-21 05:33:40', '{\n    \"site_icon\": {\n        \"value\": \"\",\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-03-21 05:33:40\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '4fbe8ce7-292f-4752-952d-6094c61e29cd', '', '', '2018-03-20 23:33:40', '2018-03-21 05:33:40', '', 0, 'http://localhost:8080/localwp.dev/2018/03/20/4fbe8ce7-292f-4752-952d-6094c61e29cd/', 0, 'customize_changeset', '', 0),
(91, 1, '2018-03-20 23:36:30', '2018-03-21 05:36:30', 'Contratame o cuentame algo\r\n\r\nEsta página deberá ser un formulario', 'Contacto', '', 'inherit', 'closed', 'closed', '', '24-revision-v1', '', '', '2018-03-20 23:36:30', '2018-03-21 05:36:30', '', 24, 'http://localhost:8080/localwp.dev/2018/03/20/24-revision-v1/', 0, 'revision', '', 0),
(92, 1, '2018-03-20 23:38:27', '2018-03-21 05:38:27', '', 'Contrátame o cuéntame algo', '', 'publish', 'closed', 'closed', '', '92', '', '', '2018-03-31 18:07:23', '2018-04-01 00:07:23', '', 0, 'http://localhost:8080/localwp.dev/?post_type=clever_contact_forms&#038;p=92', 0, 'clever_contact_forms', '', 0),
(93, 1, '2018-03-20 23:42:00', '2018-03-21 05:42:00', '{\n    \"nav_menu_item[-2005256855]\": {\n        \"value\": {\n            \"object_id\": 92,\n            \"object\": \"clever_contact_forms\",\n            \"menu_item_parent\": 0,\n            \"position\": 4,\n            \"type\": \"post_type\",\n            \"title\": \"Contr\\u00e1tame o cu\\u00e9ntame algo\",\n            \"url\": \"http://localhost:8080/localwp.dev/?post_type=clever_contact_forms&p=92\",\n            \"target\": \"\",\n            \"attr_title\": \"\",\n            \"description\": \"\",\n            \"classes\": \"\",\n            \"xfn\": \"\",\n            \"status\": \"publish\",\n            \"original_title\": \"Contr\\u00e1tame o cu\\u00e9ntame algo\",\n            \"nav_menu_term_id\": 17,\n            \"_invalid\": false,\n            \"type_label\": \"Formulario\"\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-03-21 05:42:00\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', 'f7f40ff8-0948-4c7c-99e4-a2687a23c6f8', '', '', '2018-03-20 23:42:00', '2018-03-21 05:42:00', '', 0, 'http://localhost:8080/localwp.dev/2018/03/20/f7f40ff8-0948-4c7c-99e4-a2687a23c6f8/', 0, 'customize_changeset', '', 0),
(94, 1, '2018-03-20 23:42:00', '2018-03-21 05:42:00', '', 'Contrátame <br> <span class=\"subtext\">(o cuéntame algo)<span>', '', 'publish', 'closed', 'closed', '', '94', '', '', '2018-04-02 19:04:53', '2018-04-03 01:04:53', '', 0, 'http://localhost:8080/localwp.dev/2018/03/20/94/', 4, 'nav_menu_item', '', 0),
(95, 1, '2018-03-21 00:08:00', '2018-03-21 06:08:00', '{\n    \"old_sidebars_widgets_data\": {\n        \"value\": {\n            \"wp_inactive_widgets\": [],\n            \"sidebar-1\": [\n                \"search-2\",\n                \"recent-posts-2\",\n                \"recent-comments-2\",\n                \"archives-2\",\n                \"categories-2\",\n                \"meta-2\"\n            ],\n            \"sidebar-2\": [],\n            \"sidebar-3\": []\n        },\n        \"type\": \"global_variable\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-03-21 06:08:00\"\n    },\n    \"cleverconsulting-chanchito-base-aacef1806d34::nav_menu_locations[main_menu]\": {\n        \"value\": 17,\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-03-21 06:08:00\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '083358c7-a58b-434d-9f71-f71f8c1e378f', '', '', '2018-03-21 00:08:00', '2018-03-21 06:08:00', '', 0, 'http://localhost:8080/localwp.dev/2018/03/21/083358c7-a58b-434d-9f71-f71f8c1e378f/', 0, 'customize_changeset', '', 0),
(96, 1, '2018-03-21 03:56:00', '2018-03-21 09:56:00', '', 'Blog', '', 'publish', 'closed', 'closed', '', 'blog', '', '', '2018-03-21 03:56:00', '2018-03-21 09:56:00', '', 0, 'http://localhost:8080/localwp.dev/blog/', 0, 'page', '', 0),
(98, 1, '2018-03-21 03:59:38', '2018-03-21 09:59:38', '{\n    \"show_on_front\": {\n        \"value\": \"posts\",\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-03-21 09:59:38\"\n    },\n    \"page_on_front\": {\n        \"value\": \"96\",\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-03-21 09:59:38\"\n    },\n    \"page_for_posts\": {\n        \"value\": \"0\",\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-03-21 09:59:38\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '691d109f-bb18-4416-b256-0d17074e835d', '', '', '2018-03-21 03:59:38', '2018-03-21 09:59:38', '', 0, 'http://localhost:8080/localwp.dev/2018/03/21/691d109f-bb18-4416-b256-0d17074e835d/', 0, 'customize_changeset', '', 0),
(99, 1, '2018-03-21 10:06:57', '2018-03-21 16:06:57', '<h1 style=\"text-align: left;\">Luis Monge Malo</h1>\r\nErpiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, lorem ipsum dolor est totam rem aperiam, eaque ipsa quae ab illo inventore.', 'Titulo Texto Header', '', 'publish', 'closed', 'closed', '', 'titulo-texto-header', '', '', '2018-04-01 11:17:56', '2018-04-01 17:17:56', '', 0, 'http://localhost:8080/localwp.dev/?post_type=text_blocks&#038;p=99', 0, 'text_blocks', '', 0),
(100, 1, '2018-03-21 16:16:09', '2018-03-21 22:16:09', 'Organiza tu vida digital: consume el máximo contenido en el menor tiempo y optimiza en redes sociales\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla', 'Organiza tu vida digital: consume el máximo contenido en el menor tiempo y optimiza en redes sociales', '', 'inherit', 'closed', 'closed', '', '74-revision-v1', '', '', '2018-03-21 16:16:09', '2018-03-21 22:16:09', '', 74, 'http://localhost:8080/localwp.dev/2018/03/21/74-revision-v1/', 0, 'revision', '', 0),
(101, 1, '2018-03-21 16:17:42', '2018-03-21 22:17:42', 'Esta frase tiene cinco palabras\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla', 'Esta frase tiene cinco palabras', '', 'inherit', 'closed', 'closed', '', '49-revision-v1', '', '', '2018-03-21 16:17:42', '2018-03-21 22:17:42', '', 49, 'http://localhost:8080/localwp.dev/2018/03/21/49-revision-v1/', 0, 'revision', '', 0),
(102, 1, '2018-03-21 16:19:11', '2018-03-21 22:19:11', 'El peor tipo de emprendedor\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla', 'El peor tipo de emprendedor', '', 'inherit', 'closed', 'closed', '', '53-revision-v1', '', '', '2018-03-21 16:19:11', '2018-03-21 22:19:11', '', 53, 'http://localhost:8080/localwp.dev/2018/03/21/53-revision-v1/', 0, 'revision', '', 0),
(103, 1, '2018-03-21 16:20:14', '2018-03-21 22:20:14', 'Cómo asesinar a tu peor enemigo\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla', 'Cómo asesinar a tu peor enemigo', '', 'inherit', 'closed', 'closed', '', '56-revision-v1', '', '', '2018-03-21 16:20:14', '2018-03-21 22:20:14', '', 56, 'http://localhost:8080/localwp.dev/2018/03/21/56-revision-v1/', 0, 'revision', '', 0),
(104, 1, '2018-03-21 16:21:19', '2018-03-21 22:21:19', 'Consejo del fundador de Alibaba a jóvenes emprendedores\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla', 'Consejo del fundador de Alibaba a jóvenes emprendedores', '', 'inherit', 'closed', 'closed', '', '59-revision-v1', '', '', '2018-03-21 16:21:19', '2018-03-21 22:21:19', '', 59, 'http://localhost:8080/localwp.dev/2018/03/21/59-revision-v1/', 0, 'revision', '', 0),
(105, 1, '2018-03-21 16:23:21', '2018-03-21 22:23:21', 'Por qué los detalles matan los proyectos\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla', 'Por qué los detalles matan los proyectos', '', 'inherit', 'closed', 'closed', '', '62-revision-v1', '', '', '2018-03-21 16:23:21', '2018-03-21 22:23:21', '', 62, 'http://localhost:8080/localwp.dev/2018/03/21/62-revision-v1/', 0, 'revision', '', 0),
(106, 1, '2018-03-21 16:25:22', '2018-03-21 22:25:22', 'Cómo no perder un concurso público\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla', 'Cómo no perder un concurso público', '', 'inherit', 'closed', 'closed', '', '65-revision-v1', '', '', '2018-03-21 16:25:22', '2018-03-21 22:25:22', '', 65, 'http://localhost:8080/localwp.dev/2018/03/21/65-revision-v1/', 0, 'revision', '', 0),
(107, 1, '2018-03-21 16:26:16', '2018-03-21 22:26:16', 'La forma más barata de mejorar tu producto o servicio\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla', 'La forma más barata de mejorar tu producto o servicio', '', 'inherit', 'closed', 'closed', '', '68-revision-v1', '', '', '2018-03-21 16:26:16', '2018-03-21 22:26:16', '', 68, 'http://localhost:8080/localwp.dev/2018/03/21/68-revision-v1/', 0, 'revision', '', 0),
(108, 1, '2018-03-21 16:27:44', '2018-03-21 22:27:44', 'Reseña de los 42 libros que he leído en 2016\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla', 'Reseña de los 42 libros que he leído en 2016', '', 'inherit', 'closed', 'closed', '', '71-revision-v1', '', '', '2018-03-21 16:27:44', '2018-03-21 22:27:44', '', 71, 'http://localhost:8080/localwp.dev/2018/03/21/71-revision-v1/', 0, 'revision', '', 0),
(109, 1, '2018-03-22 09:34:33', '2018-03-22 15:34:33', '{\n    \"cleverconsulting-chanchito-base-aacef1806d34::nav_menu_locations[main_menu]\": {\n        \"value\": 17,\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-03-22 15:29:12\"\n    },\n    \"nav_menu[18]\": {\n        \"value\": false,\n        \"type\": \"nav_menu\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-03-22 15:34:12\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '0fadaf73-b328-41ee-a06c-7d83dfb35eaa', '', '', '2018-03-22 09:34:33', '2018-03-22 15:34:33', '', 0, 'http://localhost:8080/localwp.dev/?p=109', 0, 'customize_changeset', '', 0),
(110, 1, '2018-03-22 09:35:10', '2018-03-22 15:35:10', '{\n    \"old_sidebars_widgets_data\": {\n        \"value\": {\n            \"wp_inactive_widgets\": [],\n            \"sidebar\": []\n        },\n        \"type\": \"global_variable\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-03-22 15:35:10\"\n    },\n    \"twentyfourteen::nav_menu_locations[primary]\": {\n        \"value\": 17,\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-03-22 15:35:10\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '471dbb00-2f01-4dd1-9fc4-56cf60726ec6', '', '', '2018-03-22 09:35:10', '2018-03-22 15:35:10', '', 0, 'http://localhost:8080/localwp.dev/2018/03/22/471dbb00-2f01-4dd1-9fc4-56cf60726ec6/', 0, 'customize_changeset', '', 0),
(111, 1, '2018-03-22 09:35:37', '2018-03-22 15:35:37', '{\n    \"old_sidebars_widgets_data\": {\n        \"value\": {\n            \"wp_inactive_widgets\": [],\n            \"sidebar-1\": [],\n            \"sidebar-2\": [],\n            \"sidebar-3\": []\n        },\n        \"type\": \"global_variable\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-03-22 15:35:37\"\n    },\n    \"cleverconsulting-chanchito-base-aacef1806d34::nav_menu_locations[main_menu]\": {\n        \"value\": 17,\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-03-22 15:35:37\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '69497970-8de8-4ab7-b781-95acb088370e', '', '', '2018-03-22 09:35:37', '2018-03-22 15:35:37', '', 0, 'http://localhost:8080/localwp.dev/2018/03/22/69497970-8de8-4ab7-b781-95acb088370e/', 0, 'customize_changeset', '', 0),
(112, 1, '2018-03-22 09:40:50', '2018-03-22 15:40:50', '{\n    \"cleverconsulting-chanchito-base-aacef1806d34::nav_menu_locations[main_menu]\": {\n        \"value\": 0,\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-03-22 15:40:50\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '77a66b0e-6434-497d-ad6a-b1209f78de58', '', '', '2018-03-22 09:40:50', '2018-03-22 15:40:50', '', 0, 'http://localhost:8080/localwp.dev/2018/03/22/77a66b0e-6434-497d-ad6a-b1209f78de58/', 0, 'customize_changeset', '', 0),
(113, 1, '2018-03-22 10:06:52', '2018-03-22 16:06:52', '<img class=\"alignnone size-full wp-image-80\" src=\"http://localhost:8080/localwp.dev/wp-content/uploads/2018/03/layer-13.png\" alt=\"\" width=\"180\" height=\"180\" />', 'TITULO 1', '', 'publish', 'closed', 'closed', '', 'titulo-1', '', '', '2018-03-28 00:33:05', '2018-03-28 06:33:05', '', 0, 'http://localhost:8080/localwp.dev/?post_type=text_blocks&#038;p=113', 0, 'text_blocks', '', 0),
(114, 1, '2018-03-22 10:39:44', '2018-03-22 16:39:44', '{\n    \"nav_menu_item[-1468530202]\": {\n        \"value\": {\n            \"object_id\": 113,\n            \"object\": \"text_blocks\",\n            \"menu_item_parent\": 0,\n            \"position\": 5,\n            \"type\": \"post_type\",\n            \"title\": \"TITULO 1\",\n            \"url\": \"http://localhost:8080/localwp.dev/text_blocks/titulo-1/\",\n            \"target\": \"\",\n            \"attr_title\": \"\",\n            \"description\": \"\",\n            \"classes\": \"\",\n            \"xfn\": \"\",\n            \"status\": \"publish\",\n            \"original_title\": \"TITULO 1\",\n            \"nav_menu_term_id\": 17,\n            \"_invalid\": false,\n            \"type_label\": \"Bloque de texto\"\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-03-22 16:39:44\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '13e6591e-d44d-40f0-9299-0d3dfd6ccd35', '', '', '2018-03-22 10:39:44', '2018-03-22 16:39:44', '', 0, 'http://localhost:8080/localwp.dev/2018/03/22/13e6591e-d44d-40f0-9299-0d3dfd6ccd35/', 0, 'customize_changeset', '', 0),
(116, 1, '2018-03-22 12:42:21', '2018-03-22 18:42:21', '', 'MainPageForm', '', 'publish', 'closed', 'closed', '', 'mainpageform', '', '', '2018-03-22 12:43:14', '2018-03-22 18:43:14', '', 0, 'http://localhost:8080/localwp.dev/?post_type=clever_contact_forms&#038;p=116', 0, 'clever_contact_forms', '', 0),
(117, 1, '2018-03-22 12:45:31', '2018-03-22 18:45:31', '<h2>Apúntate a la Newsletter</h2>\r\nY te contaré cosas que no puedo decir por aquí\r\n\r\n&nbsp;', 'Apúntate a la newsletter', '', 'publish', 'closed', 'closed', '', 'apuntate-a-la-newsletter', '', '', '2018-03-28 16:16:48', '2018-03-28 22:16:48', '', 0, 'http://localhost:8080/localwp.dev/?post_type=text_blocks&#038;p=117', 0, 'text_blocks', '', 0),
(118, 1, '2018-03-28 15:30:38', '2018-03-28 21:30:38', '<h3>Apúntate a la Newsletter</h3>\nY te contaré cosas que no puedo decir por aquí   [ccf id=\"116\"]\n\n&nbsp;', 'Apúntate a la newsletter', '', 'inherit', 'closed', 'closed', '', '117-autosave-v1', '', '', '2018-03-28 15:30:38', '2018-03-28 21:30:38', '', 117, 'http://localhost:8080/localwp.dev/2018/03/23/117-autosave-v1/', 0, 'revision', '', 0),
(119, 1, '2018-03-25 17:02:48', '2018-03-25 23:02:48', '{\n    \"old_sidebars_widgets_data\": {\n        \"value\": {\n            \"wp_inactive_widgets\": [],\n            \"sidebar-1\": [],\n            \"sidebar-2\": [],\n            \"sidebar-3\": []\n        },\n        \"type\": \"global_variable\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-03-25 23:02:48\"\n    },\n    \"cleverconsulting-chanchito-base::nav_menu_locations[main_menu]\": {\n        \"value\": 17,\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-03-25 23:02:48\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', 'fc7e271f-191a-4801-8d8a-7c8ba59231ad', '', '', '2018-03-25 17:02:48', '2018-03-25 23:02:48', '', 0, 'http://localhost:8080/localwp.dev/2018/03/25/fc7e271f-191a-4801-8d8a-7c8ba59231ad/', 0, 'customize_changeset', '', 0),
(120, 1, '2018-03-26 19:46:00', '2018-03-27 01:46:00', '', 'layer-21@2x', '', 'inherit', 'open', 'closed', '', 'layer-212x', '', '', '2018-03-26 19:46:00', '2018-03-27 01:46:00', '', 74, 'http://localhost:8080/localwp.dev/wp-content/uploads/2018/03/layer-21@2x.png', 0, 'attachment', 'image/png', 0),
(121, 1, '2018-03-27 07:57:47', '2018-03-27 13:57:47', 'Luis Monge Manlo\n\nErpiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, lorem ipsum dolor est totam rem aperiam, eaque ipsa quae ab illo inventore.', 'Titulo Texto Header', '', 'inherit', 'closed', 'closed', '', '99-autosave-v1', '', '', '2018-03-27 07:57:47', '2018-03-27 13:57:47', '', 99, 'http://localhost:8080/localwp.dev/2018/03/27/99-autosave-v1/', 0, 'revision', '', 0),
(122, 1, '2018-03-31 18:06:30', '0000-00-00 00:00:00', '', 'Borrador automático', '', 'auto-draft', 'open', 'open', '', '', '', '', '2018-03-31 18:06:30', '0000-00-00 00:00:00', '', 0, 'http://localhost:8080/localwp.dev/?p=122', 0, 'post', '', 0),
(123, 1, '2018-03-31 21:02:01', '2018-04-01 03:02:01', 'Brent Adamson y Matthew C. Dixon, autores de The Challenger Sale, estiman que alrededor del 20 % de las solicitudes de presupuesto sólo persiguen comparar tu presupuesto con otro que el cliente ya casi ha decidido contratar.', 'layer-25@2x', '', 'inherit', 'open', 'closed', '', 'layer-252x', '', '', '2018-03-31 21:02:14', '2018-04-01 03:02:14', '', 65, 'http://localhost:8080/localwp.dev/wp-content/uploads/2018/03/layer-25@2x.png', 0, 'attachment', 'image/png', 0),
(124, 1, '2018-04-02 22:27:34', '2018-04-03 04:27:34', '<h1>Cómo no perder un concurso público</h1>\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla\n\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla', 'Cómo no perder un concurso público', '', 'inherit', 'closed', 'closed', '', '65-autosave-v1', '', '', '2018-04-02 22:27:34', '2018-04-03 04:27:34', '', 65, 'http://localhost:8080/localwp.dev/2018/04/02/65-autosave-v1/', 0, 'revision', '', 0),
(125, 1, '2018-04-02 22:29:35', '2018-04-03 04:29:35', '<h1>Cómo no perder un concurso público</h1>\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla', 'Cómo no perder un concurso público', '', 'inherit', 'closed', 'closed', '', '65-revision-v1', '', '', '2018-04-02 22:29:35', '2018-04-03 04:29:35', '', 65, 'http://localhost:8080/localwp.dev/2018/04/02/65-revision-v1/', 0, 'revision', '', 0),
(126, 1, '2018-04-02 22:30:24', '2018-04-03 04:30:24', '<h1>No participes en concursos que no hayas contribuido a lanzar.</h1>\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla', 'Cómo no perder un concurso público', '', 'inherit', 'closed', 'closed', '', '65-revision-v1', '', '', '2018-04-02 22:30:24', '2018-04-03 04:30:24', '', 65, 'http://localhost:8080/localwp.dev/2018/04/02/65-revision-v1/', 0, 'revision', '', 0),
(127, 1, '2018-04-02 22:38:15', '2018-04-03 04:38:15', '<h1>Organiza tu vida digital: consume el máximo contenido en el menor tiempo y optimiza en redes sociales</h1>\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla', 'Organiza tu vida digital: consume el máximo contenido en el menor tiempo y optimiza en redes sociales', '', 'inherit', 'closed', 'closed', '', '74-revision-v1', '', '', '2018-04-02 22:38:15', '2018-04-03 04:38:15', '', 74, 'http://localhost:8080/localwp.dev/2018/04/02/74-revision-v1/', 0, 'revision', '', 0),
(128, 1, '2018-04-03 11:56:12', '2018-04-03 17:56:12', 'Lorem ipsum', 'layer-19-copy@2x', '', 'inherit', 'open', 'closed', '', 'layer-19-copy2x', '', '', '2018-04-03 11:56:31', '2018-04-03 17:56:31', '', 71, 'http://localhost:8080/localwp.dev/wp-content/uploads/2018/03/layer-19-copy@2x.png', 0, 'attachment', 'image/png', 0),
(129, 1, '2018-04-03 11:58:21', '2018-04-03 17:58:21', 'Img- Art  Lorem ipsum', 'layer-23@2x', '', 'inherit', 'open', 'closed', '', 'layer-232x', '', '', '2018-04-03 11:58:48', '2018-04-03 17:58:48', '', 68, 'http://localhost:8080/localwp.dev/wp-content/uploads/2018/03/layer-23@2x.png', 0, 'attachment', 'image/png', 0),
(130, 1, '2018-04-03 12:01:38', '2018-04-03 18:01:38', '', 'layer-27@2x', '', 'inherit', 'open', 'closed', '', 'layer-272x', '', '', '2018-04-03 12:01:38', '2018-04-03 18:01:38', '', 62, 'http://localhost:8080/localwp.dev/wp-content/uploads/2018/03/layer-27@2x.png', 0, 'attachment', 'image/png', 0),
(131, 1, '2018-04-03 12:02:42', '2018-04-03 18:02:42', 'Alibaba', 'layer-29@2x', '', 'inherit', 'open', 'closed', '', 'layer-292x', '', '', '2018-04-03 12:03:08', '2018-04-03 18:03:08', '', 59, 'http://localhost:8080/localwp.dev/wp-content/uploads/2018/03/layer-29@2x.png', 0, 'attachment', 'image/png', 0),
(132, 1, '2018-04-03 12:04:04', '2018-04-03 18:04:04', '', 'layer-31@2x', '', 'inherit', 'open', 'closed', '', 'layer-312x', '', '', '2018-04-03 12:04:04', '2018-04-03 18:04:04', '', 56, 'http://localhost:8080/localwp.dev/wp-content/uploads/2018/03/layer-31@2x.png', 0, 'attachment', 'image/png', 0),
(133, 1, '2018-04-03 12:04:51', '2018-04-03 18:04:51', '', 'layer-33@2x', '', 'inherit', 'open', 'closed', '', 'layer-332x', '', '', '2018-04-03 12:04:51', '2018-04-03 18:04:51', '', 53, 'http://localhost:8080/localwp.dev/wp-content/uploads/2018/03/layer-33@2x.png', 0, 'attachment', 'image/png', 0),
(134, 1, '2018-04-03 12:05:40', '2018-04-03 18:05:40', '', 'layer-35@2x', '', 'inherit', 'open', 'closed', '', 'layer-352x', '', '', '2018-04-03 12:05:40', '2018-04-03 18:05:40', '', 49, 'http://localhost:8080/localwp.dev/wp-content/uploads/2018/03/layer-35@2x.png', 0, 'attachment', 'image/png', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `localwp_termmeta`
--

CREATE TABLE `localwp_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `localwp_terms`
--

CREATE TABLE `localwp_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `localwp_terms`
--

INSERT INTO `localwp_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Sin categoría', 'sin-categoria', 0),
(2, 'WordPress', 'wordpress', 0),
(3, 'CSS', 'css', 0),
(4, 'JS', 'js', 0),
(5, 'Tutorials', 'tutorials', 0),
(6, 'Reviews', 'reviews', 0),
(7, 'Themes', 'themes', 0),
(8, 'Plugins', 'plugins', 0),
(9, 'post-format-gallery', 'post-format-gallery', 0),
(10, 'plant', 'plant', 0),
(11, 'tank', 'tank', 0),
(12, 'aquarium', 'aquarium', 0),
(13, 'screens', 'screens', 0),
(17, 'main_menu', 'main_menu', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `localwp_term_relationships`
--

CREATE TABLE `localwp_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `localwp_term_relationships`
--

INSERT INTO `localwp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(7, 1, 0),
(7, 2, 0),
(10, 1, 0),
(16, 2, 0),
(16, 9, 0),
(16, 10, 0),
(16, 11, 0),
(16, 12, 0),
(16, 13, 0),
(18, 1, 0),
(49, 2, 0),
(53, 1, 0),
(56, 1, 0),
(59, 1, 0),
(62, 1, 0),
(65, 1, 0),
(68, 1, 0),
(71, 1, 0),
(74, 1, 0),
(87, 17, 0),
(88, 17, 0),
(89, 17, 0),
(94, 17, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `localwp_term_taxonomy`
--

CREATE TABLE `localwp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `localwp_term_taxonomy`
--

INSERT INTO `localwp_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 8),
(2, 2, 'category', '', 0, 1),
(3, 3, 'category', '', 0, 0),
(4, 4, 'category', '', 0, 0),
(5, 5, 'post_tag', '', 0, 0),
(6, 6, 'post_tag', '', 0, 0),
(7, 7, 'post_tag', '', 0, 0),
(8, 8, 'post_tag', '', 0, 0),
(9, 9, 'post_format', '', 0, 0),
(10, 10, 'post_tag', '', 0, 0),
(11, 11, 'post_tag', '', 0, 0),
(12, 12, 'post_tag', '', 0, 0),
(13, 13, 'post_tag', '', 0, 0),
(17, 17, 'nav_menu', '', 0, 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `localwp_usermeta`
--

CREATE TABLE `localwp_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `localwp_usermeta`
--

INSERT INTO `localwp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 's1t3_@dm1n'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'true'),
(11, 1, 'locale', ''),
(12, 1, 'localwp_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(13, 1, 'localwp_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', 'theme_editor_notice'),
(15, 1, 'show_welcome_panel', '1'),
(17, 1, 'localwp_dashboard_quick_press_last_post_id', '122'),
(18, 1, 'community-events-location', 'a:1:{s:2:\"ip\";s:2:\"::\";}'),
(19, 1, 'localwp_user-settings', 'libraryContent=browse&advImgDetails=show&hidetb=1'),
(20, 1, 'localwp_user-settings-time', '1522272542'),
(21, 1, 'session_tokens', 'a:3:{s:64:\"dc1ba150e74bb79fb561e5e327cde7a3e88cb2fb003399dbebfb076334d1fcf9\";a:4:{s:10:\"expiration\";i:1522889449;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:114:\"Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36\";s:5:\"login\";i:1522716649;}s:64:\"d24c7c31a79bd70990dc606beaab07440a9ad6c86d4b11d68efd6b4395988c1d\";a:4:{s:10:\"expiration\";i:1522937677;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36\";s:5:\"login\";i:1522764877;}s:64:\"9ccb3605963975d9959f19cbbcbab48baaa9b0639ef88088ce566a6046d58005\";a:4:{s:10:\"expiration\";i:1522950795;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36\";s:5:\"login\";i:1522777995;}}'),
(22, 1, 'nav_menu_recently_edited', '17'),
(23, 1, 'managenav-menuscolumnshidden', 'a:5:{i:0;s:11:\"link-target\";i:1;s:11:\"css-classes\";i:2;s:3:\"xfn\";i:3;s:11:\"description\";i:4;s:15:\"title-attribute\";}'),
(24, 1, 'metaboxhidden_nav-menus', 'a:4:{i:0;s:25:\"add-post-type-text_blocks\";i:1;s:34:\"add-post-type-clever_contact_forms\";i:2;s:33:\"add-post-type-clever_testimonials\";i:3;s:12:\"add-post_tag\";}');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `localwp_users`
--

CREATE TABLE `localwp_users` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `localwp_users`
--

INSERT INTO `localwp_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 's1t3_@dm1n', '$P$B0yRfRNTi65rcFt4qeekScRThdZi.z/', 's1t3_dm1n', 'iromero@cleverconsulting.net', '', '2018-03-19 18:01:03', '', 0, 's1t3_@dm1n');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `localwp_commentmeta`
--
ALTER TABLE `localwp_commentmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indices de la tabla `localwp_comments`
--
ALTER TABLE `localwp_comments`
  ADD PRIMARY KEY (`comment_ID`),
  ADD KEY `comment_post_ID` (`comment_post_ID`),
  ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  ADD KEY `comment_date_gmt` (`comment_date_gmt`),
  ADD KEY `comment_parent` (`comment_parent`),
  ADD KEY `comment_author_email` (`comment_author_email`(10));

--
-- Indices de la tabla `localwp_links`
--
ALTER TABLE `localwp_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `link_visible` (`link_visible`);

--
-- Indices de la tabla `localwp_options`
--
ALTER TABLE `localwp_options`
  ADD PRIMARY KEY (`option_id`),
  ADD UNIQUE KEY `option_name` (`option_name`);

--
-- Indices de la tabla `localwp_postmeta`
--
ALTER TABLE `localwp_postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indices de la tabla `localwp_posts`
--
ALTER TABLE `localwp_posts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `post_name` (`post_name`(191)),
  ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  ADD KEY `post_parent` (`post_parent`),
  ADD KEY `post_author` (`post_author`);

--
-- Indices de la tabla `localwp_termmeta`
--
ALTER TABLE `localwp_termmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `term_id` (`term_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indices de la tabla `localwp_terms`
--
ALTER TABLE `localwp_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD KEY `slug` (`slug`(191)),
  ADD KEY `name` (`name`(191));

--
-- Indices de la tabla `localwp_term_relationships`
--
ALTER TABLE `localwp_term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- Indices de la tabla `localwp_term_taxonomy`
--
ALTER TABLE `localwp_term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`),
  ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  ADD KEY `taxonomy` (`taxonomy`);

--
-- Indices de la tabla `localwp_usermeta`
--
ALTER TABLE `localwp_usermeta`
  ADD PRIMARY KEY (`umeta_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indices de la tabla `localwp_users`
--
ALTER TABLE `localwp_users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_login_key` (`user_login`),
  ADD KEY `user_nicename` (`user_nicename`),
  ADD KEY `user_email` (`user_email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `localwp_commentmeta`
--
ALTER TABLE `localwp_commentmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `localwp_comments`
--
ALTER TABLE `localwp_comments`
  MODIFY `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `localwp_links`
--
ALTER TABLE `localwp_links`
  MODIFY `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `localwp_options`
--
ALTER TABLE `localwp_options`
  MODIFY `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=450;

--
-- AUTO_INCREMENT de la tabla `localwp_postmeta`
--
ALTER TABLE `localwp_postmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=391;

--
-- AUTO_INCREMENT de la tabla `localwp_posts`
--
ALTER TABLE `localwp_posts`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=135;

--
-- AUTO_INCREMENT de la tabla `localwp_termmeta`
--
ALTER TABLE `localwp_termmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `localwp_terms`
--
ALTER TABLE `localwp_terms`
  MODIFY `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT de la tabla `localwp_term_taxonomy`
--
ALTER TABLE `localwp_term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT de la tabla `localwp_usermeta`
--
ALTER TABLE `localwp_usermeta`
  MODIFY `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT de la tabla `localwp_users`
--
ALTER TABLE `localwp_users`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
