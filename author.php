<?php
/**
 * @package Clever
 */

get_header(); ?>
<section id="main" role="main">
	<?php do_action('clever_pre_index_section'); ?>

	<?php if ( have_posts() ) : the_post(); ?>

		<?php do_action('clever_pre_index_header'); ?>

		<header class="page-header">
			<h1 class="page-title author">
				<?php printf( __( 'Entradas de: %s', 'clever' ), '<span class="vcard"><a class="url fn n" href="'.get_author_posts_url( get_the_author_meta( "ID" ) ).'" title="'.esc_attr( get_the_author() ).'" rel="me">'.get_the_author().'</a></span>'); ?>
			</h1>
		</header>

		<?php do_action('clever_post_index_header'); ?>

		<?php rewind_posts(); ?>

		<?php if (is_single() && get_the_author_meta( 'description' ) ) : ?>
			get_template_part( 'author-bio' );
		<?php endif; ?>

		<?php do_action('clever_pre_index_content'); ?>

		<?php while ( have_posts() ) : the_post(); ?>

			<?php get_template_part( 'content', get_post_format() ); ?>

		<?php endwhile; ?>

		<?php clever_numeric_posts_nav(); ?>

		<?php do_action('clever_post_index_content'); ?>

	<?php else : ?>
		<?php get_template_part( 'content', 'none' ); ?>
	<?php endif; ?>

	<?php get_sidebar(); ?>

	<?php do_action('clever_post_index_section'); ?>
</section>
<?php get_footer(); ?>