<?php
/**
 * @package Clever
 */
?>

	<footer id="contentinfo" role="contentinfo" class="clearfix">
		<div class="el-xs-push-1 el-xs-10 el-sm-10 container group group-content-space-between group-content-center footercontainer">
			<div class="el-xs-12 el-sm-6 text-center text-sm-right">
				<div class="copyright">
					<?php bloginfo( 'name' ); ?> &copy; <?php echo date("Y"); ?>
				</div>
			</div>
			<div class="el-xs-12 el-sm-6 text-center text-sm-left">
				<ul class="legal-menu">
					<!--<li>
						<a href="<?php echo get_permalink(6); ?>"><?php _e('Aviso legal', 'clever'); ?></a>
					</li>-->
					<li>
						<a href="<?php echo get_permalink(7); ?>"><?php _e('Política de privacidad', 'clever'); ?></a>
					</li>
				</ul>
			</div>
		</div>
	</footer><!-- #contentinfo -->
</div><!-- #page -->
<?php wp_footer(); ?>
</body>
</html>