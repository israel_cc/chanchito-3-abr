<?php
/**
 * @package Clever
 */

get_header(); ?>
<section id="main" role="main">
	<?php do_action('clever_pre_index_section'); ?>
	<?php do_action('clever_pre_index_content'); ?>
	
	<?php while ( have_posts() ) : the_post(); ?>

		<?php get_template_part( 'content', get_post_format() ); ?>

		<!--<div> <!--<div class="clearfix el-xs-10">
            <?php //clever_post_nav(); ?>

		<?php
			if ( get_post_type() === 'post' && (comments_open() || get_comments_number()) ) :
				comments_template();
			endif;
		?>
            
        <!--</div>-->
	<?php endwhile; ?>
	
	<?php //do_action('clever_post_index_content'); ?>
	
	<?php //get_sidebar(); ?>
	
	<?php //do_action('clever_post_index_section'); ?>
</section>
<?php get_footer(); ?>