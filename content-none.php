<?php
/**
 * @package Clever
 */
?>
<div class="el-xs-12">
	<article id="post-0" class="post no-results not-found">
		<header class="entry-header">
			<h1 class="entry-title"><?php _e('Upss', 'clever'); ?></h1>
		</header><!-- .entry-header -->

		<div class="entry-content">
			<p><?php _e('El contenido que buscas no existe. Prueba a buscar algo…', 'clever'); ?></p>
			<?php get_search_form(); ?>
		</div><!-- .entry-content -->
	</article><!-- #post-0 -->
</div>