<?php
/**
 * @package Clever
 */
?>
<!--<div class="clearfix">
<?php //echo json_encode(is_single()); ?>
</div>-->
<article id="post-<?php the_ID(); ?>"  <?php if (is_single()) : ?> class="container group-content-center text-center el-lg-push-2 el-lg-7 el-sm-10 el-xs-10" <?php else : ?> class="container el-lg-4 el-md-4 el-sm-5 el-xs-7" <?php endif; ?> <?php post_class(); ?> > 
        <!--<div class="el-lg-push-3 el-lg-7 el-sm-10 container group-content-center">-->
        <!--<div class="container el-lg-7 el-sm-10 el-xs-10 ">-->
        
        <!-- <div class="container group-wrap"> -->
        <!--<div class=" container group-wrap el-lg-4 el-md-4 el-sm-5 el-xs-7">-->
        <!-- lg-4 el-md-5 el-sm-8 -->
        
    <!--<div>-->
    <?php if (!is_single()) : ?>
        <a href="<?php the_permalink(); ?>" title="<?php echo the_title_attribute( 'echo=0' ); ?>" rel="bookmark">
    <?php endif; ?>
        
	<?php if (has_post_thumbnail()): ?>
	<div class="entry-thumbnail">
        <?php if (is_single()) : ?>
            <?php the_post_thumbnail('large'); ?>
            <div class="entry_comments_number"> <p><?php echo get_comments_number();//comments_number("0 ","1 ","% "); ?></p></div>
            <?php echo get_post(get_post_thumbnail_id())->post_content; ?>
        <?php else : ?>
            <?php the_post_thumbnail('large');//homepage-thumb'); ?>
            <!--<span class="entry_commments_number"><?php //comments_number("0 ","1 ","% "); ?></span>-->
            <div class="entry_comments_number"> <p><?php echo get_comments_number();//comments_number("0 ","1 ","% "); ?></p></div>
        <?php endif; ?>
	<?php endif; ?>
	</div>
    <header class="entry-header">
        <?php if (is_single()) : ?>
        <h1 class="entry-title"><?php the_title(); ?></h1>
        <?php else : ?>
        <h2 class="entry-title"><?php the_title(); ?></h2>
        <?php endif; ?>
    </header><!-- .entry-header -->
    <?php if (!is_single()) : ?>
        </a>
    <?php endif; ?>
    <div class="entry-content">
        <?php if (is_single()) : ?>
            <?php the_content(); ?>
        <?php else : ?>
            <?php //the_excerpt(); ?>
        <?php endif; ?>
    </div><!-- .entry-content -->

	<?php
		if (is_single() && get_the_author_meta( 'description' ) ) :
			get_template_part( 'author-bio' );
		endif;
	?>
    
	<footer class="entry-meta">
		<?php //clever_entry_meta(); ?>
		<?php //clever_entry_tags(); ?>
		<?php //clever_social_share(); ?>
	</footer><!-- .entry-meta -->
    <?php if (is_single()) : ?> 
        <!--</div>-->
    <?php else : ?> 
        <!--</div>-->
    <?php endif; ?>
    <!--</div>-->
</article><!-- #post-<?php the_ID(); ?> -->