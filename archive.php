<?php
/**
 * @package Clever
 */

get_header(); ?>
<section id="main" role="main">
	<?php do_action('clever_pre_index_section'); ?>

	<?php if ( have_posts() ) : ?>
		<?php do_action('clever_pre_index_header'); ?>
		
		<header class="page-header">
			<h1 class="page-title">
			<?php
			if ( is_day() )
				printf( __('Archivo diario: %s', 'clever'),'<span>'.get_the_date().'</span>' );
			elseif ( is_month() )
				printf( __('Archivo mensual: %s', 'clever'),'<span>'.get_the_date( 'F Y' ).'</span>' );
			elseif ( is_year() )
				printf( __('Archivo anual: s%', 'clever'),'<span>'.get_the_date( 'Y' ).'</span>' );
			else
				printf( __('Archivo', 'clever') );
			?>
			</h1>
		</header>

		<?php do_action('clever_post_index_header'); ?>

		<?php rewind_posts(); ?>

		<?php do_action('clever_pre_index_content'); ?>

		<?php while ( have_posts() ) : the_post(); ?>

			<?php get_template_part( 'content', get_post_format() ); ?>

		<?php endwhile; ?>

		<?php clever_numeric_posts_nav(); ?>

		<?php do_action('clever_post_index_content'); ?>

	<?php else : ?>
		<?php get_template_part( 'content', 'none' ); ?>
	<?php endif; ?>

	<?php get_sidebar(); ?>
	
	<?php do_action('clever_post_index_section'); ?>
</section>
<?php get_footer(); ?>