$(function() {
	// COOKIES
	if (!localStorage || !localStorage.cleverCookies) {
		$('#cookies').show();
		$('#cookies .close-cookies').click(function() {
			try {
				localStorage.cleverCookies = true;
			} catch(e) {}
			$('#cookies').slideUp(500);
		});
	}

	// Menú móvil
	$('#toggle i').click(function(event) {
		$('#navigation').addClass('active');
	});
	$('#navigation .shadow').click(function(event) {
		event.preventDefault();
		$('#navigation').removeClass('active');
	});
	$('#navigation').on('swipe',function(){
		$(this).hide();
	});

    /*
	// STICKY NAVIGATION
	//bannerInitTopPosition = $('#banner').offset().top;
	//$(window).scroll(function() {
	//	if ($(window).scrollTop() > bannerInitTopPosition)
	//		$('#banner').addClass('sticky');
	//	else if ($('#banner').hasClass('sticky'))
	//		$('#banner').removeClass('sticky');
	//});
    */
	// ANCHORS
	$('a[href*="#"]:not([href="#"])').click(function() {
		if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
			var target = $(this.hash);
			target = target.length ? target : $('[name="'+this.hash.slice(1)+'"]');
			if (target.length) {
				$('html,body').animate({
					scrollTop: target.offset().top-70
				}, 1000);
				return false;
			}
		}
	});

	// AJAX NAVIGATION
	$('#load-more').click(function(e) {
		e.stopPropagation();
		var page = $(this).data('page'),
			url = $(this).find('a').attr('href');

		$.ajax({
			url: clever.ajaxurl,
			type: 'POST',
			data: {
				action: 'clever_ajax_get_posts',
				query_vars: clever.query_vars,
				page: page,
				url: url
			},
			success: function( data ) {
				data = jQuery.parseJSON(data);
				if (data.posts) {
					$('#load-more').before(data.posts);
					if (data.nextUrl)
						$('#load-more a').attr('href', data.nextUrl);
					if (data.url)
						history.pushState({}, "", data.url);
				}

				if (data.nextPage)
					$('#load-more').data('page', data.nextPage);
				else
					$('#load-more').hide();
			},
			error: function() {
				// redireccionar
			}
		})
		return false;
	});

	// SLIDER
	$('.clever-slider').each(function() {
		$(this).flexslider({
			animation: 'slide',
			controlNav: false,
			nextText: '',
			prevText: '',
			pauseOnHover: true,
			slideshow: true,
			cations: true,
			random: false
		});
	});

	// CONTACT FORMS
	$('.ccf form').each(function() {
		$(this).find('[type="checkbox"],[type="radio"]').change(function() {
			var elements = $(this).closest('form').find('[name="'+$(this).attr('name')+'"]'),
				required = false,
				filled = false;

			elements.each(function() {
				if ($(this).prop('required'))
					required = true;
				if ($(this).is(':checked'))
					filled = true;
			});
			if (required) {
				elements.each(function() {
					$(this).prop('required', !(filled ^ $(this).is(':checked')));
				});
			}
		});

		// $(this).submit(function() {
		// 	var ajaxData = new FormData($(this)[0]),
		// 		sending = $(this).closest('.ccf').find('.sending'),
		// 		response = $(this).closest('.ccf').find('.response'),
		// 		form = $(this);

		// 	ajaxData.append('action', 'clever_contact_forms_send_mail');
		// 	ajaxData.append('id', $(this).data('id'));

		// 	sending.html('<i class="fa fa-lg fa-spinner fa-pulse"></i>');

		// 	$.ajax({
		// 		url: clever.ajaxurl,
		// 		type: 'POST',
		// 		cache: false,
		// 		contentType: false,
		// 		processData: false,
		// 		data: ajaxData
		// 	}).done(function(data) {
		// 		data = jQuery.parseJSON(data);
		// 		sending.html('');
		// 		if (data.result)
		// 			form[0].reset();
		// 		response.addClass((data.result ? 'correct' : 'error'));
		// 		response.html(data.response);
		// 	}).fail(function(jqXHR, textStatus, errorThrown) {
		// 		response.addClass('error');
		// 		response.html(textStatus);
		// 	});

		// 	return false;
		// });
	});

	if (needs_html5_validation())
		html5_validation();

	clever_inputs();

	/* Open menú */
	var touch = $('#menu-opener'),
		touch_start = 0;

	touch.bind('touchend', function(e) {
		var shadow = $('#navigation .shadow');

		if (parseInt(e.originalEvent.changedTouches[0].clientX) < $(window).width() - 30)
			$('#navigation').addClass('active');

		shadow.bind('touchstart', function(e) {
			touch_start = parseInt(e.originalEvent.changedTouches[0].clientX);
		});
		shadow.bind('touchend', function(e) {
			if (parseInt(e.originalEvent.changedTouches[0].clientX) > touch_start + 20)
				$('#navigation').removeClass('active')
		});

	});
});

function html5_validation() {
}

function clever_inputs() {

	$('[type="file"]:not(.no-js)').each(function() {
		var file = $(this);
		var wrap = file.wrap('<span class="file-wrap input-wrap" data-file=""></span>').parent();

		if (file.data('title') && file.data('title') != "")
			wrap.attr('title', file.data('title'));
		var text = "";
		if (file.attr('placeholder') && file.attr('placeholder') != "")
			text = file.attr('placeholder');
		wrap.prepend('<i class="fa fa-file fa-fw"></i> '+text);
	});

	$('[type="checkbox"]:not(.no-js)').each(function() {
		var input = $(this);
		var wrap = input.wrap('<label class="checkbox"></label>').parent();

		wrap.append('<span class="icon"></span>');
		if (input.data('option') && input.data('option') != "")
			wrap.append("&nbsp;"+input.data('option'));
	});

	$('[type="radio"]:not(.no-js)').each(function() {
		var input = $(this);
		var wrap = input.wrap('<label class="radio"></label>').parent();

		wrap.append('<span class="icon"></span>');
		if (input.data('option') && input.data('option') != "")
			wrap.append("&nbsp;"+input.data('option'));
	});

	$('select:not(.no-js)').each(function() {
		var select = $(this);
		var wrap = select.wrap('<span class="select-wrap input-wrap"></span>').parent();

		wrap.append('<span class="selector"><i class="fa fa-caret-up selector-up"></i><i class="fa fa-caret-down selector-down"></i></span>');
	});

	$('[type="file"]').click(function() {
		if ($(this).val().length > 0)
			$(this).val('');
	});
	$('[type="file"]').change(function() {
		var file = $(this).val();
		if (file.indexOf('fakepath') >= 0)
			file = file.substr(file.indexOf('fakepath')+9);
		$(this).closest('.file-wrap').attr('data-file', file);
	});
	$('[type="file"]').focusin(function() {
		$(this).closest('.file-wrap').addClass('active');
	}).focusout(function() {
		$(this).closest('.file-wrap').removeClass('active');
	});
	$('select').focusin(function() {
		$(this).closest('.select-wrap').addClass('active');
	}).focusout(function() {
		$(this).closest('.select-wrap').removeClass('active');
	});
	$('.checkbox [type="checkbox"]').focusin(function() {
		$(this).closest('.checkbox').addClass('active');
	}).focusout(function() {
		$(this).closest('.checkbox').removeClass('active');
	});
	$('.radio [type="radio"]').focusin(function() {
		$(this).closest('.radio').addClass('active');
	}).focusout(function() {
		$(this).closest('.radio').removeClass('active');
	});
}

function needs_html5_validation() {
	return typeof document.createElement('input').checkValidity !== 'function';
}
