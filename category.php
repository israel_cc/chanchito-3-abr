<?php
/**
 * @package Clever
 */

get_header(); ?>
<section id="main" role="main">
	<?php do_action('clever_pre_index_section'); ?>
	<?php if ( have_posts() ) : ?>

		<?php do_action('clever_pre_index_header'); ?>
				
		<header class="page-header">
			<h1 class="page-title">
				<?php single_cat_title( '', false ); ?>
			</h1>
			<?php
			$category_description = category_description();
			if ( ! empty( $category_description ) )
				echo apply_filters( 'category_archive_meta', '<h2 class="category-archive-meta">' . $category_description . '</h2>' );
			?>
		</header>

		<?php do_action('clever_post_index_header'); ?>

		<?php do_action('clever_pre_index_content'); ?>

		<?php while ( have_posts() ) : the_post(); ?>

			<?php get_template_part( 'content', get_post_format() ); ?>

		<?php endwhile; ?>

		<?php clever_numeric_posts_nav(); ?>

		<?php do_action('clever_post_index_content'); ?>

	<?php else : ?>
		<?php get_template_part( 'content', 'none' ); ?>
	<?php endif; ?>

	<?php get_sidebar(); ?>

	<?php do_action('clever_post_index_section'); ?>
</section>
<?php get_footer(); ?>