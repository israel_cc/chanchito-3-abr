jQuery(function() {
	jQuery('.backuper').click(function() {
		var data = {
			'action':'translator_backup_ajax',
			'lang':jQuery(this).parent('td').parent('tr').attr('data-lang')
		}
		jQuery.post(ajaxurl, data,
			function(data,textStatus,jqXHR) {
				alert(data);
				document.location.reload(true);
			}
		);
	});
	jQuery('.restorer').click(function() {
		var data = {
			'action':'translator_restore_ajax',
			'lang':jQuery(this).parent('td').parent('tr').attr('data-lang')
		}
		jQuery.post(ajaxurl, data,
			function(data,textStatus,jqXHR) {
				alert(data);
				document.location.reload(true);
			}
		);
	});
	jQuery('.deleter').click(function() {
		if (confirm(jQuery(this).attr('data-warning'))) {
			var data = {
				'action':'translator_delete_ajax',
				'lang':jQuery(this).parent('td').parent('tr').attr('data-lang')
			}
			jQuery.post(ajaxurl, data,
				function(data,textStatus,jqXHR) {
					document.location.reload(true);
				}
			);
		}
	});
	jQuery('.hider').click(function() {
		jQuery('table.translations tr').each(function() {
			if (jQuery(this).children('td').children('textarea').val() != "")
				jQuery(this).css({'visibility':'hidden','position':'absolute'});
		});
	});
	jQuery('.shower').click(function() {
		jQuery('table.translations tr').each(function() {
			jQuery(this).css({'visibility':'visible','position':'static'});
		});
	});
});