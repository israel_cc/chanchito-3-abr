(function ($) {
	$('#add-images').click(function() {
		wp.media.editor.send.attachment = function(props, attachment) {
			$.post( ajaxurl, 'action=clever_slider_get_field_row&image='+attachment.id,
				function(data) {
					if (data.row) {
						var row = $(data.row);
						$('#slides').append(row);
						prepareRow(row);
					}
				},'json');
		}
		wp.media.editor.open(this);
		return false;
	});

	$('#slides .slide').each(function() {
		prepareRow($(this));
	});
	prepareSort();

	function prepareRow(row) {
		row.find('.delete-image').click(function() {
			$(this).closest('.slide').remove();
			return false;
		});
	}

	function prepareSort() {
		$('#slides').sortable({
			opacity: 0.6,
			revert: true,
			cursor: 'move',
			handle: '.sort-image'
		});
	}
}(jQuery));