(function ($) {

	$('#new_field').click(function() {
		$.post(ajaxurl, 'action=clever_contact_forms_get_field_row',
			function(data,textStatus,jqXHR) {
				if (data.row) {
					var row = $(data.row);
					row.insertBefore('#fields_table .submit-row');
					prepareRow(row);
				}
			},
			'json'
		);
	});

	$('#shortcode').click(function() {
		$(this).select();
	});

	$('#fields_table tbody tr').each(function() {
		prepareRow($(this));
	});
	prepareShortcodes();
	
	function prepareRow(row) {
		row.find('[name="field_type[]"]').change(function() {
			var row = $(this).closest('tr');
			$.post(ajaxurl, 'action=clever_contact_forms_get_field_row&type='+$(this).val(),
				function(data,textStatus,jqXHR) {
					var newRow = $(data.row);
					row.replaceWith(newRow);
					prepareRow(newRow);
				},
				'json'
			);
		});
		row.find('[name="field_placeholder[]"]').focusout(function() {
			if ($(this).closest('tr').find('[name="field_name[]"]').val() == '')
				$(this).closest('tr').find('[name="field_name[]"]').val($(this).val().toLowerCase().split(' ').join('_'));
		});
		row.find('[name="field_name[]"]').click(function() {
			if (this.readOnly)
				this.readOnly = false;
		});
		row.find('[name="field_name[]"]').focusout(function() {
			prepareShortcodes();
		});
		if ($.inArray(row.find('[name="field_type[]"]').val(), ['checkbox','radio','select']) >= 0) {
			var textarea = $('<textarea class=".field-options"></textarea>');
			textarea.css('resize','vertical');
			row.find('[name="field_options[]"]').closest('td').append(textarea);

			if (row.find('[name="field_options[]"]').val() !== "")
				textarea.val( row.find('[name="field_options[]"]').val().split(';').join('\r\n') );

			textarea.focusout(function() {
				var lines = $(this).val().split(/\r|\r\n|\n/);
				$(this).closest('tr').find('[name="field_options[]"]').val( lines.join(';') );
			});
		}
		row.find('.setrequired').change(function() {
			var required = $(this).is(':checked') ? 'on' : 'off';
			$(this).parent('td').find('[name="field_required[]"]').val(required);
		});
		row.find('.delete_field').click(function() {
			if (confirm("¿Seguro que quieres eliminar este campo?"))
				$(this).closest('tr').remove();
			return false;
		});
	}

	function prepareShortcodes() {
		$('.contactshortcodes').html('');
		$('#fields_table [name="field_name[]"]').each(function() {
			if ($(this).val() != '')
				$('.contactshortcodes').append('<li><code>*'+$(this).val()+'*</code></li>');
		});
	}

}(jQuery));