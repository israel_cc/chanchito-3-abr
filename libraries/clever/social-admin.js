(function ($) {
	prepareSort();

	$(document).ajaxSuccess(function(e, xhr, settings) {
		var widget_id_base = 'clever_social';
		
		if (settings.data.search('action=save-widget') != -1 && settings.data.search('id_base=' + widget_id_base) != -1) {
			prepareSort();
		}
	});

	function prepareSort() {
		$('#social-options ul').sortable({
			handle: '.sort-social',
			opacity: 0.6,
			revert: true,
			scroll: true
		});
	}
}(jQuery));